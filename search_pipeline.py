#!/usr/bin/env python3

import numpy
import math
from astropy import units 
from astropy.cosmology import Planck15, z_at_value
import time 
from multiprocessing import Pool, Process
import multiprocessing
import sys
import random
from astropy.cosmology.core import CosmologyError
import os
from lal.rate import gaussian_window
import scipy.signal
import subprocess
import argparse

d_ref = 40 * units.Mpc

consective_rev06 = [1878, 1879, 1889, 1894, 1904, 1906, 1907, 1908, 1909, 1912, 1913, 1914, 1919, 1920, 1921, 1924, 1926, 1927, 1928, 1929, 1931, 1933, 1934, 1935, 1936, 1939,1958, 1959, 1965, 1969, 1970, 1971, 1972, 1975, 1976, 1977, 1978, 1979, 1980, 1983, 1984, 1985, 1988, 1989, 1990, 2012, 2013, 2029, 2030, 2031, 2032, 2039, 2040, 2044, 2046, 2059, 2060, 2072, 2073, 2075, 2077, 2079, 2080, 2081, 2084, 2085, 2086, 2087, 2093, 2096, 2097, 2098, 2099, 2105, 2106, 2107, 2108, 2109, 2111, 2112, 2118, 2121, 2122, 2126, 2127, 2133, 2134, 2135, 2143, 2163, 2166, 2167, 2174, 2175, 2180, 2181, 2182]
day_duration = consective_rev06[-1] - consective_rev06[0] + 1

t_duration_list = numpy.logspace(0, 3.5, 10)
threshold_array = numpy.logspace(-1, 2, 50)

valid_pix_array = numpy.load("npys/million_valid_pix_array.npy")
merger_random = numpy.load("npys/million_merger_random.npy")
inj_template_array = numpy.load("npys/million_inj_temp_array.npy")
tot_com_dis = numpy.load("npys/million_tot_com_dis.npy")

# updated version so that the modules below can treat a template bank, i.e. multiple templates, at once
# the template bank must have a shape of (number of templates, time)
def weight_ave_mt(x,sigma_array):
    sigma_sq = sigma_array**2
    convolution = numpy.nansum(x/sigma_sq, axis=-1)
    denominator = numpy.nansum(1./sigma_sq, axis=-1)
    #print(x, convolution.shape, denominator.shape, numpy.ones_like(convolution).shape)
    try:
        return numpy.divide(convolution, denominator, out=numpy.ones_like(convolution)*numpy.nan, where=(denominator != 0))
    except TypeError: 
        if denominator == 0.:
            return numpy.nan
        else:
            return 1.*convolution/denominator

def convolve_temp_mt(data_array, sigma_array):
    """return a weighted inner product of (data, template) and that of (template, template) with a template bank"""
    template_bank = numpy.load("npys/tot_array_interp_100.npy")
    
    convolve_temp = numpy.zeros(template_bank.shape)
    convolve_temp_data = numpy.zeros(template_bank.shape)
    ave_temp = numpy.zeros(template_bank.shape)
    ave_data = numpy.zeros(template_bank.shape)
    
    for n in range(template_bank.shape[1]):
        zero_pad = numpy.zeros((template_bank.shape[0],n))
        part_template = template_bank[:,:template_bank.shape[1]-n]
        padded_template = numpy.hstack((zero_pad, template_bank))[:,:template_bank.shape[1]]
        part_data = data_array
        part_sigma = sigma_array

        assert padded_template.shape[1] == part_data.shape[0], f"data not matched, {padded_template.shape}, {part_data.shape}"            
        assert padded_template.shape[1] == part_sigma.shape[0], f"sigma not matched, {padded_template.shape}, {part_sigma.shape}"
        assert (numpy.isnan(part_data) == numpy.isnan(part_sigma)).all(), "all the nan values should be in the same places!"
        sub_temp_ave = weight_ave_mt(padded_template, part_sigma)
        sub_data_ave = weight_ave_mt(part_data, part_sigma)

        padded_template -= sub_temp_ave[:,numpy.newaxis]
        convolve_temp[:,n] = numpy.nansum((padded_template/part_sigma)**2, axis=1)
        convolve_temp_data[:,n] = numpy.nansum(part_data*padded_template/part_sigma**2, axis=1)
        
        ave_temp[:,n] = sub_temp_ave
        ave_data[:,n] = sub_data_ave

    max_lambda = numpy.divide(0.5 * convolve_temp_data**2, convolve_temp, out=numpy.ones_like(convolve_temp)*numpy.nan, where=(convolve_temp != 0))
    amp_array =  numpy.divide(convolve_temp_data, convolve_temp, out=numpy.ones_like(convolve_temp)*numpy.nan, where=(convolve_temp != 0))
    const_array = ave_data - amp_array*ave_temp
    
    return convolve_temp, convolve_temp_data, max_lambda, amp_array, const_array, ave_temp

def data_condition_remove_outliers(data_vector, kernel_width, threshold_rms):
    """
    remove outliers.   pixels more than threshold_rms times the
    Gaussian-weighted RMS centred on their location are set to NaN.  the
    Gaussian kernel's FWHM is kernel_width days.  the input data is
    modified in place, and becomes the return value.
    """
    # see https://en.wikipedia.org/wiki/Full_width_at_half_maximum for
    # the standard deviation -to- FWHM conversion factor.  the kernel is
    # constructed so that the samples sum to 1
    kernel = gaussian_window(kernel_width / math.sqrt(2. * math.log(2.)))
    assert len(kernel) & 1, "kernel length is not odd"
    mid = int((len(kernel) - 1) / 2)
    orig_data_vector = data_vector.copy()

    # compute moving mean and mean square by looping over output days.
    data_squared = data_vector**2.
    mean = numpy.zeros_like(data_squared)
    mean_square = numpy.zeros_like(data_squared)
    norm_array = numpy.zeros_like(data_squared)
    max_ind = numpy.nanargmax(numpy.abs(data_vector))
    for i in range(len(mean_square)):
        # compute the slice of the input data and of the kernel to use for
        # this day
        start = max(0, i - mid)
        end = min(len(data_squared), i + mid + 1)
        kernel_start = mid - (i - start)
        kernel_end = mid + (end - i)

        # obtain the slices, and mask off days that are NaN so they do not
        # contribute to the average
        data_slice = data_vector[start : end].copy()
        data_squared_slice = data_squared[start : end].copy()
        kernel_slice = kernel[kernel_start : kernel_end].copy()
        mask = numpy.isnan(data_squared_slice)
        data_slice[mask] = 0.
        data_squared_slice[mask] = 0.
        kernel_slice[mask] = 0.

        # compute the Gaussian-weighted mean and mean square centred on
        # this day.  if the effective number of days used for the average
        # (surviving the NaN mask) is less than 1 then set the mean_square to inf
        # (day is gauranteed not to be deleted).  FIXME:  is that helpful?
        norm = kernel_slice.sum()
        norm_array[i] = norm/10.
        if norm <= kernel[mid]:
            mean[i] = 0.
            mean_square[i] = numpy.inf
        else:
            mean[i] = numpy.dot(data_slice, kernel_slice) / norm 
            mean_square[i] = numpy.dot(data_squared_slice, kernel_slice) / norm 

    # set samples whose magnitudes are above threshold to NaN
    data_vector[(data_vector - mean)**2. > (mean_square - mean**2.) * threshold_rms**2.] = numpy.nan

    return data_vector


# calculate P(likelihood ratio|noise), finding max likelihood ratio
def calc_likelihood(start_pix, end_pix, data, result_list, result_list_lock, inj_or_no_inj, dir_path, kernel_width, threshold_rms):
    start_func = time.time()

    for k in range(start_pix, end_pix):
        real_data = data[:,k]

        assert ~numpy.isnan(real_data).all(), f"{numpy.isnan(real_data)}"
        data_condition_remove_outliers(real_data, kernel_width, threshold_rms)
 
        data_sigma = numpy.nanstd(real_data)
        sigma_array = numpy.ones(day_duration)*data_sigma
        ind = (numpy.isnan(real_data))
        sigma_array[ind] = numpy.nan

        convolve_template, convolve_temp_data, Lambda_n, amp, const_array, ave_temp = convolve_temp_mt(real_data, sigma_array)
    
        pos_amp_ind = ((amp > 0) & (amp <= 1.78e3))
        try:
            max_ind = numpy.nanargmax(Lambda_n[pos_amp_ind])
            max_lambda = Lambda_n[pos_amp_ind][max_ind]
            if inj_or_no_inj == "y":
                estimate_amp = amp[pos_amp_ind][max_ind]
                estimate_const = const_array[pos_amp_ind][max_ind]
                max_ind_2d = numpy.where((Lambda_n == max_lambda) & (amp > 0))
                recovery_merger_time = max_ind_2d[1][0]
                recovery_template_number = max_ind_2d[0][0]
            else:
                pass
        except ValueError:
            max_lambda = numpy.nan
            if inj_or_no_inj == "y":
                estimate_amp = numpy.nan
                estimate_const = numpy.nan
                recovery_merger_time = numpy.nan
                recovery_template_number = numpy.nan
            else:
                pass

        scrambled_data = numpy.random.permutation(real_data)

        sigma_array_sc = numpy.ones(day_duration)*data_sigma
        ind = (numpy.isnan(scrambled_data))
        sigma_array_sc[ind] = numpy.nan

        convolve_template_sc, convolve_temp_data_sc, Lambda_n_sc, amp_sc, const_array_sc, ave_temp_sc = convolve_temp_mt(scrambled_data, sigma_array_sc)

        sc_pos_amp_ind = ((amp_sc > 0) & (amp_sc <= 1.78e3))
        try:
            sc_max_ind = numpy.nanargmax(Lambda_n_sc[sc_pos_amp_ind])
            max_lambda_sc = Lambda_n_sc[sc_pos_amp_ind][sc_max_ind]
        except ValueError:
            max_lambda_sc = numpy.nan

        result_list_lock.acquire()
        if inj_or_no_inj == "y":
            result_list.append((k, max_lambda, recovery_merger_time, estimate_amp, estimate_const, max_lambda_sc, recovery_template_number, scrambled_data))
        else:
            result_list.append((k, max_lambda, max_lambda_sc, scrambled_data))
        result_list_lock.release()

        if (k+1)%1000 == 0.:
            print(k+1, time.time() - start_func)
        if k == 1024 * 1793 + 818:
            print(k, data_sigma, max_lambda, max_lambda_sc)

if __name__ == "__main__":
    print("... starting ...")
    parser = argparse.ArgumentParser()

    parser.add_argument("-d", "--data-option", type=str, default="closed-box")
    parser.add_argument("-i", "--inj-no-inj", type=str, default="n")
    parser.add_argument("-n", "--part-num", type=int, default="0")
    parser.add_argument("-w", "--kernel-width", type=float, default="85.")
    parser.add_argument("-t", "--threshold-rms", type=float, default="1.265")

    args = parser.parse_args()

    if args.data_option == "closed-box":
        orig_dir_path = "not_perfect_temp_bank/all_map/true_uniform/million"
        #dir_path = f"window_width_challenge/{int(args.kernel_width)}_days/threshold_{args.threshold_rms}/not_perfect_temp_bank/"
        dir_path = f"window_width_challenge/{int(args.kernel_width)}_days/threshold_{args.threshold_rms}/not_perfect_temp_bank/million"
        if not os.path.exists(dir_path):
            subprocess.run(f"mkdir -p {os.path.join(dir_path, 'temp/storage0')}", shell=True)
            subprocess.run(f"mkdir -p {os.path.join(dir_path, 'temp/storage1')}", shell=True)
            subprocess.run(f"mkdir -p {os.path.join(dir_path, 'temp/storage2')}", shell=True)
        if args.inj_no_inj == "y":
            tag = f"w_inj_{args.part_num}"
            data_path = os.path.join(orig_dir_path, "injected_data.npy")
        elif args.inj_no_inj == "n":
            data_path = os.path.join(orig_dir_path, "no_inj_all_map.npy")
            tag = f"no_inj_{args.part_num}"
        else:
            print("set 'y' or 'n'.")
            exit()
    elif args.data_option == "gaussian":
        orig_dir_path = "gaussian_noise/all_at_once/data_sigma/not_perfect_temp_bank/true_uniform/million"
        #dir_path = f"window_width_challenge/{int(args.kernel_width)}_days/threshold_{args.threshold_rms}/gaussian/"
        dir_path = f"window_width_challenge/{int(args.kernel_width)}_days/threshold_{args.threshold_rms}/gaussian/million"
        if not os.path.exists(dir_path):
            subprocess.run(f"mkdir -p {os.path.join(dir_path, 'temp/storage0')}", shell=True)
            subprocess.run(f"mkdir -p {os.path.join(dir_path, 'temp/storage1')}", shell=True)
            subprocess.run(f"mkdir -p {os.path.join(dir_path, 'temp/storage2')}", shell=True)
        if args.inj_no_inj == "y":
            tag = f"w_inj_{args.part_num}"
            data_path = os.path.join(orig_dir_path, "injected_data.npy")
        elif args.inj_no_inj == "n":
            data_path = os.path.join(orig_dir_path, "no_inj_gaussian_noise.npy")
            tag = f"no_inj_{args.part_num}"
        else:
            print("set 'y' or 'n'.")
            exit()
        '''
    elif args.data_option == "limited_map":
        dir_path = "new_limited_map/"
        if args.inj_no_inj == "y":
            tag = f"w_inj_{args.part_num}"
            data_path = os.path.join(dir_path, "injected_data.npy")
        elif args.inj_no_inj == "n":
            #data_path = os.path.join(dir_path, "no_inj_new_limited_map.npy")
            data_path = os.path.join(dir_path, "no_inj_limited_map_new.npy")
            tag = f"no_inj_{args.part_num}"
        else:
            print("set 'y' or 'n'.")
            exit()
        '''
    elif args.data_option == "no_mask_gaussian":
        #dir_path = "no_mask/"
        dir_path = "no_mask/million"
        if args.inj_no_inj == "y":
            tag = f"w_inj_{args.part_num}"
            data_path = os.path.join(dir_path, "injected_data.npy")
        elif args.inj_no_inj == "n":
            data_path = os.path.join(dir_path, "no_inj_gaussian_noise_no_mask.npy")
            tag = f"no_inj_{args.part_num}"
        else:
            print("set 'y' or 'n'.")
            exit()
    else:
        print("set an appropriate tag!")
        exit()

    start_time = time.time()
    manager = multiprocessing.Manager()
    result_list = manager.list()
    result_list_lock = manager.Lock()

    processes = []
    data = numpy.load(data_path)
    
    print(f"... # of data = {data.shape[1]} ...")

    process_num = 32
    sample_number = 32

    for q in range(process_num):
        start_pix = process_num*sample_number*args.part_num + sample_number*q
        end_pix = min(start_pix + sample_number, data.shape[1])

        process = Process(target=calc_likelihood, kwargs={'start_pix': start_pix, 'end_pix': end_pix, 'data': data, 'result_list': result_list, 'result_list_lock':result_list_lock, "inj_or_no_inj": args.inj_no_inj, "dir_path": dir_path, "kernel_width":args.kernel_width, "threshold_rms": args.threshold_rms})
        process.start()
        processes.append(process)
        print(f"... {q+1}/{process_num} process(es) have been submitted: {start_pix} -  {end_pix}...")
        if end_pix == data.shape[1]:
            break

    for p in processes:
        p.join()

    result_list = list(result_list)
    result_list.sort(key = lambda row : row[0])
    print(len(result_list), dir_path)
    exp_pix_number = [row[0] for row in result_list]
    exp_likelihood = [row[1] for row in result_list]

    if args.inj_no_inj == "y":
        exp_rec_merger_time = [row[2] for row in result_list]
        exp_amp = [row[3] for row in result_list]
        exp_const = [row[4] for row in result_list]
        exp_likelihood_sc = [row[5] for row in result_list]
        exp_rec_temp_num = [row[6] for row in result_list]
        exp_data = [row[7] for row in result_list]
        print(exp_pix_number[0:10], exp_likelihood[0:10], exp_rec_merger_time[0:10], exp_rec_temp_num[:10])

        numpy.save(os.path.join(dir_path, f"lambda_{tag}"), exp_likelihood)
        numpy.save(os.path.join(dir_path, f"recovered_merger_time_{tag}"), exp_rec_merger_time)
        numpy.save(os.path.join(dir_path, f"recovered_amp_{tag}"), exp_amp)
        numpy.save(os.path.join(dir_path, f"recovered_const_{tag}"), exp_const)
        numpy.save(os.path.join(dir_path, f"lambda_after_shuffling_{tag}"), exp_likelihood_sc)
        numpy.save(os.path.join(dir_path, f"recovered_template_{tag}"), exp_rec_temp_num)
    elif args.inj_no_inj == "n":
        exp_likelihood_sc = [row[2] for row in result_list]
        exp_data = [row[3] for row in result_list]
        print(exp_pix_number[0:10], exp_likelihood[0:10])

        numpy.save(os.path.join(dir_path, f"temp/storage0/lambda_{tag}"), exp_likelihood)
        numpy.save(os.path.join(dir_path, f"temp/storage0/lambda_after_shuffling_{tag}"), exp_likelihood_sc)
    else:
        assert False, "cannot get here."

    print(len(exp_data))
    numpy.save(os.path.join(dir_path, f"temp/storage2/shuffled_data_{args.inj_no_inj}_{args.part_num}"), exp_data)
        
    print(f"... calculation done ... : {time.time() - start_time:.2f} sec")

