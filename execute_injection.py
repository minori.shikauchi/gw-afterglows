import subprocess
import numpy as np
import time
import sys

cmd = ""

if __name__ == "__main__":
    argvs = sys.argv
    start_submit_number = int(argvs[1])
    end_submit_number = int(argvs[2])

    for i in range(start_submit_number, end_submit_number):
        if i == end_submit_number - 1:
            #cmd += f"python3 injection_log.py {i} "
            cmd += f"python3 injection_data_sigma_gaussian_modified.py {i} "
            #cmd += f"python3 injection_large_data_sigma_gaussian.py {i} "
            #cmd += f"python3 injection.py {i} "
        else:
            #cmd += f"python3 injection_log.py {i} && "
            cmd += f"python3 injection_data_sigma_gaussian_modified.py {i} && "
            #cmd += f"python3 injection_large_data_sigma_gaussian.py {i} && "
            #cmd += f"python3 injection.py {i} && "

    print(cmd)
    subprocess.run(cmd, shell=True)

"""
tot_array = np.load(f"gaussian_noise/perfect_temp_bank/true_uniform/injection_0.npy")
print(f"0, {np.sum(tot_array > threshold_array[0])}")
print(tot_array.shape)
for i in range(1,50):
    part_array = np.load(f"gaussian_noise/perfect_temp_bank/true_uniform/injection_{i}.npy")
    print(f"{i}, {np.sum(part_array > threshold_array[i])}")

"""
