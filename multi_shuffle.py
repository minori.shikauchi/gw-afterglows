﻿import numpy as np
import math
from astropy.constants import c
import astropy.units as u
from draco.core import containers # needed for WienerFilter module
from ch_pipeline.core import containers as ch_containers
from astropy.cosmology import Planck15, z_at_value
from scipy.stats import chi2, chi
import time as tm
from multiprocessing import Pool, Process
import multiprocessing
import sys

consective_rev06 = [1878, 1879, 1889, 1894, 1904, 1906, 1907, 1908, 1909, 1912, 1913, 1914, 1919, 1920, 1921, 1924, 1926, 1927, 1928, 1929, 1931, 1933, 1934, 1935, 1936, 1939,1958, 1959, 1965, 1969, 1970, 1971, 1972, 1975, 1976, 1977, 1978, 1979, 1980, 1983, 1984, 1985, 1988, 1989, 1990, 2012, 2013, 2029, 2030, 2031, 2032, 2039, 2040, 2044, 2046, 2059, 2060, 2072, 2073, 2075, 2077, 2079, 2080, 2081, 2084, 2085, 2086, 2087, 2093, 2096, 2097, 2098, 2099, 2105, 2106, 2107, 2108, 2109, 2111, 2112, 2118, 2121, 2122, 2126, 2127, 2133, 2134, 2135, 2143, 2163, 2166, 2167, 2174, 2175, 2180, 2181, 2182]
day_duration = consective_rev06[-1] - consective_rev06[0] + 1

sigma = 6 * 10**(-3)
t_duration_list = np.logspace(0, 3.5, 10)
sample_number = 1200

# updated version so that the modules below can treat a template bank, i.e. multiple templates, at once
# the template bank must have a shape of (number of templates, time)
def weight_ave_mt(x,sigma):
    return np.nansum(x/sigma**2, axis=1)/np.nansum(1/sigma**2, axis=1)

#def convolve_temp_mt(data, temp, sigma):
def convolve_temp_mt(data, sigma):
    """return a weighted inner product of (data, template) and that of (template, template) with a template bank"""
    temp = np.load("/project/rpp-chime/minorip/tot_array_interp_100.npy")
    stack_data = np.tile(data, (temp.shape[0], 1)) # duplicate data for the number of templates
    stack_sigma = np.tile(sigma, (temp.shape[0], 1)) # duplicate sigma for the number of templates
        
    #nan_array = np.array(np.where(np.isnan(stack_data)))
    nan_array = np.isnan(stack_data)
    assert len(stack_data) == len(temp), f"lengths should be the same, {len(stack_data)}, {len(temp)}"
    
    convolve_temp = np.zeros((temp.shape[0],temp.shape[1]))
    convolve_temp_data = np.zeros((temp.shape[0],temp.shape[1]))
    ave_temp = np.zeros((temp.shape[0],temp.shape[1]))
    #convolve_temp = []
    #convolve_temp_data = []
    #ave_temp = []
    
    sub_data_ave = weight_ave_mt(stack_data, stack_sigma)
    for n in range(temp.shape[1]):
        copy_template = temp.copy()
        #copy_template = np.load("/project/rpp-chime/minorip/tot_array_interp_100.npy")
        
        if n == 0:
            copy_template[nan_array] = np.nan
            sub_temp_ave = weight_ave_mt(copy_template, stack_sigma)
            
            part_convolve_temp = np.nansum((copy_template - sub_temp_ave[:,np.newaxis])**2/sigma**2, axis=1)
            part_convolve_temp_data = np.nansum(data*(copy_template - sub_temp_ave[:,np.newaxis])/sigma**2, axis=1)
            #if (part_convolve_temp == 0).all():
            #    print(copy_template[0:10])
            #    print(n, np.sum(np.isnan(copy_template)), np.sum(copy_template == 0), (part_convolve_temp == 0).all())
        else:
            zero_pad = np.zeros((temp.shape[0], n))
            padded_template = np.hstack((zero_pad, copy_template[:,0:-n]))

            assert len(padded_template) == len(temp), f"not matched, {len(padded_template)}, {len(temp)}"            
            padded_template[nan_array] = np.nan
            sub_temp_ave = weight_ave_mt(padded_template, stack_sigma)

            part_convolve_temp = np.nansum((padded_template - sub_temp_ave[:,np.newaxis])**2/sigma**2, axis=1)
            part_convolve_temp_data = np.nansum(data*(padded_template - sub_temp_ave[:,np.newaxis])/sigma**2, axis=1)
            #if (part_convolve_temp == 0).all():
            #    #print(part_convolve_temp[0:10])
            #    #print(copy_template[0:10])
            #    print(n, np.sum(np.isnan(padded_template)), np.sum(padded_template == 0), (part_convolve_temp == 0).all())
        
        #ave_temp.append(sub_temp_ave)
        #print(sub_temp_ave.shape)
        ave_temp[:,n] = sub_temp_ave
        
        #convolve_temp.append(part_convolve_temp)
        #convolve_temp_data.append(part_convolve_temp_data)
        convolve_temp[:,n] = part_convolve_temp
        convolve_temp_data[:,n] = part_convolve_temp_data

    #convolve_temp = np.array(convolve_temp)
    #convolve_temp_data = np.array(convolve_temp_data)
    #ave_temp = np.array(ave_temp)
    #print(sub_data_ave.shape, convolve_temp.shape, convolve_temp_data.shape, ave_temp.shape)
    #if (convolve_temp == 0).all():
    #    print(temp)
    
    #max_lambda = np.where(convolve_temp != 0., 1./2 * convolve_temp_data**2 /convolve_temp, np.nan)
    #amp_array =  np.where(convolve_temp != 0., convolve_temp_data/convolve_temp, np.nan)    
    max_lambda = np.divide(1./2 * convolve_temp_data**2, convolve_temp, out=np.ones_like(convolve_temp)*np.nan, where=(convolve_temp != 0))
    amp_array =  np.divide(convolve_temp_data, convolve_temp, out=np.ones_like(convolve_temp)*np.nan, where=(convolve_temp != 0))
    const_array = sub_data_ave[:,np.newaxis] - amp_array*ave_temp
    #print(sub_data_ave.shape, amp_array.shape, ave_temp.shape)
    
    return convolve_temp, convolve_temp_data, max_lambda, amp_array, const_array, ave_temp

# calculate P(likelihood ratio|noise), finding max likelihood ratio
def calc_likelihood_ratio(start_pix, end_pix, result_list):
    #limited_map = np.load("/project/rpp-chime/minorip/limited_map.npy")

    for p in range(start_pix, end_pix):
        pixel_random_number = p
        #data = limited_map[:,pixel_random_number].copy()
        #scrambled_data = np.random.permutation(data)
        scrambled_data = limited_map[:,pixel_random_number].copy()
    
        sigma_array = np.ones(day_duration)*sigma
        ind = (np.isnan(scrambled_data))
        sigma_array[ind] = np.nan

        #convolve_template, convolve_temp_data, Lambda_n, amp, const_array, ave_temp = convolve_temp_mt(scrambled_data, tot_array_interp, sigma_array)
        convolve_template, convolve_temp_data, Lambda_n, amp, const_array, ave_temp = convolve_temp_mt(scrambled_data, sigma_array)
        if np.isnan(Lambda_n).all():
            print(k, np.isnan(convolve_template).all(), np.isnan(convolve_temp_data).all(), Lambda_n)
            print(k, (convolve_template == 0).all(), np.isnan(convolve_temp_data).all(), Lambda_n)
            exit()
        max_ind = np.where(Lambda_n == np.nanmax(Lambda_n))
        estimate_amp = amp[max_ind]
        estimate_const = const_array[max_ind]
        if (p+1) % 10000 == 0:
            print(f"... {p+1} processed ...")

        result_list.append([p, np.nanmax(Lambda_n)])

if __name__ == "__main__":
    print("... starting ...")
    argvs = sys.argv
    file_number = int(argvs[1])
    repeat_number = int(argvs[2])

    limited_map = np.load("/project/rpp-chime/minorip/shuffle_limited_map_{repeat_number}.npy")

    start_time = tm.time()
    manager = multiprocessing.Manager()
    result_list = manager.list()

    processes = []
    for q in range(50):
        start_pix = sample_number*50*file_number + sample_number*q
        end_pix = start_pix + sample_number

        process = Process(target=calc_likelihood_ratio, kwargs={'start_pix': start_pix, 'end_pix': end_pix, 'result_list': result_list})
        process.start()
        processes.append(process)
        #print(f"... {q+1}/50 process(es) have been submitted: {start_pix} -  {end_pix}...")

    for p in processes:
        p.join()

    result_list = np.array(result_list)
    print(result_list.shape)
    np.save(f"test_likelihood_shuffle{repeat_number}_{file_number}", result_list)
        
    print(f"... {sample_number*50} calculate likelihood ratio ... : {tm.time() - start_time:.2f} sec")

