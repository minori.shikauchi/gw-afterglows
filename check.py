import numpy 
from lal.rate import gaussian_window
import matplotlib.pyplot as plt
import math

#shuffle_data = numpy.load("not_perfect_temp_bank/all_map/true_uniform/no_inj_all_map.npy")

#def data_condition_remove_outliers(data_vector, kernel_width = 60., threshold_rms = 0.8):
def data_condition_remove_outliers(data_vector, kernel_width = 60., threshold_rms = 2.):
    """
    remove outliers.   pixels more than threshold_rms times the
    Gaussian-weighted RMS centred on their location are set to NaN.  the
    Gaussian kernel's FWHM is kernel_width days.  the input data is
    modified in place, and becomes the return value.
    """
    # see https://en.wikipedia.org/wiki/Full_width_at_half_maximum for
    # the standard deviation -to- FWHM conversion factor.  the kernel is
    # constructed so that the samples sum to 1
    kernel = gaussian_window(kernel_width / math.sqrt(2. * math.log(2.)))
    assert len(kernel) & 1, "kernel length is not odd"
    mid = int((len(kernel) - 1) / 2)
    orig_data_vector = data_vector.copy()

    # compute moving mean and mean square by looping over output days.
    data_squared = data_vector**2.
    mean = numpy.zeros_like(data_squared)
    mean_square = numpy.zeros_like(data_squared)
    norm_array = numpy.zeros_like(data_squared)
    max_ind = numpy.nanargmax(numpy.abs(data_vector))
    for i in range(len(mean_square)):
        # compute the slice of the input data and of the kernel to use for
        # this day
        start = max(0, i - mid)
        end = min(len(data_squared), i + mid + 1)
        kernel_start = mid - (i - start)
        kernel_end = mid + (end - i)

        # obtain the slices, and mask off days that are NaN so they do not
        # contribute to the average
        data_slice = data_vector[start : end].copy()
        data_squared_slice = data_squared[start : end].copy()
        kernel_slice = kernel[kernel_start : kernel_end].copy()
        mask = numpy.isnan(data_squared_slice)
        data_slice[mask] = 0.
        data_squared_slice[mask] = 0.
        kernel_slice[mask] = 0.

        # compute the Gaussian-weighted mean and mean square centred on
        # this day.  if the effective number of days used for the average
        # (surviving the NaN mask) is less than 1 then set the mean_square to inf
        # (day is gauranteed not to be deleted).  FIXME:  is that helpful?
        norm = kernel_slice.sum()
        norm_array[i] = norm/10.
        if norm <= kernel[mid]:
            mean[i] = 0.
            mean_square[i] = numpy.inf
        else:
            mean[i] = numpy.dot(data_slice, kernel_slice) / norm 
            mean_square[i] = numpy.dot(data_squared_slice, kernel_slice) / norm 

        if not numpy.isnan(data_vector[i]):
            #print(data_slice, data_squared_slice, kernel_slice)
            print(i, numpy.abs(data_vector)[i], numpy.sqrt(mean_square-mean**2)[i], mean[i], mean_square[i])

    # set samples whose magnitudes are above threshold to NaN
    """
    std_array = numpy.sqrt(mean_square - mean**2)
    no_nan_ind = numpy.where(~numpy.isnan(std_array))[0]
    print(no_nan_ind)
    no_nan_std_array = std_array[no_nan_ind]
    for i in range(len(no_nan_ind)):
        start = max(0, i-1)
        end = min(len(mean_square), i+2)
        mean_std = numpy.nanmean(no_nan_std_array[start : end])

        data_ind = no_nan_ind[i]
        if data_vector[data_ind] > threshold_rms * mean_std:
            print(i, data_vector[data_ind], mean_std)
            data_vector[data_ind] = numpy.nan
        else:
            pass
    #data_vector[numpy.abs(data_vector) > mean_std * threshold_rms] = numpy.nan
    """
    data_vector[(data_vector - mean)**2. > (mean_square - mean**2.) * threshold_rms**2.] = numpy.nan

    return data_vector, norm_array, numpy.sqrt(numpy.clip(mean_square - mean**2., 0, None)), mean

std = 0.1
test_data = numpy.random.normal(0, std, 305)
nan_ind = numpy.append(numpy.random.randint(305, size=240), numpy.arange(290, 305, 1))
test_data[nan_ind] = numpy.nan

#last_ind = numpy.random.randint(numpy.min(nan_ind), 280, size=5)
test_data[280:] = std*numpy.arange(25)
test_data[300] = 10*std

#numpy.save("orig_data", numpy.random.permutation(shuffle_data[:,0]))
#orig_data = numpy.load("orig_data.npy")
#test_data = numpy.random.permutation(orig_data)
#numpy.save("test_data", test_data)
#test_data = numpy.load("test_data.npy")
orig_test_data = test_data.copy()

_, norm_array, std_sq_array, mean_array = data_condition_remove_outliers(test_data)
"""
threshold = 1.
block_num = 30
prev_rms = 10.
for i in range(0, len(test_data), block_num):
    end = min(len(test_data), i+block_num)
    data_slice = test_data[i:end]

    rms = numpy.sqrt(numpy.nanmean(data_slice**2.))
    if rms > 3. * prev_rms:
        ind = (numpy.abs(data_slice) > threshold * prev_rms)
    else:
        ind = (numpy.abs(data_slice) > threshold * rms)
    print(i, rms, data_slice, prev_rms)
    test_data[i:end][ind] = numpy.nan

    prev_rms = rms
"""

fig = plt.figure(figsize=(12,8))
axis = fig.add_subplot(1,1,1)

#plt.scatter(numpy.arange(305), mean_array, label="mean_array")
plt.scatter(numpy.arange(305), orig_test_data, label="test_data")
plt.scatter(numpy.arange(305), test_data, label="clean_data")
#plt.scatter(numpy.arange(305), norm_array, label="norm_array")
#plt.scatter(numpy.arange(305), std_sq_array, label="std_sq_array")
print(numpy.nanstd(test_data))

plt.legend()
fig.savefig("test.png")

