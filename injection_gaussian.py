import numpy
import math
from astropy import units 
from astropy.cosmology import Planck15, z_at_value
import time 
from multiprocessing import Pool, Process
import multiprocessing
import sys
import random
from astropy.cosmology.core import CosmologyError

d_ref = 40 * units.Mpc

consective_rev06 = [1878, 1879, 1889, 1894, 1904, 1906, 1907, 1908, 1909, 1912, 1913, 1914, 1919, 1920, 1921, 1924, 1926, 1927, 1928, 1929, 1931, 1933, 1934, 1935, 1936, 1939,1958, 1959, 1965, 1969, 1970, 1971, 1972, 1975, 1976, 1977, 1978, 1979, 1980, 1983, 1984, 1985, 1988, 1989, 1990, 2012, 2013, 2029, 2030, 2031, 2032, 2039, 2040, 2044, 2046, 2059, 2060, 2072, 2073, 2075, 2077, 2079, 2080, 2081, 2084, 2085, 2086, 2087, 2093, 2096, 2097, 2098, 2099, 2105, 2106, 2107, 2108, 2109, 2111, 2112, 2118, 2121, 2122, 2126, 2127, 2133, 2134, 2135, 2143, 2163, 2166, 2167, 2174, 2175, 2180, 2181, 2182]
day_duration = consective_rev06[-1] - consective_rev06[0] + 1

sigma_orig = 6 * 10**(-6)
t_duration_list = numpy.logspace(0, 3.5, 10)
threshold_array = numpy.logspace(-1, 2, 50)

tot_likelihood = numpy.load("tot_likelihood_all_map.npy")
tot_pix_number = numpy.load("tot_pix_number_all_map.npy")
valid_pix_array = numpy.load("valid_pix_array.npy")

lc_random = numpy.load("lc_random.npy")
merger_random = numpy.load("merger_random.npy")

#tot_array_interp_100 = numpy.load("tot_array_interp_100.npy")
inj_template_array = numpy.load("inj_temp_array.npy")

tot_com_dis = numpy.load("tot_com_dis.npy")
#tot_com_dis = numpy.load("com_dis_log_uniform.npy")

# updated version so that the modules below can treat a template bank, i.e. multiple templates, at once
# the template bank must have a shape of (number of templates, time)
def weight_ave_mt(x,sigma_array):
    sigma_sq = sigma_array**2
    convolution = numpy.nansum(x/sigma_sq, axis=-1)
    denominator = numpy.nansum(1./sigma_sq, axis=-1)
    #print(x, convolution.shape, denominator.shape, numpy.ones_like(convolution).shape)
    try:
        return numpy.divide(convolution, denominator, out=numpy.ones_like(convolution)*numpy.nan, where=(denominator != 0))
    except TypeError: 
        if denominator == 0.:
            return numpy.nan
        else:
            return 1.*convolution/denominator

def convolve_temp_mt(data_array, sigma_array):
    """return a weighted inner product of (data, template) and that of (template, template) with a template bank"""
    template_bank = numpy.load("tot_array_interp_100.npy")
    
    convolve_temp = numpy.zeros(template_bank.shape)
    convolve_temp_data = numpy.zeros(template_bank.shape)
    ave_temp = numpy.zeros(template_bank.shape)
    ave_data = numpy.zeros(template_bank.shape)
    
    for n in range(template_bank.shape[1]):
        zero_pad = numpy.zeros((template_bank.shape[0],n))
        part_template = template_bank[:,:template_bank.shape[1]-n]
        padded_template = numpy.hstack((zero_pad, template_bank))[:,:template_bank.shape[1]]
        part_data = data_array
        part_sigma = sigma_array

        assert padded_template.shape[1] == part_data.shape[0], f"data not matched, {padded_template.shape}, {part_data.shape}"            
        assert padded_template.shape[1] == part_sigma.shape[0], f"sigma not matched, {padded_template.shape}, {part_sigma.shape}"
        assert (numpy.isnan(part_data) == numpy.isnan(part_sigma)).all(), "all the nan values should be in the same places!"
        sub_temp_ave = weight_ave_mt(padded_template, part_sigma)
        sub_data_ave = weight_ave_mt(part_data, part_sigma)

        padded_template -= sub_temp_ave[:,numpy.newaxis]
        convolve_temp[:,n] = numpy.nansum((padded_template/part_sigma)**2, axis=1)
        convolve_temp_data[:,n] = numpy.nansum(part_data*padded_template/part_sigma**2, axis=1)
        
        ave_temp[:,n] = sub_temp_ave
        ave_data[:,n] = sub_data_ave

    max_lambda = numpy.divide(0.5 * convolve_temp_data**2, convolve_temp, out=numpy.ones_like(convolve_temp)*numpy.nan, where=(convolve_temp != 0))
    amp_array =  numpy.divide(convolve_temp_data, convolve_temp, out=numpy.ones_like(convolve_temp)*numpy.nan, where=(convolve_temp != 0))
    const_array = ave_data - amp_array*ave_temp
    
    return convolve_temp, convolve_temp_data, max_lambda, amp_array, const_array, ave_temp

# calculate P(likelihood ratio|noise), finding max likelihood ratio
def injection_test_w_gaussian_noise(start_pix, end_pix, result_list, result_list_lock):
    for k in range(start_pix, end_pix):
        merger_random_number = merger_random[k]
        
        sigma_array = numpy.ones(day_duration)*sigma_orig
        ind = (numpy.isnan(sigma_array))
        sigma_array[ind] = numpy.nan

        data = numpy.random.normal(0, sigma_orig, 305)
        convolve_template_ni, convolve_temp_data_ni, Lambda_n_ni, amp_ni, const_array_ni, ave_temp_ni = convolve_temp_mt(data, sigma_array)
        pos_amp_ind_ni = (amp_ni > 0)
        try:
            max_ind_ni = numpy.nanargmax(Lambda_n_ni[pos_amp_ind_ni])
            max_lambda_ni = Lambda_n_ni[pos_amp_ind_ni][max_ind_ni]
        except ValueError:
            max_lambda_ni = numpy.nan

        inj_template = inj_template_array[k]
    
        com_dis = tot_com_dis[k] * units.Mpc
        try:
            z = z_at_value(Planck15.comoving_distance, com_dis)
        except CosmologyError:
            z = 0
                
        lum_dis = Planck15.luminosity_distance(z)
        beta = (1 + z)*(d_ref/lum_dis)**2
    
        data[merger_random_number:] += (beta.value*inj_template)[:305-merger_random_number]
        data -= numpy.nanmedian(data)
    
        convolve_template, convolve_temp_data, Lambda_n, amp, const_array, ave_temp = convolve_temp_mt(data, sigma_array)

        pos_amp_ind = (amp > 0)
        try:
            max_ind = numpy.nanargmax(Lambda_n[pos_amp_ind])
            max_lambda = Lambda_n[pos_amp_ind][max_ind]
            estimate_amp = amp[pos_amp_ind][max_ind]
            estimate_const = const_array[pos_amp_ind][max_ind]
            max_ind_2d = numpy.where((Lambda_n == max_lambda) & (amp > 0))
            recovery_merger_time = max_ind_2d[1][0]
        except ValueError:
            max_lambda = numpy.nan
            estimate_amp = numpy.nan
            estimate_const = numpy.nan
            recovery_merger_time = numpy.nan

        if (k + 1) % sample_number == 0:
            print(k+1, time.time() - start_time)

        result_list_lock.acquire()
        result_list.append([k, max_lambda, recovery_merger_time, merger_random_number, estimate_amp, estimate_const, max_lambda_ni])
        result_list_lock.release()

if __name__ == "__main__":
    print("... starting ...")

    start_time = time.time()
    manager = multiprocessing.Manager()
    result_list = manager.list()
    result_list_lock = manager.Lock()

    processes = []

    process_num = 20
    inj_num = 1000
    sample_number = inj_num//process_num + 1

    for q in range(process_num):
        start_pix = sample_number*q
        end_pix = min(start_pix + sample_number, inj_num)

        process = Process(target=injection_test_w_gaussian_noise, kwargs={'start_pix': start_pix, 'end_pix': end_pix, 'result_list': result_list, "result_list_lock": result_list_lock})
        process.start()
        processes.append(process)
        print(f"... {q+1}/{process_num} process(es) have been submitted: {start_pix} -  {end_pix}...")
        if end_pix == inj_num:
            break

    for p in processes:
        p.join()

    result_list = numpy.array(result_list)
    ind = numpy.argsort(result_list[:,0])
    exp_likelihood = result_list[ind, 1]
    exp_pix_number = result_list[ind, 0]
    exp_rec_merger_time = result_list[ind, 2]
    exp_inj_merger_time = result_list[ind, 3]
    exp_amp = result_list[ind, 4]
    exp_const = result_list[ind, 5]
    exp_likelihood_ni = result_list[ind, 6]
    print(result_list.shape, exp_pix_number[0:10], exp_likelihood[0:10], exp_rec_merger_time[0:10], exp_inj_merger_time[0:10])
    numpy.save(f"gaussian_noise/not_perfect_temp_bank/true_uniform/det_pix", exp_pix_number)
    numpy.save(f"gaussian_noise/not_perfect_temp_bank/true_uniform/injection", exp_likelihood)
    numpy.save(f"gaussian_noise/not_perfect_temp_bank/true_uniform/recovered_merger_time", exp_rec_merger_time)
    numpy.save(f"gaussian_noise/not_perfect_temp_bank/true_uniform/injected_merger_time", exp_inj_merger_time)
    numpy.save(f"gaussian_noise/not_perfect_temp_bank/true_uniform/recovered_amp", exp_amp)
    numpy.save(f"gaussian_noise/not_perfect_temp_bank/true_uniform/recovered_const", exp_const)
    numpy.save(f"gaussian_noise/not_perfect_temp_bank/true_uniform/no_injection", exp_likelihood_ni)
        
    print(f"... injection test ... : {time.time() - start_time:.2f} sec")

