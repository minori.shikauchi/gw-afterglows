﻿import numpy
import math
from astropy import units
from astropy.cosmology import Planck15, z_at_value
from scipy.stats import chi2, chi
import time 
from multiprocessing import Pool, Process
import multiprocessing
import sys

consective_rev06 = [1878, 1879, 1889, 1894, 1904, 1906, 1907, 1908, 1909, 1912, 1913, 1914, 1919, 1920, 1921, 1924, 1926, 1927, 1928, 1929, 1931, 1933, 1934, 1935, 1936, 1939,1958, 1959, 1965, 1969, 1970, 1971, 1972, 1975, 1976, 1977, 1978, 1979, 1980, 1983, 1984, 1985, 1988, 1989, 1990, 2012, 2013, 2029, 2030, 2031, 2032, 2039, 2040, 2044, 2046, 2059, 2060, 2072, 2073, 2075, 2077, 2079, 2080, 2081, 2084, 2085, 2086, 2087, 2093, 2096, 2097, 2098, 2099, 2105, 2106, 2107, 2108, 2109, 2111, 2112, 2118, 2121, 2122, 2126, 2127, 2133, 2134, 2135, 2143, 2163, 2166, 2167, 2174, 2175, 2180, 2181, 2182]
day_duration = consective_rev06[-1] - consective_rev06[0] + 1

sigma_orig = 6 * 10**(-3)
t_duration_list = numpy.logspace(0, 3.5, 10)
sample_number = 3400*2

# updated version so that the modules below can treat a template bank, i.e. multiple templates, at once
# the template bank must have a shape of (number of templates, time)
def weight_ave_mt(x,sigma_array):
    sigma_sq = sigma_array**2
    convolution = numpy.nansum(x/sigma_sq, axis=-1)
    denominator = numpy.nansum(1./sigma_sq, axis=-1)
    #print(x, convolution.shape, denominator.shape, numpy.ones_like(convolution).shape)
    try:
        return numpy.divide(convolution, denominator, out=numpy.ones_like(convolution)*numpy.nan, where=(denominator != 0))
    except TypeError: 
        if denominator == 0.:
            return numpy.nan
        else:
            return 1.*convolution/denominator

def convolve_temp_mt(data_array, sigma_array):
    """return a weighted inner product of (data, template) and that of (template, template) with a template bank"""
    template_bank = numpy.load("tot_array_interp_100.npy")
    
    convolve_temp = numpy.zeros(template_bank.shape)
    convolve_temp_data = numpy.zeros(template_bank.shape)
    ave_temp = numpy.zeros(template_bank.shape)
    ave_data = numpy.zeros(template_bank.shape)
    
    for n in range(template_bank.shape[1]):
        zero_pad = numpy.zeros((template_bank.shape[0],n))
        part_template = template_bank[:,:template_bank.shape[1]-n]
        padded_template = numpy.hstack((zero_pad, template_bank))[:,:template_bank.shape[1]]
        part_data = data_array
        part_sigma = sigma_array

        assert padded_template.shape[1] == part_data.shape[0], f"data not matched, {padded_template.shape}, {part_data.shape}"            
        assert padded_template.shape[1] == part_sigma.shape[0], f"sigma not matched, {padded_template.shape}, {part_sigma.shape}"
        assert (numpy.isnan(part_data) == numpy.isnan(part_sigma)).all(), "all the nan values should be in the same places!"
        sub_temp_ave = weight_ave_mt(padded_template, part_sigma)
        sub_data_ave = weight_ave_mt(part_data, part_sigma)

        padded_template -= sub_temp_ave[:,numpy.newaxis]
        convolve_temp[:,n] = numpy.nansum((padded_template/part_sigma)**2, axis=1)
        convolve_temp_data[:,n] = numpy.nansum(part_data*padded_template/part_sigma**2, axis=1)
        
        ave_temp[:,n] = sub_temp_ave
        ave_data[:,n] = sub_data_ave

    max_lambda = numpy.divide(0.5 * convolve_temp_data**2, convolve_temp, out=numpy.ones_like(convolve_temp)*numpy.nan, where=(convolve_temp != 0))
    amp_array =  numpy.divide(convolve_temp_data, convolve_temp, out=numpy.ones_like(convolve_temp)*numpy.nan, where=(convolve_temp != 0))
    const_array = ave_data - amp_array*ave_temp
    
    return convolve_temp, convolve_temp_data, max_lambda, amp_array, const_array, ave_temp


# calculate P(likelihood ratio|noise), finding max likelihood ratio
def calc_likelihood_ratio(start_pix, end_pix, result_list, shuffle_number, result_list_lock):
    limited_map = numpy.load(f"all_map_shuffle{shuffle_number}.npy")
    block_num = 3
    threshold = 3
    loop_num = limited_map.shape[0]//block_num + 1

    for p in range(start_pix, end_pix):
        pixel_random_number = p
        data = limited_map[:,pixel_random_number].copy()
    
        scrambled_data = numpy.random.permutation(data)
        for i in range(loop_num):
            start = block_num*i
            end = min(block_num*(i+1), len(scrambled_data))
            part_scrambled_data = scrambled_data[start:end]
    
            rms = numpy.sqrt(numpy.nanmean(scrambled_data**2))
            deviation_ind = (part_scrambled_data > threshold * rms)
            part_scrambled_data[deviation_ind] = numpy.nan
    
            scrambled_data[start:end] = part_scrambled_data

        data_sigma = numpy.nanstd(scrambled_data)
        sigma_array = numpy.ones(day_duration)*data_sigma
        ind = (numpy.isnan(scrambled_data))
        sigma_array[ind] = numpy.nan

        convolve_template, convolve_temp_data, Lambda_n, amp, const_array, ave_temp = convolve_temp_mt(scrambled_data, sigma_array)
        if numpy.isnan(Lambda_n).all():
            print(k, numpy.isnan(convolve_template).all(), numpy.isnan(convolve_temp_data).all(), Lambda_n)
            print(k, (convolve_template == 0).all(), numpy.isnan(convolve_temp_data).all(), Lambda_n)
            exit()

        pos_amp_ind = (amp > 0)
        try:
            max_ind = numpy.nanargmax(Lambda_n[pos_amp_ind])
            max_lambda = Lambda_n[pos_amp_ind][max_ind]
        except ValueError:
            max_lambda = numpy.nan

        if (p+1) % 10000 == 0:
            print(f"... {p+1} processed ...")
            print(max_lambda)

        result_list_lock.acquire()
        result_list.append([p, max_lambda])
        result_list_lock.release()

if __name__ == "__main__":
    print("... starting ...")
    argvs = sys.argv
    file_number = int(argvs[1])
    shuffle_number = int(argvs[2])

    start_time = time.time()
    manager = multiprocessing.Manager()
    result_list = manager.list()
    result_list_lock = manager.Lock()

    processes = []
    process_num = 25
    for q in range(process_num):
        #process = Process(target=calc_likelihood_ratio, kwargs={'k': q, 'limited_map': limited_map, 'tot_array_interp': tot_array_interp, 'result_list': result_list})
        start_pix = sample_number*process_num*file_number + sample_number*q
        end_pix = start_pix + sample_number

        process = Process(target=calc_likelihood_ratio, kwargs={'start_pix': start_pix, 'end_pix': end_pix, 'result_list': result_list, 'shuffle_number': shuffle_number, "result_list_lock":result_list_lock})
        process.start()
        processes.append(process)
        #print(f"... {q+1}/50 process(es) have been submitted: {start_pix} -  {end_pix}...")

    for p in processes:
        p.join()

    result_list = numpy.array(result_list)
    print(result_list.shape)
    ind = numpy.argsort(result_list[:,0])
    tot_likelihood = result_list[ind, 1]
    tot_pix_number = result_list[ind, 0]
    print(tot_pix_number[0:10])
    numpy.save(f"shuffle{shuffle_number}/all_map/test_likelihood_shuffle{shuffle_number}_{file_number}", tot_likelihood)
        
    print(f"... {sample_number*process_num} calculate likelihood ratio ... : {time.time() - start_time:.2f} sec")
