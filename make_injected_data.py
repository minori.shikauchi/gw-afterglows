import numpy
import math
from astropy import units 
from astropy.cosmology import Planck15, z_at_value
import time 
from multiprocessing import Pool, Process
import multiprocessing
import sys
import random
from astropy.cosmology.core import CosmologyError
import os
import subprocess

d_ref = 40 * units.Mpc

consective_rev06 = [1878, 1879, 1889, 1894, 1904, 1906, 1907, 1908, 1909, 1912, 1913, 1914, 1919, 1920, 1921, 1924, 1926, 1927, 1928, 1929, 1931, 1933, 1934, 1935, 1936, 1939,1958, 1959, 1965, 1969, 1970, 1971, 1972, 1975, 1976, 1977, 1978, 1979, 1980, 1983, 1984, 1985, 1988, 1989, 1990, 2012, 2013, 2029, 2030, 2031, 2032, 2039, 2040, 2044, 2046, 2059, 2060, 2072, 2073, 2075, 2077, 2079, 2080, 2081, 2084, 2085, 2086, 2087, 2093, 2096, 2097, 2098, 2099, 2105, 2106, 2107, 2108, 2109, 2111, 2112, 2118, 2121, 2122, 2126, 2127, 2133, 2134, 2135, 2143, 2163, 2166, 2167, 2174, 2175, 2180, 2181, 2182]
day_duration = consective_rev06[-1] - consective_rev06[0] + 1

sigma_orig = 6 * 10**(-3)
t_duration_list = numpy.logspace(0, 3.5, 10)

valid_pix_array = numpy.load("npys/million_valid_pix_array.npy")
corr_pix_array_new_limited = numpy.load("npys/corr_pix_number_for_new_limited_map.npy")
merger_random = numpy.load("npys/million_merger_random.npy")
inj_template_array = numpy.load("npys/million_inj_temp_array.npy")
tot_com_dis = numpy.load("npys/million_tot_com_dis.npy")
#tot_com_dis = numpy.load("npys/com_dis_log_uniform.npy")

def make_injected_data(start_pix, end_pix, result_list, pixel_random, orig_pix, result_list_lock, data_array, dir_path, option):
    for k in range(start_pix, end_pix):
        if option == "limited_map":
            pixel_random_number = numpy.where(corr_pix_array_new_limited == int(pixel_random[k]))[0][0]
            ind_pixel = numpy.where(orig_pix == int(pixel_random[k]))[0]
        else:
            pixel_random_number = int(pixel_random[k])
            ind_pixel = numpy.where(orig_pix == pixel_random_number)[0]

        for x in ind_pixel:
            inj_template = inj_template_array[x]
            merger_random_number = merger_random[x]
        
            scrambled_data = data_array[:,pixel_random_number].copy()

            com_dis = tot_com_dis[x] * units.Mpc
            try:
                z = z_at_value(Planck15.comoving_distance, com_dis)
            except CosmologyError:
                z = 0
                
            lum_dis = Planck15.luminosity_distance(z)
            beta = (1 + z)*(d_ref/lum_dis)**2
        
            scrambled_data[merger_random_number:] += (beta.value*inj_template)[:305-merger_random_number]
            scrambled_data -= numpy.nanmedian(scrambled_data)

            result_list_lock.acquire()
            result_list.append((pixel_random[k], merger_random_number, beta.value, tot_com_dis[x], x, inj_template, scrambled_data))
            result_list_lock.release()

if __name__ == "__main__":
    print("... starting ...")
    pixel_random = numpy.array(list(set(valid_pix_array)))
    sorted_valid_pix_array = numpy.sort(valid_pix_array)
    sorted_pixel_random = numpy.sort(pixel_random)
    print("... sorted ...")

    argvs = sys.argv
    option = argvs[1]
    part_num = int(argvs[2])

    if option == "real_data":
        dir_path = "not_perfect_temp_bank/all_map/true_uniform/"
        million_dir_path = "not_perfect_temp_bank/all_map/true_uniform/million"
        data_path = os.path.join(dir_path, "no_inj_all_map.npy")
        data_array = numpy.load(data_path)
    elif option == "gaussian":
        real_dir_path = "not_perfect_temp_bank/all_map/true_uniform/"
        real_data_path = os.path.join(real_dir_path, "no_inj_all_map.npy")
        real_data_array = numpy.load(real_data_path)

        dir_path = "gaussian_noise/all_at_once/data_sigma/not_perfect_temp_bank/true_uniform/"
        million_dir_path = "gaussian_noise/all_at_once/data_sigma/not_perfect_temp_bank/true_uniform/million"
        '''
        data_array = numpy.random.normal(0, sigma_orig, 305*4096*1024)
        data_array = data_array.reshape(305, 4096*1024)

        ind = numpy.isnan(real_data_array)
        data_array[ind] = numpy.nan
        assert (numpy.isnan(real_data_array) == numpy.isnan(data_array)).all(), "nan values should be in the same indices!"
        #numpy.save(os.path.join(dir_path, "no_inj_gaussian_noise"), data_array)
        '''
        data_path = os.path.join(dir_path, "no_inj_gaussian_noise.npy")
        data_array = numpy.load(data_path)
    elif option == "limited_map":
        pixel_random = numpy.array(list(set(corr_pix_array_new_limited) & set(valid_pix_array)))
        sorted_valid_pix_array = numpy.sort(valid_pix_array)
        sorted_pixel_random = numpy.sort(pixel_random)

        dir_path = "new_limited_map/"
        data_path = os.path.join(dir_path, "no_inj_limited_map_new.npy")
        data_array = numpy.load(data_path)
    elif option == "no_mask_gaussian":
        dir_path = "no_mask/"
        million_dir_path = "no_mask/million"
        #data_array = numpy.random.normal(0, sigma_orig, 305*4096*1024)
        #data_array = data_array.reshape(305, 4096*1024)

        #numpy.save(os.path.join(million_dir_path, "no_inj_gaussian_noise_no_mask"), data_array)
        data_array = numpy.load(os.path.join(dir_path, "no_inj_gaussian_noise_no_mask.npy"))

    '''
    print("... counting up the number of injected sources ...")
    total_inj_num = 0
    for i in sorted_pixel_random:
        ind = (valid_pix_array == i)
        total_inj_num += valid_pix_array[ind].shape[0]
    '''

    start_time = time.time()
    manager = multiprocessing.Manager()
    result_list = manager.list()
    result_list_lock = manager.Lock()

    processes = []

    print(f"... make injected data, pix number = {sorted_pixel_random.shape[0]}, # of sources = {len(valid_pix_array)} ...")

    process_num = 32
    #sample_number = len(sorted_pixel_random)//process_num + 1
    sample_number = 32

    for q in range(process_num):
        #start_pix = sample_number*q 
        start_pix = process_num*sample_number*part_num + sample_number*q
        end_pix = min(start_pix + sample_number, len(sorted_pixel_random))

        process = Process(target=make_injected_data, kwargs={'start_pix': start_pix, 'end_pix': end_pix, 'result_list': result_list, 'pixel_random': sorted_pixel_random, "orig_pix": sorted_valid_pix_array, 'result_list_lock':result_list_lock, "data_array":data_array, "dir_path":dir_path, "option": option})
        process.start()
        processes.append(process)
        print(f"... {q+1}/{process_num} process(es) have been submitted: {start_pix} -  {end_pix}...")
        if end_pix == len(sorted_pixel_random):
            break

    for p in processes:
        p.join()

    result_list = list(result_list)
    result_list.sort(key = lambda row : row[0])
    print(len(result_list))

    inj_pix_number = [row[0] for row in result_list]
    inj_merger_time = [row[1] for row in result_list]
    inj_amp = [row[2] for row in result_list]
    inj_dis = [row[3] for row in result_list]
    number_in_valid = [row[4] for row in result_list]
    injected_temp = [row[5] for row in result_list]
    injected_data = [row[6] for row in result_list]
    injected_data = numpy.array(injected_data).T

    print(inj_pix_number[0:10], inj_merger_time[0:10], inj_dis[0:10], injected_data.shape)
    #numpy.save(os.path.join(million_dir_path, f"injected_pix_{part_num}"), inj_pix_number)
    #numpy.save(os.path.join(million_dir_path, f"injected_data_{part_num}"), injected_data)
    numpy.save(os.path.join(f"npys/million_injected_temp_{option}_{part_num}"), injected_temp)
    #numpy.save(os.path.join(million_dir_path, f"injected_merger_time_{part_num}"), inj_merger_time)
    #numpy.save(os.path.join(million_dir_path, f"injected_amp_{part_num}"), inj_amp)
    #numpy.save(os.path.join(million_dir_path, f"injected_com_dis_{part_num}"), inj_dis)
        
    print(f"... make injected data ... : {time.time() - start_time:.2f} sec")

