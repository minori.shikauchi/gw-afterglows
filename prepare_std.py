import numpy as np
import math
import os
import time

start_time = time.time()

block_num = 3
threshold_rms = 3

print("... loading ...")
dir_path = "not_perfect_temp_bank/all_map/true_uniform/"
data_path = os.path.join(dir_path, "no_inj_all_map.npy")
data_array = np.load(data_path)
#data_array = data_array[:,:10]

print("... remove glitches ...")
for start in range(0, len(data_array), block_num):
    end = min(start+block_num, len(data_array))
    part_data = data_array[start:end,:]
    print(start, part_data)
    
    rms = np.sqrt(np.nanmean(part_data**2,axis=0))
    deviation_ind = (part_data > threshold_rms * rms)
    part_data[deviation_ind] = np.nan
    print(part_data)
    
    data_array[start:end,:] = part_data

std_array = np.nanstd(data_array, axis=0)
assert (np.isnan(std_array) != True).all(), "nan std?"
print(std_array.shape)
np.save("data_based_sigma", std_array)

print(f"... finished ... : {time.time() - start_time} sec")
