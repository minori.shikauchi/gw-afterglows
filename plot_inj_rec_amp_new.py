import matplotlib.pyplot as plt
import numpy as np
import math
from matplotlib import rc
day = "Sep 7"
threshold_array = np.logspace(-1, 2, 50)

rc('font',**{'family':'serif', 'serif':['Computer Modern']})
rc('text', usetex=True)
fontsize = 23
titlesize = 1.2*fontsize
sgrb_count = np.array([279, 893, 2225])

tot_likelihood = np.load("tot_likelihood_all_map.npy")
tot_pix_number = np.load("tot_pix_number_all_map.npy")
valid_pix_array = np.load("valid_pix_array.npy")
#tot_com_dis = np.load("tot_com_dis.npy")

# pipeline tuning
threshold_array = np.logspace(-1, 2, 50)
upper_array_test = np.zeros(len(threshold_array))
upper_array_gaussian_test = np.zeros(len(threshold_array))

norm_much_gaussian_test = 5.*10**(6)

injection_path_gaussian_test = f"gaussian_noise/data_sigma/not_perfect_temp_bank/log_uniform/injection.npy"
injection_Lambda_array_gaussian_test = np.load(injection_path_much_small_gaussian_test)
injection_Lambda_array_gaussian_test *= norm_gaussian_test
no_injection_path_gaussian_test = f"gaussian_noise/data_sigma/not_perfect_temp_bank/log_uniform/no_injection.npy"
no_injection_Lambda_array_gaussian_test = np.load(no_injection_path_much_small_gaussian_test)
no_injection_Lambda_array_gaussian_test *= norm_gaussian_test

for i in range(len(threshold_array)):
    threshold = threshold_array[i]

    injection_path_test = f"not_perfect_temp_bank/all_map/true_uniform/injection_{i}.npy"
    part_Lambda_array_test = np.load(injection_path_test)

    ind_test = (part_Lambda_array_test > threshold)
    time_space_test = 4. * math.pi/3. * (1.5)**3 * part_Lambda_array_test[ind_test].shape[0]/100000 * 305/365.25

    #ind_gaussian_test = ((injection_Lambda_array_gaussian_test > threshold))
    ind_gaussian_test = ((no_injection_Lambda_array_gaussian_test < threshold) & (injection_Lambda_array_gaussian_test > threshold))
    time_space_gaussian_test = 4. * math.pi/3. * (1.5)**3 * injection_Lambda_array_gaussian_test[ind_gaussian_test].shape[0]/100000 * 305/365.25

    print(i, threshold, part_Lambda_array_test[ind_test].shape[0], injection_Lambda_array_gaussian_test[ind_gaussian_test].shape[0])

    try:
        upper_test = 2.303/time_space_test
    except ZeroDivisionError:
        upper_test = np.nan
        
    try:
        upper_gaussian_test = 2.303/time_space_gaussian_test
    except ZeroDivisionError:
        upper_gaussian_test = np.nan
        
    upper_array_test[i] = upper_test
    upper_array_gaussian_test[i] = upper_gaussian_test

print("closed, gaussian")
print(np.nanargmin(upper_array_test), np.nanargmin(upper_array_gaussian_test))
print(threshold_array[np.nanargmin(upper_array_test)], threshold_array[np.nanargmin(upper_array_gaussian_test)])
print(np.nanmin(upper_array_test), np.nanmin(upper_array_gaussian_test))

smallest_ind = np.nanargmin(upper_array_test)
smallest_ind_gaussian = np.nanargmin(upper_array_gaussian_test)
print(smallest_ind, smallest_ind_gaussian, threshold_array[smallest_ind])
threshold_gaussian_test = threshold_array[smallest_ind_gaussian]

rec_amp_gaussian_test_path = f"gaussian_noise/much_small_data_sigma/not_perfect_temp_bank/log_uniform/recovered_amp.npy"
inj_amp_gaussian_test_path = f"gaussian_noise/much_small_data_sigma/not_perfect_temp_bank/log_uniform/injected_amp.npy"
rec_amp_gaussian_test = np.load(rec_amp_gaussian_test_path)
inj_amp_gaussian_test = np.load(inj_amp_gaussian_test_path)

threshold = threshold_array[smallest_ind]
injection_path_test = f"not_perfect_temp_bank/all_map/true_uniform/injection_{smallest_ind}.npy"
part_Lambda_array_test = np.load(injection_path_test)
ind_test = (part_Lambda_array_test > threshold)

rec_amp_test_path = f"not_perfect_temp_bank/all_map/true_uniform/recovered_amp_{smallest_ind}.npy"
inj_amp_test_path = f"not_perfect_temp_bank/all_map/true_uniform/injected_amp_{smallest_ind}.npy"
rec_amp_test = np.load(rec_amp_test_path)
inj_amp_test = np.load(inj_amp_test_path)
rec_amp_test = rec_amp_test[ind_test]
inj_amp_test = inj_amp_test[ind_test]
print(rec_amp_test.shape, rec_amp_gaussian_test.shape, inj_amp_test[:5], inj_amp_gaussian_test[:5])
    
ind_gaussian_test = ((injection_Lambda_array_gaussian_test > threshold) & (no_injection_Lambda_array_gaussian_test < threshold))
rec_amp_gaussian_test = rec_amp_gaussian_test[ind_gaussian_test]
inj_amp_gaussian_test = inj_amp_gaussian_test[ind_gaussian_test]
print(rec_amp_test.shape, rec_amp_gaussian_test.shape, inj_amp_test[:5], inj_amp_gaussian_test[:5])
    
fig = plt.figure(figsize=(12,8))
axis = fig.add_subplot(1,1,1)
#plt.scatter(inj_amp_test, rec_amp_test, c="black", s=15, label="closed box", marker="*",rasterized=True)
plt.scatter(inj_amp_gaussian_test, rec_amp_gaussian_test, c="orangered", s=15, label="Gaussian noise (6 mJy)", marker="*",rasterized=True)
    
plt.xlabel(r"$A_\mathrm{inj}$",fontsize=fontsize)
plt.ylabel(r"$A_\mathrm{rec}$", fontsize=fontsize)
axis.tick_params(axis='x',labelsize=fontsize)
axis.tick_params(axis='y',labelsize=fontsize)
plt.legend(fontsize=fontsize, loc="lower right",markerscale=5)
#axis.set_yticks(np.arange(0, 301, 150))
#axis.set_yticks(np.arange(0, 301, 10), minor=True)
plt.grid()
plt.yscale("log")
plt.xscale("log")
#plt.ylim(10**(-3), 1)

fig.set_facecolor("white")

print(threshold_array[smallest_ind])
#plt.title(f"plotted on {day}, 2023", fontsize=titlesize, y=1.05)
plt.savefig("/home/minorip/pdfs/inj_rec_amp_log.pdf", facecolor="white", bbox_inches="tight", pad_inches=0.3)
