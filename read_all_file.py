import numpy as np
import sys, os

if __name__ == "__main__":
    argvs = sys.argv
    data_type = argvs[1]
    inj_or_no_inj = argvs[2]

    if data_type == "gaussian":
        dir_path = "gaussian_noise/all_at_once/data_sigma/not_perfect_temp_bank/true_uniform/"
    elif data_type == "closed-box":
        dir_path = "not_perfect_temp_bank/all_map/true_uniform/"

    if inj_or_no_inj == "y":
        number = 83
        cont_array = np.array(["lambda", "recovered_merger_time", "recovered_amp", "recovered_const", "lambda_after_shuffling"])
        for cont in cont_array:
            tot_array = np.load(os.path.join(dir_path, f"{cont}_w_inj_0.npy"))
            for i in range(1, number):
                part_array = np.load(os.path.join(dir_path, f"{cont}_w_inj_{i}.npy"))
                tot_array = np.append(tot_array, part_array)

            print(cont, tot_array.shape)
            np.save(os.path.join(dir_path, f"{cont}_all"), tot_array)
    elif inj_or_no_inj == "n":
        dir_path = os.path.join(dir_path, "temp/storage0")

        number = 4195
        cont_array = np.array(["lambda", "lambda_after_shuffling"])
        for cont in cont_array:
            tot_array = np.load(os.path.join(dir_path, f"{cont}_no_inj_0.npy"))
            for i in range(1, number):
                part_array = np.load(os.path.join(dir_path, f"{cont}_no_inj_{i}.npy"))
                tot_array = np.append(tot_array, part_array)

            print(cont, tot_array.shape)
            np.save(os.path.join(dir_path, f"{cont}_all"), tot_array)

