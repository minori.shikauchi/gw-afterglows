import time 
import numpy as np

array = np.arange(10**9)

start_time = time.time()
np.save("test_write", array)
print(time.time() - start_time)
