import mkipp
import matplotlib.pyplot as plt
import numpy as np
import mesa_reader
from matplotlib import rc
from tools import *

rc('font',**{'family':'serif', 'serif':['Computer Modern']})
rc('text', usetex=True)
fontsize = 23
titlesize = 1.2*fontsize
frac_array = np.array([20, 40, 60])
mt_array = np.array([5*10**(-6), 10**(-5), 1.5*10**(-5), 10**(-4)])

def solve_diff_eq(delta_center_he4, delta_mmix, delta_mtot, delta_mmix_wo_mt):
    """try solving a differential equation:
    d \ln m_mix/dY = \beta * d \ln m_tot/dY * (1 - Y)**\gamma + (d \ln m_mix/dY)_natural,
    where Y is a new coordinate of y,
    Y = (y - y_0)/(1 - y_0), y_0 = the initial He fraction,
    beta, gamma are unity for a trial,
    (d \ln m_mix/dY)_natural is (d \ln m_mix/dY) without the MT.
    """
    #beta_now = 1.
    #gamma = 1

    # for 25 M_sun, beta = 1.3, gamma = 1.2 look fine for all the timing.
    # for 30 M_sun, beta = 1.3, gamma = 1.2 look fine for all the timing.
    # for 40 M_sun, beta = 1.3, gamma = 1.2 look fine for all the timing.
    # for 60 M_sun, beta = 1.3, gamma = 2 look fine for all the timing.

    start_center_he4 = delta_center_he4[0]
    end_center_he4 = delta_center_he4[-1]
    initial_mtot = delta_mtot[0]
    delta_center_he4_array = np.linspace(delta_center_he4[0], delta_center_he4[-1], 100)
    delta_mtot_array = np.linspace(delta_mtot[0], delta_mtot[-1], 100)
    delta_mmix_array_wo_mt = np.linspace(delta_mmix_wo_mt[0], delta_mmix_wo_mt[-1], 100)

    incorp_mmix_array = np.zeros(len(delta_center_he4_array))
    incorp_mmix_array[0] = delta_mmix[0]
    for i in range(1, len(delta_center_he4_array)):
        dY = delta_center_he4_array[i] - delta_center_he4_array[i-1]
        dmtot = delta_mtot_array[i] - delta_mtot_array[i-1]
        dmmix_wo_mt = delta_mmix_array_wo_mt[i] - delta_mmix_array_wo_mt[i-1]

        Y = delta_center_he4_array[i-1]
        mtot = delta_mtot_array[i-1]
        mmix = incorp_mmix_array[i-1]
        mmix_wo_mt = delta_mmix_array_wo_mt[i-1]
        beta_now = beta(mtot)

        #delta_mmix = (beta * dmtot/(dY*mtot) * (1-Y)**gamma + dmmix_wo_mt/(dY*mmix_wo_mt)) * dY * mmix
        #delta_mmix = (beta_now * dmtot/(dY*mtot) * (1-Y)**gamma -0.38/(1 - 0.38*Y)) * dY * mmix
        delta_mmix = (beta_now * dmtot/(dY*mtot) * 10**(-Y) -0.38/(1 - 0.38*Y)) * dY * mmix
        incorp_mmix_array[i] = mmix + delta_mmix

    return incorp_mmix_array, delta_center_he4_array

def gamma_value(delta_center_he4, delta_mmix, delta_mtot):
    """try finding a form of 'delta' including any Y dependences from the differential equation
    d \ln m_mix/dY = \beta * d \ln m_tot/dY * delta(Y) + (d \ln m_mix/dY)_natural,
    -> delta(Y) = (d \ln m_mix/dY - d \ln m_tot/dY)/(beta * d \ln m_tot/dY)
    where Y is a new coordinate of y,
    Y = (y - y_0)/(1 - y_0), y_0 = the initial He fraction,
    beta = f_mix \equiv d \ln m_mix,ini/d \ln m_tot,ini,
    (d \ln m_mix/dY)_natural is (d \ln m_mix/dY) without the MT.
    """
    start_center_he4 = delta_center_he4[0]
    end_center_he4 = delta_center_he4[-1]
    initial_mtot = delta_mtot[0]

    delta_array = np.array([])
    incorp_mmix_array_linear = np.array([])
    incorp_mmix_array_pl = np.array([])
    incorp_mmix_array_pl2 = np.array([])
    #incorp_mmix_array_2 = np.array([])

    mmix_term_in_diff = np.array([])
    mtot_term_in_diff = np.array([])
    mmix_natural_term_in_diff = np.array([])
    valid_Y_array = np.array([])

    #incorp_mmix_array = np.append(incorp_mmix_array, delta_mmix[0])
    #incorp_mmix_array_2 = np.append(incorp_mmix_array_2, delta_mmix[0])

    dmmix_threshold = 10**(-5)
    #print(f"beta = {beta_now} @ ini mass = {initial_mtot} M_sun ...")

    prev_ind = 1
    #Y_now = delta_center_he4[prev_ind]
    #mtot_now = delta_mtot[prev_ind]
    #mmix_now = delta_mmix[prev_ind]
    #valid_Y_array = np.append(valid_Y_array, Y_now)
    #incorp_mmix_array = np.append(incorp_mmix_array, mmix_now)
    #incorp_mmix_array_2 = np.append(incorp_mmix_array_2, mmix_now)
    for i in range(1, len(delta_center_he4)):
        if i < prev_ind or prev_ind > len(delta_center_he4) - 1:
            pass
        else:
            #print(f".... No. {prev_ind}/{len(delta_center_he4)} ...")
            dmmix = delta_mmix[prev_ind] - delta_mmix[prev_ind-1]
            dY = delta_center_he4[prev_ind] - delta_center_he4[prev_ind-1]
            dmtot = delta_mtot[prev_ind] - delta_mtot[prev_ind-1]

            Y_prev = delta_center_he4[prev_ind-1]
            mtot_prev = delta_mtot[prev_ind-1]
            mmix_prev = delta_mmix[prev_ind-1]
            Y_now = delta_center_he4[prev_ind]
            mtot_now = delta_mtot[prev_ind]
            mmix_now = delta_mmix[prev_ind]
            beta_now = beta(mtot_prev)

            n = 0
            while abs(dmmix) < dmmix_threshold and prev_ind+n < len(delta_center_he4):
                dmmix = delta_mmix[prev_ind+n] - delta_mmix[prev_ind-1]
                dY = delta_center_he4[prev_ind+n] - delta_center_he4[prev_ind-1]
                dmtot = delta_mtot[prev_ind+n] - delta_mtot[prev_ind-1]

                Y_now = delta_center_he4[prev_ind+n]
                mtot_now = delta_mtot[prev_ind+n]
                mmix_now = delta_mmix[prev_ind+n]
                n += 1

            prev_ind = i+n+1

            #incorp_delta_mmix_2 = (beta_now * dmtot/(dY*mtot_prev) * (1-Y_prev) -0.38/(1 - 0.38*Y_prev)) * dY * mmix_prev
            #incorp_delta_mmix = (beta_now * (math.log(mtot_now) - math.log(mtot_prev))/dY * (1-Y_prev) -0.38/(1 - 0.38*Y_prev)) * dY
            incorp_delta_mmix_linear = (beta_now * (math.log(mtot_now) - math.log(mtot_prev))/dY * (1 - Y_prev) -0.38/(1 - 0.38*Y_prev)) * dY
            incorp_delta_mmix_pl = (beta_now * (math.log(mtot_now) - math.log(mtot_prev))/dY * 10**(-Y_prev) -0.38/(1 - 0.38*Y_prev)) * dY
            incorp_delta_mmix_pl2 = (beta_now * (math.log(mtot_now) - math.log(mtot_prev))/dY * 10**(-1.2*Y_prev) -0.38/(1 - 0.38*Y_prev)) * dY
            #delta = (dmmix/(dY*mmix) + 0.4/(1 - 0.4*Y))/(beta_now * dmtot/(dY*mtot))

            mmix_change_w_mt = (math.log(mmix_now) - math.log(mmix_prev))/dY 
            mmix_change_natural = - 0.38/(1 - 0.38*Y_prev)
            mtot_change_w_mt = (beta_now * (math.log(mtot_now) - math.log(mtot_prev))/dY)
            delta = ((math.log(mmix_now) - math.log(mmix_prev))/dY + 0.38/(1 - 0.38*Y_prev))/(beta_now * (math.log(mtot_now) - math.log(mtot_prev))/dY)

            valid_Y_array = np.append(valid_Y_array, Y_now)
            delta_array = np.append(delta_array, delta)
            incorp_mmix_array_linear = np.append(incorp_mmix_array_linear, math.exp(math.log(mmix_prev) + incorp_delta_mmix_linear))
            incorp_mmix_array_pl = np.append(incorp_mmix_array_pl, math.exp(math.log(mmix_prev) + incorp_delta_mmix_pl))
            incorp_mmix_array_pl2 = np.append(incorp_mmix_array_pl2, math.exp(math.log(mmix_prev) + incorp_delta_mmix_pl2))
            #incorp_mmix_array_2 = np.append(incorp_mmix_array_2, mmix_prev + incorp_delta_mmix)
            mmix_term_in_diff = np.append(mmix_term_in_diff, mmix_change_w_mt)
            mtot_term_in_diff = np.append(mtot_term_in_diff, mtot_change_w_mt)
            mmix_natural_term_in_diff = np.append(mmix_natural_term_in_diff, mmix_change_natural)

    return delta_array, incorp_mmix_array_linear, incorp_mmix_array_pl, incorp_mmix_array_pl2, mmix_term_in_diff, mtot_term_in_diff, mmix_natural_term_in_diff, valid_Y_array

fig = plt.figure(figsize=(12*3,8*2))
metal = 0.02
L_sun = 3.828*10**(33) # erg/s
nuc_CNO = 6.019*10**(18) # erg/s, emitted energy when 1 g of Hydrogen is burned in CNO cycle

model = "60_ms"
threshold = 0.9
if model == "25_ms":
    label = "$m_\mathrm{ini} = 25 M_\odot$"
    mtot_b_mt = 25
    mtot_a_mt = 13.71
elif model == "30_ms":
    label = "$m_\mathrm{ini} = 30 M_\odot$"
    mtot_b_mt = 30
    mtot_a_mt = 17.71
elif model == "40_ms":
    label = "$m_\mathrm{ini} = 40 M_\odot$"
    mtot_b_mt = 40
    mtot_a_mt = 26.28
elif model == "50_ms":
    label = "$m_\mathrm{ini} = 40 M_\odot$"
    mtot_b_mt = 50
    mtot_a_mt = 35.27
elif model == "60_ms":
    label = "$m_\mathrm{ini} = 60 M_\odot$"
    mtot_b_mt = 60
    mtot_a_mt = 44.50
log = f"wo_mt/LOG_{model}/history.data"

age_array_wo_mt = []
mconv_array_wo_mt = []
m_array_wo_mt = []
center_h1_array_wo_mt = []
center_he4_array_wo_mt = []
mmix_array_wo_mt = []
ltot_array_wo_mt = []
lh_array_wo_mt = []
lnuc_array_wo_mt = []
with open(log, "r") as f_log:
    lines = f_log.readlines()
    for i in range(6,len(lines)):
        line = lines[i].strip().split()
        age = float(line[2])
        tot_mass = float(line[4])
        conv_mass = float(line[7])
        center_h1 = float(line[300])
        center_he4 = float(line[301])
        mmix = float(line[310])
        ltot = 10**float(line[290])
        lh = 10**float(line[144])
        lnuc = 10**float(line[147])

        if lh > threshold*lnuc:
            age_array_wo_mt.append(age/10**6)
            mconv_array_wo_mt.append(conv_mass)
            m_array_wo_mt.append(tot_mass)
            center_h1_array_wo_mt.append(center_h1)
            center_he4_array_wo_mt.append(center_he4)
            mmix_array_wo_mt.append(mmix)
            ltot_array_wo_mt.append(ltot)
            lh_array_wo_mt.append(lh)
            lnuc_array_wo_mt.append(lnuc)

mconv_array_wo_mt = np.array(mconv_array_wo_mt)
m_array_wo_mt = np.array(m_array_wo_mt)
age_array_wo_mt = np.array(age_array_wo_mt)
center_he4_array_wo_mt = np.array(center_he4_array_wo_mt)
center_h1_array_wo_mt = np.array(center_h1_array_wo_mt)
mmix_array_wo_mt = np.array(mmix_array_wo_mt)
ltot_array_wo_mt = np.array(ltot_array_wo_mt)

for q in range(len(frac_array)):
    frac = frac_array[q]

    wind_model = f"half_at_{frac}pc_use"
    log = f"w_mt/{model}/LOG_{wind_model}/history.data"

    age_array_half_40 = []
    mconv_array_half_40 = []
    m_array_half_40 = []
    center_h1_array_half_40 = []
    center_he4_array_half_40 = []
    mmix_array_half_40 = []
    ltot_array_half_40 = []
    lh_array_half_40 = []
    lnuc_array_half_40 = []
    with open(log, "r") as f_log:
        lines = f_log.readlines()
        for i in range(6,len(lines)):
            line = lines[i].strip().split()
            age = float(line[2])
            tot_mass = float(line[4])
            conv_mass = float(line[7])
            center_h1 = float(line[300])
            center_he4 = float(line[301])
            mmix = float(line[310])
            ltot = 10**float(line[290])
            lh = 10**float(line[144])
            lnuc = 10**float(line[147])

            if lh > threshold*lnuc:
                age_array_half_40.append(age/10**6)
                mconv_array_half_40.append(conv_mass)
                m_array_half_40.append(tot_mass)
                center_h1_array_half_40.append(center_h1)
                center_he4_array_half_40.append(center_he4)
                mmix_array_half_40.append(mmix)
                ltot_array_half_40.append(ltot)
                lh_array_half_40.append(lh)
                lnuc_array_half_40.append(lnuc)

    wind_model = f"fiducial_at_{frac}pc_use"
    log = f"w_mt/{model}/LOG_{wind_model}/history.data"

    age_array_fiducial_40 = []
    mconv_array_fiducial_40 = []
    m_array_fiducial_40 = []
    center_h1_array_fiducial_40 = []
    center_he4_array_fiducial_40 = []
    mmix_array_fiducial_40 = []
    ltot_array_fiducial_40 = []
    lh_array_fiducial_40 = []
    lnuc_array_fiducial_40 = []
    with open(log, "r") as f_log:
        lines = f_log.readlines()
        for i in range(6,len(lines)):
            line = lines[i].strip().split()
            age = float(line[2])
            tot_mass = float(line[4])
            conv_mass = float(line[7])
            center_h1 = float(line[300])
            center_he4 = float(line[301])
            mmix = float(line[310])
            ltot = 10**float(line[290])
            lh = 10**float(line[144])
            lnuc = 10**float(line[147])

            if lh > threshold*lnuc:
                age_array_fiducial_40.append(age/10**6)
                mconv_array_fiducial_40.append(conv_mass)
                m_array_fiducial_40.append(tot_mass)
                center_h1_array_fiducial_40.append(center_h1)
                center_he4_array_fiducial_40.append(center_he4)
                mmix_array_fiducial_40.append(mmix)
                ltot_array_fiducial_40.append(ltot)
                lh_array_fiducial_40.append(lh)
                lnuc_array_fiducial_40.append(lnuc)

    wind_model = f"high_at_{frac}pc_use"
    log = f"w_mt/{model}/LOG_{wind_model}/history.data"

    age_array_high_40 = []
    mconv_array_high_40 = []
    m_array_high_40 = []
    center_h1_array_high_40 = []
    center_he4_array_high_40 = []
    mmix_array_high_40 = []
    ltot_array_high_40 = []
    lh_array_high_40 = []
    lnuc_array_high_40 = []
    with open(log, "r") as f_log:
        lines = f_log.readlines()
        for i in range(6,len(lines)):
            line = lines[i].strip().split()
            age = float(line[2])
            tot_mass = float(line[4])
            conv_mass = float(line[7])
            center_h1 = float(line[300])
            center_he4 = float(line[301])
            mmix = float(line[310])
            ltot = 10**float(line[290])
            lh = 10**float(line[144])
            lnuc = 10**float(line[147])

            if lh > threshold*lnuc:
                age_array_high_40.append(age/10**6)
                mconv_array_high_40.append(conv_mass)
                m_array_high_40.append(tot_mass)
                center_h1_array_high_40.append(center_h1)
                center_he4_array_high_40.append(center_he4)
                mmix_array_high_40.append(mmix)
                ltot_array_high_40.append(ltot)
                lh_array_high_40.append(lh)
                lnuc_array_high_40.append(lnuc)

    wind_model = f"15_at_{frac}pc_use"
    log = f"w_mt/{model}/LOG_{wind_model}/history.data"

    age_array_15_40 = []
    mconv_array_15_40 = []
    m_array_15_40 = []
    center_h1_array_15_40 = []
    center_he4_array_15_40 = []
    mmix_array_15_40 = []
    ltot_array_15_40 = []
    lh_array_15_40 = []
    lnuc_array_15_40 = []
    with open(log, "r") as f_log:
        lines = f_log.readlines()
        for i in range(6,len(lines)):
            line = lines[i].strip().split()
            age = float(line[2])
            tot_mass = float(line[4])
            conv_mass = float(line[7])
            center_h1 = float(line[300])
            center_he4 = float(line[301])
            mmix = float(line[310])
            ltot = 10**float(line[290])
            lh = 10**float(line[144])
            lnuc = 10**float(line[147])
    
            if lh > threshold*lnuc:
                age_array_15_40.append(age/10**6)
                mconv_array_15_40.append(conv_mass)
                m_array_15_40.append(tot_mass)
                center_h1_array_15_40.append(center_h1)
                center_he4_array_15_40.append(center_he4)
                mmix_array_15_40.append(mmix)
                ltot_array_15_40.append(ltot)
                lh_array_15_40.append(lh)
                lnuc_array_15_40.append(lnuc)

    mconv_array_fiducial_40 = np.array(mconv_array_fiducial_40)
    mconv_array_half_40 = np.array(mconv_array_half_40)
    mconv_array_high_40 = np.array(mconv_array_high_40)
    mconv_array_15_40 = np.array(mconv_array_15_40)
    mconv_array_wo_mt = np.array(mconv_array_wo_mt)

    ltot_array_fiducial_40 = np.array(ltot_array_fiducial_40)
    ltot_array_half_40 = np.array(ltot_array_half_40)
    ltot_array_high_40 = np.array(ltot_array_high_40)
    ltot_array_15_40 = np.array(ltot_array_15_40)

    m_array_fiducial_40 = np.array(m_array_fiducial_40)
    m_array_half_40 = np.array(m_array_half_40)
    m_array_high_40 = np.array(m_array_high_40)
    m_array_15_40 = np.array(m_array_15_40)

    age_array_fiducial_40 = np.array(age_array_fiducial_40)
    age_array_half_40 = np.array(age_array_half_40)
    age_array_high_40 = np.array(age_array_high_40)
    age_array_15_40 = np.array(age_array_15_40)

    center_he4_array_fiducial_40 = np.array(center_he4_array_fiducial_40)
    center_he4_array_half_40 = np.array(center_he4_array_half_40)
    center_he4_array_high_40 = np.array(center_he4_array_high_40)
    center_he4_array_15_40 = np.array(center_he4_array_15_40)
    center_he4_array_wo_mt = np.array(center_he4_array_wo_mt)

    initial_center_he4_fiducial_40 = center_he4_array_fiducial_40[0]
    initial_center_he4_half_40 = center_he4_array_half_40[0]
    initial_center_he4_15_40 = center_he4_array_15_40[0]
    initial_center_he4_high_40 = center_he4_array_high_40[0]
    initial_center_he4_wo_mt = center_he4_array_wo_mt[0]
    print(initial_center_he4_fiducial_40, initial_center_he4_half_40, initial_center_he4_15_40, initial_center_he4_high_40, initial_center_he4_wo_mt)
    exit()
    if q == 0:
        initial_Y = initial_center_he4_wo_mt

    # assign new coordinate, Y
    center_he4_array_fiducial_40 = (center_he4_array_fiducial_40 - initial_center_he4_fiducial_40)/(1 - initial_center_he4_fiducial_40 - metal)
    center_he4_array_half_40 = (center_he4_array_half_40 - initial_center_he4_half_40)/(1 - initial_center_he4_half_40 - metal)
    center_he4_array_15_40 = (center_he4_array_15_40 - initial_center_he4_15_40)/(1 - initial_center_he4_15_40 - metal)
    center_he4_array_high_40 = (center_he4_array_high_40 - initial_center_he4_high_40)/(1 - initial_center_he4_high_40 - metal)
    center_he4_array_wo_mt = (center_he4_array_wo_mt - initial_center_he4_wo_mt)/(1 - initial_center_he4_wo_mt - metal)

    center_h1_array_fiducial_40 = np.array(center_h1_array_fiducial_40)
    center_h1_array_half_40 = np.array(center_h1_array_half_40)
    center_h1_array_high_40 = np.array(center_h1_array_high_40)
    center_h1_array_15_40 = np.array(center_h1_array_15_40)

    mmix_array_fiducial_40 = np.array(mmix_array_fiducial_40)
    mmix_array_half_40 = np.array(mmix_array_half_40)
    mmix_array_high_40 = np.array(mmix_array_high_40)
    mmix_array_15_40 = np.array(mmix_array_15_40)

    b_ind_half_40 = (m_array_half_40 >= mtot_b_mt)
    b_ind_fiducial_40 = (m_array_fiducial_40 >= mtot_b_mt)
    b_ind_15_40 = (m_array_15_40 >= mtot_b_mt)
    b_ind_high_40 = (m_array_high_40 >= mtot_b_mt)

    a_ind_half_40 = np.argmin(np.abs(m_array_half_40 - mtot_a_mt))
    a_ind_fiducial_40 = np.argmin(np.abs(m_array_fiducial_40 - mtot_a_mt))
    a_ind_15_40 = np.argmin(np.abs(m_array_15_40 - mtot_a_mt))
    a_ind_high_40 = np.argmin(np.abs(m_array_high_40 - mtot_a_mt))

    mmix_b_half_40 = mmix_array_half_40[b_ind_half_40][-1]
    mmix_b_fiducial_40 = mmix_array_fiducial_40[b_ind_fiducial_40][-1]
    mmix_b_15_40 = mmix_array_15_40[b_ind_15_40][-1]
    mmix_b_high_40 = mmix_array_high_40[b_ind_high_40][-1]

    m_b_half_40 = m_array_half_40[b_ind_half_40][-1]
    m_b_fiducial_40 = m_array_fiducial_40[b_ind_fiducial_40][-1]
    m_b_15_40 = m_array_15_40[b_ind_15_40][-1]
    m_b_high_40 = m_array_high_40[b_ind_high_40][-1]

    center_he4_b_half_40 = center_he4_array_half_40[b_ind_half_40][-1]
    center_he4_b_fiducial_40 = center_he4_array_fiducial_40[b_ind_fiducial_40][-1]
    center_he4_b_15_40 = center_he4_array_15_40[b_ind_15_40][-1]
    center_he4_b_high_40 = center_he4_array_high_40[b_ind_high_40][-1]

    ind_half_wo_mt = np.argmin(np.abs(center_he4_array_wo_mt - center_he4_b_half_40))
    ind_fiducial_wo_mt = np.argmin(np.abs(center_he4_array_wo_mt - center_he4_b_fiducial_40))
    ind_15_wo_mt = np.argmin(np.abs(center_he4_array_wo_mt - center_he4_b_15_40))
    ind_high_wo_mt = np.argmin(np.abs(center_he4_array_wo_mt - center_he4_b_high_40))
    mmix_b_half_40_wo_mt = mmix_array_wo_mt[ind_half_wo_mt]
    mmix_b_fiducial_40_wo_mt = mmix_array_wo_mt[ind_fiducial_wo_mt]
    mmix_b_15_40_wo_mt = mmix_array_wo_mt[ind_15_wo_mt]
    mmix_b_high_40_wo_mt = mmix_array_wo_mt[ind_high_wo_mt]

    mmix_a_half_40 = mmix_array_half_40[a_ind_half_40]
    m_a_half_40 = m_array_half_40[a_ind_half_40]
    center_he4_a_half_40 = center_he4_array_half_40[a_ind_half_40]

    #print(np.where(b_ind_half_40)[0][-1], a_ind_half_40)
    delta_center_he4_half_40 = center_he4_array_half_40[np.where(b_ind_half_40)[0][-1]:a_ind_half_40]
    delta_mmix_half_40 = mmix_array_half_40[np.where(b_ind_half_40)[0][-1]:a_ind_half_40]
    delta_mtot_half_40 = m_array_half_40[np.where(b_ind_half_40)[0][-1]:a_ind_half_40]
    if np.min(np.abs(m_array_half_40 - mtot_a_mt)) > mtot_a_mt * 10**(-3):
        max_center_he4 = np.max(center_he4_array_half_40)
        new_ind = np.argmin(np.abs(center_he4_array_half_40 - max_center_he4*0.99))
        #print(np.where(b_ind_half_40)[0][-1], np.where(new_ind)[0][0])
        mmix_a_half_40 = mmix_array_half_40[new_ind]
        center_he4_a_half_40 = center_he4_array_half_40[new_ind]
        delta_center_he4_half_40 = center_he4_array_half_40[np.where(b_ind_half_40)[0][-1]:new_ind]
        delta_mmix_half_40 = mmix_array_half_40[np.where(b_ind_half_40)[0][-1]:new_ind]
        delta_mtot_half_40 = m_array_half_40[np.where(b_ind_half_40)[0][-1]:new_ind]

    mmix_a_fiducial_40 = mmix_array_fiducial_40[a_ind_fiducial_40]
    m_a_fiducial_40 = m_array_fiducial_40[a_ind_fiducial_40]
    center_he4_a_fiducial_40 = center_he4_array_fiducial_40[a_ind_fiducial_40]
    delta_center_he4_fiducial_40 = center_he4_array_fiducial_40[np.where(b_ind_fiducial_40)[0][-1]:a_ind_fiducial_40]
    delta_mmix_fiducial_40 = mmix_array_fiducial_40[np.where(b_ind_fiducial_40)[0][-1]:a_ind_fiducial_40]
    delta_mtot_fiducial_40 = m_array_fiducial_40[np.where(b_ind_fiducial_40)[0][-1]:a_ind_fiducial_40]
    if np.min(np.abs(m_array_fiducial_40 - mtot_a_mt)) > mtot_a_mt * 10**(-3):
        max_center_he4 = np.max(center_he4_array_fiducial_40)
        new_ind = np.argmin(np.abs(center_he4_array_fiducial_40 - max_center_he4*0.99))
        mmix_a_fiducial_40 = mmix_array_fiducial_40[new_ind]
        center_he4_a_fiducial_40 = center_he4_array_fiducial_40[new_ind]
        delta_center_he4_fiducial_40 = center_he4_array_fiducial_40[np.where(b_ind_fiducial_40)[0][-1]:new_ind]
        delta_mmix_fiducial_40 = mmix_array_fiducial_40[np.where(b_ind_fiducial_40)[0][-1]:new_ind]
        delta_mtot_fiducial_40 = m_array_fiducial_40[np.where(b_ind_fiducial_40)[0][-1]:new_ind]

    mmix_a_15_40 = mmix_array_15_40[a_ind_15_40]
    m_a_15_40 = m_array_15_40[a_ind_15_40]
    center_he4_a_15_40 = center_he4_array_15_40[a_ind_15_40]
    delta_center_he4_15_40 = center_he4_array_15_40[np.where(b_ind_15_40)[0][-1]:a_ind_15_40]
    delta_mmix_15_40 = mmix_array_15_40[np.where(b_ind_15_40)[0][-1]:a_ind_15_40]
    delta_mtot_15_40 = m_array_15_40[np.where(b_ind_15_40)[0][-1]:a_ind_15_40]
    if np.min(np.abs(m_array_15_40 - mtot_a_mt)) > mtot_a_mt * 10**(-3):
        max_center_he4 = np.max(center_he4_array_15_40)
        new_ind = np.argmin(np.abs(center_he4_array_15_40 - max_center_he4*0.99))
        mmix_a_15_40 = mmix_array_15_40[new_ind]
        center_he4_a_15_40 = center_he4_array_15_40[new_ind]
        delta_center_he4_15_40 = center_he4_array_15_40[np.where(b_ind_15_40)[0][-1]:new_ind]
        delta_mmix_15_40 = mmix_array_15_40[np.where(b_ind_15_40)[0][-1]:new_ind]
        delta_mtot_15_40 = m_array_15_40[np.where(b_ind_15_40)[0][-1]:new_ind]

    mmix_a_high_40 = mmix_array_high_40[a_ind_high_40]
    m_a_high_40 = m_array_high_40[a_ind_high_40]
    center_he4_a_high_40 = center_he4_array_high_40[a_ind_high_40]
    delta_center_he4_high_40 = center_he4_array_high_40[np.where(b_ind_high_40)[0][-1]:a_ind_high_40]
    delta_mmix_high_40 = mmix_array_high_40[np.where(b_ind_high_40)[0][-1]:a_ind_high_40]
    delta_mtot_high_40 = m_array_high_40[np.where(b_ind_high_40)[0][-1]:a_ind_high_40]
    if np.min(np.abs(m_array_high_40 - mtot_a_mt)) > mtot_a_mt * 10**(-3):
        max_center_he4 = np.max(center_he4_array_high_40)
        new_ind = np.argmin(np.abs(center_he4_array_high_40 - max_center_he4*0.99))
        mmix_a_high_40 = mmix_array_high_40[new_ind]
        center_he4_a_high_40 = center_he4_array_high_40[new_ind]
        delta_center_he4_high_40 = center_he4_array_high_40[np.where(b_ind_high_40)[0][-1]:new_ind]
        delta_mmix_high_40 = mmix_array_high_40[np.where(b_ind_high_40)[0][-1]:new_ind]
        delta_mtot_high_40 = m_array_high_40[np.where(b_ind_high_40)[0][-1]:new_ind]

    fig2 = plt.figure(figsize=(12,8))
    axis = fig2.add_subplot(1,1,1)
    delta_array_half_40,incorp_mmix_array_linear_half_40, incorp_mmix_array_pl_half_40, incorp_mmix_array_pl2_half_40, mmix_term_half_40, mtot_term_half_40, mmix_natural_half_40, valid_Y_array_half_40 = gamma_value(delta_center_he4_half_40, delta_mmix_half_40, delta_mtot_half_40)
    #print(f"half ... mmix in MESA = {delta_mmix_half_40[-1]:.3f} M_sun, mmix incorpolated = {incorp_mmix_array_half_40[-1]:.3f} M_sun, delta = {abs(delta_mmix_half_40[-1] - incorp_mmix_array_half_40[-1]):.3f} M_sun.")
    delta_array_fiducial_40,incorp_mmix_array_linear_fiducial_40, incorp_mmix_array_pl_fiducial_40, incorp_mmix_array_pl2_fiducial_40, mmix_term_fiducial_40, mtot_term_fiducial_40, mmix_natural_fiducial_40, valid_Y_array_fiducial_40 = gamma_value(delta_center_he4_fiducial_40, delta_mmix_fiducial_40, delta_mtot_fiducial_40)
    #print(f"fiducial ... mmix in MESA = {delta_mmix_fiducial_40[-1]:.3f} M_sun, mmix incorpolated = {incorp_mmix_array_fiducial_40[-1]:.3f} M_sun, delta = {abs(delta_mmix_fiducial_40[-1] - incorp_mmix_array_fiducial_40[-1]):.3f} M_sun.")
    delta_array_15_40,incorp_mmix_array_linear_15_40, incorp_mmix_array_pl_15_40, incorp_mmix_array_pl2_15_40, mmix_term_15_40, mtot_term_15_40, mmix_natural_15_40, valid_Y_array_15_40 = gamma_value(delta_center_he4_15_40, delta_mmix_15_40, delta_mtot_15_40)
    #print(f"1.5x ... mmix in MESA = {delta_mmix_15_40[-1]:.3f} M_sun, mmix incorpolated = {incorp_mmix_array_15_40[-1]:.3f} M_sun, delta = {abs(delta_mmix_15_40[-1] - incorp_mmix_array_15_40[-1]):.3f} M_sun.")
    delta_array_high_40,incorp_mmix_array_linear_high_40, incorp_mmix_array_pl_high_40, incorp_mmix_array_pl2_high_40, mmix_term_high_40, mtot_term_high_40, mmix_natural_high_40, valid_Y_array_high_40 = gamma_value(delta_center_he4_high_40, delta_mmix_high_40, delta_mtot_high_40)
    #print(f"high ... mmix in MESA = {delta_mmix_high_40[-1]:.3f} M_sun, mmix incorpolated = {incorp_mmix_array_high_40[-1]:.3f} M_sun, delta = {abs(delta_mmix_high_40[-1] - incorp_mmix_array_high_40[-1]):.3f} M_sun.")

    """
    plt.plot(delta_center_he4_half_40, delta_mmix_half_40, lw=3, color="black", label="half")
    plt.scatter(valid_Y_array_half_40, incorp_mmix_array_half_40, s=80, marker="*",color="black", linestyle="dashed", label="half (incorp.)")

    plt.plot(delta_center_he4_fiducial_40, delta_mmix_fiducial_40, lw=3, color="orangered", label="fiducial")
    plt.scatter(valid_Y_array_fiducial_40, incorp_mmix_array_fiducial_40, s=80, marker="*", color="orangered", linestyle="dashed", label="fiducial (incorp.)")

    plt.plot(delta_center_he4_15_40, delta_mmix_15_40, lw=3, color="royalblue", label="1.5x")
    plt.scatter(valid_Y_array_15_40, incorp_mmix_array_15_40, s=80, marker="*", color="royalblue", linestyle="dashed", label="1.5x (incorp.)")

    print(delta_center_he4_high_40, valid_Y_array_high_40)
    plt.plot(delta_center_he4_high_40, delta_mmix_high_40, lw=3, color="forestgreen", label="high")
    plt.scatter(valid_Y_array_high_40, incorp_mmix_array_high_40, s=80, marker="*", color="forestgreen", linestyle="dashed", label="high (incorp.)")

    axis.set_xlabel(r"$Y$", fontsize=fontsize)
    axis.set_ylabel(r"$m_\mathrm{mix} $ [$M_\odot$]", fontsize=fontsize)
    axis.tick_params(axis="both", which="major", labelsize=fontsize)
    axis.set_yticks(np.arange(0,61,10))
    axis.set_yticks(np.arange(0,61,2),minor=True)
    axis.set_xticks(np.arange(0,1.1,0.5))
    axis.set_xticks(np.arange(0,1.1,0.1),minor=True)
    plt.yscale("log")
    plt.ylim(0, 61)

    #plt.legend(loc="upper right", fontsize=fontsize, ncol=2)
    plt.legend(loc="lower left", fontsize=fontsize, ncol=2)
    plt.grid()

    #plt.subplots_adjust()
    #figname = rf"pngs/dMmixdY_dMdt_func_{model}_normed_w_ini_value"
    print(f"pngs/time_evolution_tot_mass_{model}_{frac}pc_w_diff_eq_update_alpha.png")
    figname = f"pngs/time_evolution_tot_mass_{model}_{frac}pc_w_diff_eq_update_alpha.png"
    print(figname)
    plt.savefig(figname,facecolor="white", bbox_inches="tight", pad_inches=0.3)
    """
axis.set_xlabel(r"$Y$", fontsize=fontsize)
axis.set_ylabel(r"$m_\mathrm{mix} $ [$M_\odot$]", fontsize=fontsize)
axis.tick_params(axis="both", which="major", labelsize=fontsize)
axis.set_yticks(np.arange(0,61,10))
axis.set_yticks(np.arange(0,61,2),minor=True)
axis.set_xticks(np.arange(0,1.1,0.5))
axis.set_xticks(np.arange(0,1.1,0.1),minor=True)
plt.yscale("log")
plt.ylim(0, 61)

#plt.legend(loc="upper right", fontsize=fontsize, ncol=2)
plt.legend(loc="lower left", fontsize=fontsize, ncol=2)
plt.grid()

#plt.subplots_adjust()
print(f"pngs/time_evolution_tot_mass_{model}_{frac}pc_w_diff_eq_update_alpha.png")
figname = f"pngs/time_evolution_tot_mass_{model}_{frac}pc_w_diff_eq_update_alpha.png"
print(figname)
plt.savefig(figname,facecolor="white", bbox_inches="tight", pad_inches=0.3)


"""
    r_mmix_half_40 = mmix_a_half_40/mmix_b_half_40
    r_mmix_fiducial_40 = mmix_a_fiducial_40/mmix_b_fiducial_40
    r_mmix_15_40 = mmix_a_15_40/mmix_b_15_40
    r_mmix_high_40 = mmix_a_high_40/mmix_b_high_40

    center_he4_a_array = [center_he4_a_half_40, center_he4_a_fiducial_40, center_he4_a_15_40, center_he4_a_high_40]
    center_he4_b_array = [center_he4_b_half_40, center_he4_b_fiducial_40, center_he4_b_15_40, center_he4_b_high_40]
    mmix_a_array = [mmix_a_half_40, mmix_a_fiducial_40, mmix_a_15_40, mmix_a_high_40]
    mmix_b_array = [mmix_b_half_40, mmix_b_fiducial_40, mmix_b_15_40, mmix_b_high_40]
    m_a_array = [m_a_half_40, m_a_fiducial_40, m_a_15_40, m_a_high_40]
    m_b_array = [m_b_half_40, m_b_fiducial_40, m_b_15_40, m_b_high_40]

    dMmixdY_array = np.array([dMmixdY_half_40, dMmixdY_fiducial_40, dMmixdY_15_40, dMmixdY_high_40])
    dMtotdY_array = np.array([dMtotdY_half_40, dMtotdY_fiducial_40, dMtotdY_15_40, dMtotdY_high_40])
    print(dMmixdY_array, dMtotdY_array)

    axis = fig.add_subplot(2,3,q+1)
    plt.plot(center_he4_array_wo_mt, mmix_array_wo_mt, label=r"No MT", lw=3, color="black")
    plt.plot(center_he4_array_fiducial_40, mmix_array_fiducial_40, label=r"fiducial", lw=3, color="royalblue")
    plt.plot(center_he4_array_half_40, mmix_array_half_40, label=r"half", lw=3, color="royalblue", linestyle="dashed")
    plt.plot(center_he4_array_15_40, mmix_array_15_40, label=r"1.5x", lw=3, color="royalblue", linestyle="dotted")
    plt.plot(center_he4_array_high_40, mmix_array_high_40, label=r"10x", lw=3, color="royalblue", linestyle="dashdot")

    plt.plot(incorp_Y_fiducial_40, incorp_mmix_fiducial_40, label=r"fiducial (incorp.)", lw=3, color="orangered")
    plt.plot(incorp_Y_half_40, incorp_mmix_half_40, label=r"half (incorp.)", lw=3, color="orangered", linestyle="dashed")
    plt.plot(incorp_Y_15_40, incorp_mmix_15_40, label=r"1.5x (incorp.)", lw=3, color="orangered", linestyle="dotted")
    plt.plot(incorp_Y_high_40, incorp_mmix_high_40, label=r"high (incorp.)", lw=3, color="orangered", linestyle="dashdot")

    plt.scatter(center_he4_b_array, mmix_b_array, s=150, color="black", marker="*")
    plt.scatter(center_he4_a_array, mmix_a_array, s=150, color="black", marker="*")

    axis.set_xlabel(r"$Y$", fontsize=fontsize)
    axis.set_ylabel(r"$m_\mathrm{mix} $ [$M_\odot$]", fontsize=fontsize)
    #axis.set_yticks(np.arange(0,21,10))
    #axis.set_yticks(np.arange(0,21,2),minor=True)
    axis.tick_params(axis="both", which="major", labelsize=fontsize)
    #axis.set_yticks(np.arange(0,61,10))
    #axis.set_yticks(np.arange(0,61,2),minor=True)
    axis.set_xticks(np.arange(0,1.1,0.5))
    axis.set_xticks(np.arange(0,1.1,0.1),minor=True)
    #plt.yscale("log")
    plt.ylim(0, 61)
    plt.xlim(0.15, 1)

    plt.legend(loc="lower left", fontsize=fontsize,ncol=2)
    #plt.legend(loc="upper left", fontsize=fontsize,ncol=2)
    plt.grid()

    axis = fig.add_subplot(2,3,q+4)
    plt.plot(center_he4_array_wo_mt, m_array_wo_mt, label=r"No MT", lw=3, color="black")
    plt.plot(center_he4_array_fiducial_40, m_array_fiducial_40, label=r"fiducial", lw=3, color="royalblue")
    plt.plot(center_he4_array_half_40, m_array_half_40, label=r"half", lw=3, color="royalblue", linestyle="dashed")
    plt.plot(center_he4_array_15_40, m_array_15_40, label=r"1.5x", lw=3, color="royalblue", linestyle="dotted")
    plt.plot(center_he4_array_high_40, m_array_high_40, label=r"10x", lw=3, color="royalblue", linestyle="dashdot")

    plt.scatter(center_he4_b_array, m_b_array, s=150, color="black", marker="*")
    plt.scatter(center_he4_a_array, m_a_array, s=150, color="black", marker="*")

    axis.set_xlabel(r"$Y$", fontsize=fontsize)
    axis.set_ylabel(r"$m_\mathrm{tot} $ [$M_\odot$]", fontsize=fontsize)
    #axis.set_yticks(np.arange(0,21,10))
    #axis.set_yticks(np.arange(0,21,2),minor=True)
    axis.tick_params(axis="both", which="major", labelsize=fontsize)
    axis.set_yticks(np.arange(0,61,10))
    axis.set_yticks(np.arange(0,61,2),minor=True)
    axis.set_xticks(np.arange(0,1.1,0.5))
    axis.set_xticks(np.arange(0,1.1,0.1),minor=True)
    #plt.yscale("log")
    plt.ylim(0, 61)

    plt.legend(loc="lower left", fontsize=fontsize)
    #plt.legend(loc="upper left", fontsize=fontsize)
    plt.grid()

#plt.subplots_adjust()
#figname = rf"pngs/dMmixdY_dMdt_func_{model}_normed_w_ini_value"
print(f"pngs/time_evolution_tot_mass_{model}_w_diff_eq.png")
figname = f"pngs/time_evolution_tot_mass_{model}_w_diff_eq.png"
print(figname)
plt.savefig(figname,facecolor="white", bbox_inches="tight", pad_inches=0.3)
"""

