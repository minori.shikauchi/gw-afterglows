import numpy 
import math
from astropy import units
from astropy.cosmology import Planck15, z_at_value
import time 
from multiprocessing import Pool, Process
import multiprocessing
import sys
import random

from astropy.cosmology.core import CosmologyError

d_ref = 40 * units.Mpc

consective_rev06 = [1878, 1879, 1889, 1894, 1904, 1906, 1907, 1908, 1909, 1912, 1913, 1914, 1919, 1920, 1921, 1924, 1926, 1927, 1928, 1929, 1931, 1933, 1934, 1935, 1936, 1939,1958, 1959, 1965, 1969, 1970, 1971, 1972, 1975, 1976, 1977, 1978, 1979, 1980, 1983, 1984, 1985, 1988, 1989, 1990, 2012, 2013, 2029, 2030, 2031, 2032, 2039, 2040, 2044, 2046, 2059, 2060, 2072, 2073, 2075, 2077, 2079, 2080, 2081, 2084, 2085, 2086, 2087, 2093, 2096, 2097, 2098, 2099, 2105, 2106, 2107, 2108, 2109, 2111, 2112, 2118, 2121, 2122, 2126, 2127, 2133, 2134, 2135, 2143, 2163, 2166, 2167, 2174, 2175, 2180, 2181, 2182]
day_duration = consective_rev06[-1] - consective_rev06[0] + 1

sigma = 6 * 10**(-3)
t_duration_list = numpy.logspace(0, 3.5, 10)
threshold_array = numpy.logspace(-1, 2, 50)

#tot_array_interp_100 = numpy.load("/project/rpp-chime/minorip/tot_array_interp_100.npy")
tot_likelihood = numpy.load("tot_likelihood.npy")
tot_pix_number = numpy.load("tot_pix_number.npy")
valid_pix_array = numpy.load("million_valid_pix_array.npy")

##lc_random = numpy.random.randint(0, tot_array_interp_100.shape[0], size=sample_number)
#lc_random = numpy.load("million_lc_random.npy")

merger_random = numpy.load("million_merger_random.npy")
inj_template_array = numpy.load("million_inj_temp_array.npy")

tot_com_dis = numpy.load("million_tot_com_dis.npy")


# calculate P(likelihood ratio|noise), finding max likelihood ratio
def injection_test(start_pix, end_pix, result_list, pixel_random, orig_pix):
    limited_map = numpy.load("/project/rpp-chime/minorip/limited_map.npy")

    for k in range(start_pix, end_pix):
        pixel_random_number = int(pixel_random[k])
        ind_pixel = numpy.where(orig_pix == pixel_random_number)[0][0]

        #lc_random_number = lc_random[ind_pixel]
        merger_random_number = merger_random[ind_pixel]
        
        data = limited_map[:,pixel_random_number].copy()
        numpy.random.shuffle(data)
        inj_template = inj_template_array[ind_pixel]
        #inj_template = tot_array_interp_100[lc_random_number]
    
        com_dis = tot_com_dis[ind_pixel] * units.Mpc
        #print(pixel_random_number, ind_pixel, merger_random_number, com_dis)
        #print(pixel_random_number, ind_pixel, merger_random_number, lc_random_number, com_dis)
        try:
            z = z_at_value(Planck15.comoving_distance, com_dis)
        except CosmologyError:
            z = 0
                
        lum_dis = Planck15.luminosity_distance(z)
        beta = (1 + z)*(d_ref/lum_dis)**2
        
        data[merger_random_number:] += (beta.value*inj_template)[:305-merger_random_number]
    
        sigma_array = numpy.ones(day_duration)*sigma
        ind = (numpy.isnan(data))
        sigma_array[ind] = numpy.nan

        convolve_template, convolve_temp_data, Lambda_n, amp, const_array, ave_temp = convolve_temp_mt(data, sigma_array)

        #max_ind = numpy.where(Lambda_n == numpy.nanmax(Lambda_n))[0][0]
        max_ind = numpy.where(Lambda_n == numpy.nanmax(Lambda_n))
        recovery_ind = numpy.unravel_index(numpy.nanargmax(Lambda_n), Lambda_n.shape)
        recovery_merger_time = recovery_ind[1]

        estimate_amp = amp[max_ind]
        estimate_const = const_array[max_ind]
        #calc_amp_array[k] = estimate_amp
        #estimate_const_array[k] = estimate_const
        #print(k, Lambda_array[q][k], lc_random_number, max_ind, beta.value, estimate_amp)

        result_list.append([pixel_random[k], numpy.nanmax(Lambda_n), recovery_merger_time, merger_random_number])

if __name__ == "__main__":
    print("... starting ...")
    argvs = sys.argv
    threshold_number = int(argvs[1])
    threshold = threshold_array[threshold_number]

    start_time = time.time()
    manager = multiprocessing.Manager()
    result_list = manager.list()

    processes = []

    ind = (tot_likelihood < threshold)
    #part_pix_number = tot_pix_number[ind]
    #pixel_random = numpy.array(random.choices(part_pix_number, numpy.ones(part_pix_number.shape[0]), k=sample_number*50))
    pixel_random = numpy.array(list(set(tot_pix_number[ind]) & set(valid_pix_array)))
    sorted_valid_pix_array = numpy.sort(valid_pix_array)
    sorted_pixel_random = numpy.sort(pixel_random)
    print(f"... threshold = {threshold}, pix number = {sorted_pixel_random.shape[0]} ...")
    print(sorted_pixel_random)

    if len(sorted_pixel_random) <= 50:
        process_num = len(sorted_pixel_random)
        sample_number = 1
    else:
        process_num = 50
        sample_number = len(sorted_pixel_random)//process_num + 1

    for q in range(process_num):
        start_pix = sample_number*q
        end_pix = min(start_pix + sample_number, len(sorted_pixel_random))

        process = Process(target=injection_test, kwargs={'start_pix': start_pix, 'end_pix': end_pix, 'result_list': result_list, 'pixel_random': sorted_pixel_random, "orig_pix": sorted_valid_pix_array})
        process.start()
        processes.append(process)
        print(f"... {q+1}/50 process(es) have been submitted: {start_pix} -  {end_pix}...")
        if end_pix == len(sorted_pixel_random):
            break

    for p in processes:
        p.join()

    result_list = numpy.array(result_list)
    print(result_list.shape)
    ind = numpy.argsort(result_list[:,0])
    exp_likelihood = result_list[ind, 1]
    exp_pix_number = result_list[ind, 0]
    exp_rec_merger_time = result_list[ind, 2]
    exp_inj_merger_time = result_list[ind, 3]
    print(exp_pix_number[0:10], exp_rec_merger_time[0:10], exp_inj_merger_time[0:10])
    #numpy.save(f"not_perfect_temp_bank/injection_{threshold_number}_not_perfect_temp_bank", tot_likelihood)
    numpy.save(f"not_perfect_temp_bank/true_uniform/million_det_pix_{threshold_number}", exp_pix_number)
    numpy.save(f"not_perfect_temp_bank/true_uniform/million_injection_{threshold_number}", exp_likelihood)
    numpy.save(f"not_perfect_temp_bank/true_uniform/million_recovered_merger_time_{threshold_number}", exp_rec_merger_time)
    numpy.save(f"not_perfect_temp_bank/true_uniform/million_injected_merger_time_{threshold_number}", exp_inj_merger_time)
        
    print(f"... {threshold_number + 1} injection test ... : {time.time() - start_time:.2f} sec")

