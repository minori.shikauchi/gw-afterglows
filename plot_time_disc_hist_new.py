import matplotlib.pyplot as plt
import numpy as np
import math
from matplotlib import rc
day = "Sep 7"
threshold_array = np.logspace(-1, 2, 50)

rc('font',**{'family':'serif', 'serif':['Computer Modern']})
rc('text', usetex=True)
fontsize = 23
titlesize = 1.2*fontsize

# pipeline tuning
threshold_array = np.logspace(-1, 2, 50)
upper_array_test = np.zeros(len(threshold_array))
upper_array_gaussian_test = np.zeros(len(threshold_array))

norm_much_gaussian_test = 5.*10**(6)

injection_path_gaussian_test = f"gaussian_noise/all_at_once/much_small_data_sigma/not_perfect_temp_bank/true_uniform/injection.npy"
injection_Lambda_array_gaussian_test = np.load(injection_path_much_small_gaussian_test)
injection_Lambda_array_gaussian_test *= norm_gaussian_test
no_injection_path_gaussian_test = f"gaussian_noise/all_at_once/much_small_data_sigma/not_perfect_temp_bank/true_uniform/no_injection.npy"
no_injection_Lambda_array_gaussian_test = np.load(no_injection_path_much_small_gaussian_test)
no_injection_Lambda_array_gaussian_test *= norm_gaussian_test

for i in range(len(threshold_array)):
    threshold = threshold_array[i]

    injection_path_test = f"not_perfect_temp_bank/all_map/true_uniform/injection_{i}.npy"
    part_Lambda_array_test = np.load(injection_path_test)

    ind_test = (part_Lambda_array_test > threshold)
    time_space_test = 4. * math.pi/3. * (1.5)**3 * part_Lambda_array_test[ind_test].shape[0]/100000 * 305/365.25

    #ind_gaussian_test = ((injection_Lambda_array_gaussian_test > threshold))
    ind_gaussian_test = ((no_injection_Lambda_array_gaussian_test < threshold) & (injection_Lambda_array_gaussian_test > threshold))
    time_space_gaussian_test = 4. * math.pi/3. * (1.5)**3 * injection_Lambda_array_gaussian_test[ind_gaussian_test].shape[0]/100000 * 305/365.25

    print(i, threshold, part_Lambda_array_test[ind_test].shape[0], injection_Lambda_array_gaussian_test[ind_gaussian_test].shape[0])

    try:
        upper_test = 2.303/time_space_test
    except ZeroDivisionError:
        upper_test = np.nan
        
    try:
        upper_gaussian_test = 2.303/time_space_gaussian_test
    except ZeroDivisionError:
        upper_gaussian_test = np.nan
        
    upper_array_test[i] = upper_test
    upper_array_gaussian_test[i] = upper_gaussian_test

print("closed, gaussian")
print(np.nanargmin(upper_array_test), np.nanargmin(upper_array_gaussian_test))
print(threshold_array[np.nanargmin(upper_array_test)], threshold_array[np.nanargmin(upper_array_gaussian_test)])
print(np.nanmin(upper_array_test), np.nanmin(upper_array_gaussian_test))

smallest_ind = np.nanargmin(upper_array_test)
smallest_ind_gaussian = np.nanargmin(upper_array_gaussian_test)
print(smallest_ind, smallest_ind_gaussian, upper_array_test[smallest_ind], upper_array_gaussian_test[smallest_ind_gaussian])
print(threshold_array[smallest_ind_gaussian])
threshold_gaussian_test = threshold_array[smallest_ind_gaussian]

threshold = threshold_array[smallest_ind]
rec_merger_time_gaussian_test_path = "gaussian_noise/all_at_once/much_small_data_sigma/not_perfect_temp_bank/true_uniform/recovered_merger_time.npy"
inj_merger_time_gaussian_test_path = f"gaussian_noise/all_at_once/much_small_data_sigma/not_perfect_temp_bank/true_uniform/injected_merger_time.npy"
rec_merger_time_gaussian_test = np.load(rec_merger_time_gaussian_test_path)
inj_merger_time_gaussian_test = np.load(inj_merger_time_gaussian_test_path)

injection_path_test = f"not_perfect_temp_bank/all_map/true_uniform/injection_{smallest_ind}.npy"
part_Lambda_array_test = np.load(injection_path_test)
ind_test = (part_Lambda_array_test > threshold)

rec_merger_time_test_path = f"not_perfect_temp_bank/all_map/true_uniform/recovered_merger_time_{smallest_ind}.npy"
inj_merger_time_test_path = f"not_perfect_temp_bank/all_map/true_uniform/injected_merger_time_{smallest_ind}.npy"

rec_merger_time_test = np.load(rec_merger_time_test_path)
inj_merger_time_test = np.load(inj_merger_time_test_path)
    
diff_merger_time_test = rec_merger_time_test - inj_merger_time_test
diff_merger_time_gaussian_test = rec_merger_time_gaussian_test - inj_merger_time_gaussian_test
diff_merger_time_test = diff_merger_time_test[ind_test]
    
ind_gaussian_test = ((injection_Lambda_array_gaussian_test > threshold) & (no_injection_Lambda_array_gaussian_test < threshold))
diff_merger_time_gaussian_test = diff_merger_time_gaussian_test[ind_gaussian_test]
print(diff_merger_time_test.shape, diff_merger_time_gaussian_test.shape)
    
abs_diff_merger_time_test = np.abs(diff_merger_time_test)
abs_diff_merger_time_gaussian_test = np.abs(diff_merger_time_gaussian_test)
    
fig = plt.figure(figsize=(12,8))
axis = fig.add_subplot(1,1,1)
plt.hist(diff_merger_time_test, color="black", lw=3, label="closed box", histtype="step" ,density=True, bins=np.arange(-210, 211, 10))
plt.hist(diff_merger_time_gaussian_test, color="orangered", lw=3, label="Gaussian noise (1 $\mu$Jy)", histtype="step" ,density=True, bins=np.arange(-210, 211, 10), linestyle="dashed")
    
plt.xlabel(r"$t_\mathrm{rec} - t_\mathrm{inj}$ [days]",fontsize=fontsize)
plt.ylabel(r"$P$", fontsize=fontsize)
axis.tick_params(axis='x',labelsize=fontsize)
axis.tick_params(axis='y',labelsize=fontsize)
plt.legend(fontsize=fontsize, loc="lower left")
axis.set_xticks(np.arange(-200, 201, 100))
axis.set_xticks(np.arange(-200, 201, 10), minor=True)
plt.grid()
#plt.yscale("log")

fig.set_facecolor("white")

print(threshold_array[smallest_ind])
#plt.title(f"plotted on {day}, 2023", fontsize=titlesize, y=1.05)
plt.savefig("/home/minorip/pdfs/time_discrepancy_hist.pdf", facecolor="white", bbox_inches="tight", pad_inches=0.3)


