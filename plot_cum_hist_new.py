import matplotlib.pyplot as plt
import numpy as np
import math
from matplotlib import rc
day = "Sep 7"
threshold_array = np.logspace(-1, 2, 50)

rc('font',**{'family':'serif', 'serif':['Computer Modern']})
rc('text', usetex=True)
fontsize = 23
titlesize = 1.2*fontsize
sgrb_count = np.array([279, 893, 2225])

# pipeline tuning
threshold_array = np.logspace(-1, 2, 50)
upper_array_test = np.zeros(len(threshold_array))
upper_array_gaussian_test = np.zeros(len(threshold_array))

norm_much_gaussian_test = 5.*10**(6)

injection_path_gaussian_test = f"gaussian_noise/all_at_once/much_small_data_sigma/not_perfect_temp_bank/true_uniform/injection.npy"
injection_Lambda_array_gaussian_test = np.load(injection_path_much_small_gaussian_test)
injection_Lambda_array_gaussian_test *= norm_gaussian_test
no_injection_path_gaussian_test = f"gaussian_noise/all_at_once/much_small_data_sigma/not_perfect_temp_bank/true_uniform/no_injection.npy"
no_injection_Lambda_array_gaussian_test = np.load(no_injection_path_much_small_gaussian_test)
no_injection_Lambda_array_gaussian_test *= norm_gaussian_test

for i in range(len(threshold_array)):
    threshold = threshold_array[i]

    injection_path_test = f"not_perfect_temp_bank/all_map/true_uniform/injection_{i}.npy"
    part_Lambda_array_test = np.load(injection_path_test)

    ind_test = (part_Lambda_array_test > threshold)
    time_space_test = 4. * math.pi/3. * (1.5)**3 * part_Lambda_array_test[ind_test].shape[0]/100000 * 305/365.25

    #ind_gaussian_test = ((injection_Lambda_array_gaussian_test > threshold))
    ind_gaussian_test = ((no_injection_Lambda_array_gaussian_test < threshold) & (injection_Lambda_array_gaussian_test > threshold))
    time_space_gaussian_test = 4. * math.pi/3. * (1.5)**3 * injection_Lambda_array_gaussian_test[ind_gaussian_test].shape[0]/100000 * 305/365.25

    print(i, threshold, part_Lambda_array_test[ind_test].shape[0], injection_Lambda_array_gaussian_test[ind_gaussian_test].shape[0])

    try:
        upper_test = 2.303/time_space_test
    except ZeroDivisionError:
        upper_test = np.nan
        
    try:
        upper_gaussian_test = 2.303/time_space_gaussian_test
    except ZeroDivisionError:
        upper_gaussian_test = np.nan
        
    upper_array_test[i] = upper_test
    upper_array_gaussian_test[i] = upper_gaussian_test

print("closed, gaussian")
print(np.nanargmin(upper_array_test), np.nanargmin(upper_array_gaussian_test))
print(threshold_array[np.nanargmin(upper_array_test)], threshold_array[np.nanargmin(upper_array_gaussian_test)])
print(np.nanmin(upper_array_test), np.nanmin(upper_array_gaussian_test))

smallest_ind = np.nanargmin(upper_array_test)
smallest_ind_gaussian = np.nanargmin(upper_array_gaussian_test)
print(smallest_ind, smallest_ind_gaussian)
threshold_gaussian_test = threshold_array[smallest_ind_gaussian]

rec_merger_time_gaussian_test_path = f"gaussian_noise/all_at_once/much_small_data_sigma/not_perfect_temp_bank/true_uniform/recovered_merger_time.npy"
inj_merger_time_gaussian_test_path = f"gaussian_noise/all_at_once/much_small_data_sigma/not_perfect_temp_bank/true_uniform/injected_merger_time.npy"
rec_merger_time_gaussian_test = np.load(rec_merger_time_gaussian_test_path)
inj_merger_time_gaussian_test = np.load(inj_merger_time_gaussian_test_path)

threshold = threshold_array[smallest_ind]
injection_path_test = f"not_perfect_temp_bank/all_map/true_uniform/injection_{smallest_ind}.npy"
part_Lambda_array_test = np.load(injection_path_test)
ind_test = (part_Lambda_array_test > threshold)

rec_merger_time_test_path = f"not_perfect_temp_bank/all_map/true_uniform/recovered_merger_time_{smallest_ind}.npy"
inj_merger_time_test_path = f"not_perfect_temp_bank/all_map/true_uniform/injected_merger_time_{smallest_ind}.npy"

rec_merger_time_test = np.load(rec_merger_time_test_path)
inj_merger_time_test = np.load(inj_merger_time_test_path)
    
diff_merger_time_test = rec_merger_time_test - inj_merger_time_test
diff_merger_time_gaussian_test = rec_merger_time_gaussian_test - inj_merger_time_gaussian_test
diff_merger_time_test = diff_merger_time_test[ind_test]
    
ind_gaussian_test = ((injection_Lambda_array_gaussian_test > threshold_gaussian_test) & (no_injection_Lambda_array_gaussian_test < threshold_gaussian_test))
diff_merger_time_gaussian_test = diff_merger_time_gaussian_test[ind_gaussian_test]
print(diff_merger_time_test.shape, diff_merger_time_gaussian_test.shape)
    
abs_diff_merger_time_test = np.abs(diff_merger_time_test)
abs_diff_merger_time_gaussian_test = np.abs(diff_merger_time_gaussian_test)
    
hist_count_test, bin_test = np.histogram(abs_diff_merger_time_test, bins=np.logspace(0,2.3,10))
hist_count_test = np.array(hist_count_test, dtype="float")
bin_width_test = bin_test[1:] - bin_test[:-1]
bin_pos_test = (bin_test[1:] + bin_test[:-1])/2.
#hist_count_test /= bin_width_test
cum_hist_count_test = np.cumsum(hist_count_test)
cum_hist_count_test /= cum_hist_count_test[-1]
error_high_test = sgrb_count[-1]*cum_hist_count_test
error_low_test = sgrb_count[0]*cum_hist_count_test
error_bars_test = np.vstack((sgrb_count[1]*cum_hist_count_test-error_low_test, error_high_test-sgrb_count[1]*cum_hist_count_test))
#print(hist_count_test, bin_width_test, error_bars_test)
    
hist_count_gaussian_test, bin_gaussian_test = np.histogram(abs_diff_merger_time_gaussian_test, bins=np.logspace(0,2.3,10))
hist_count_gaussian_test = np.array(hist_count_gaussian_test, dtype="float")
bin_width_gaussian_test = bin_gaussian_test[1:] - bin_gaussian_test[:-1]
bin_pos_gaussian_test = (bin_gaussian_test[1:] + bin_gaussian_test[:-1])/2.
#hist_count_gaussian_test /= bin_width_gaussian_test
cum_hist_count_gaussian_test = np.cumsum(hist_count_gaussian_test)
cum_hist_count_gaussian_test /= cum_hist_count_gaussian_test[-1]
error_high_gaussian_test = sgrb_count[-1]*cum_hist_count_gaussian_test
error_low_gaussian_test = sgrb_count[0]*cum_hist_count_gaussian_test
error_bars_gaussian_test = np.vstack((sgrb_count[1]*cum_hist_count_gaussian_test-error_low_gaussian_test, error_high_gaussian_test-sgrb_count[1]*cum_hist_count_gaussian_test))
#print(hist_count_gaussian_test, bin_width_gaussian_test, error_bars_gaussian_test)
    
fig = plt.figure(figsize=(12,8))
axis = fig.add_subplot(1,1,1)
#plt.bar(bin_pos_test, sgrb_count[1]*cum_hist_count_test, edgecolor="black", lw=3, width=bin_width_test, label="closed-box", fill=None)
#plt.bar(bin_pos_gaussian_test, sgrb_count[1]*cum_hist_count_gaussian_test, edgecolor="orangered", lw=3, width=bin_width_test, label="Gaussian noise (data-based)", fill=None)
plt.errorbar(bin_pos_test, sgrb_count[1]*cum_hist_count_test, yerr=error_bars_test, marker=".", drawstyle="steps-mid", capsize=10, markeredgewidth=3, color="black", lw=3, label="closed-box")
plt.errorbar(bin_pos_gaussian_test, sgrb_count[1]*cum_hist_count_gaussian_test, yerr=error_bars_gaussian_test, marker=".", drawstyle="steps-mid", capsize=10, markeredgewidth=3, color="orangered", lw=3, label="Gaussian noise (1 $\mu$Jy)")
    
plt.xlabel(r"$|t_\mathrm{rec} - t_\mathrm{inj}|$ [days]",fontsize=fontsize)
plt.ylabel(r"$N_\mathrm{det}$ [/year]", fontsize=fontsize)
axis.tick_params(axis='x',labelsize=fontsize)
axis.tick_params(axis='y',labelsize=fontsize)
plt.legend(fontsize=fontsize, loc="upper left")
axis.set_xticks(np.arange(0, 201, 50))
axis.set_xticks(np.arange(0, 201, 10), minor=True)
axis.set_yticks(np.arange(0, 1.1, 0.5))
axis.set_yticks(np.arange(0, 1.1, 0.1), minor=True)
plt.grid()
plt.yscale("log")
plt.xscale("log")

fig.set_facecolor("white")

print(threshold_array[smallest_ind])
#plt.title(f"plotted on {day}, 2023", fontsize=titlesize, y=1.05)

plt.savefig("/home/minorip/pdfs/cum_hist.pdf", facecolor="white", bbox_inches="tight", pad_inches=0.3)

