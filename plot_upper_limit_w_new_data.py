import matplotlib.pyplot as plt
import numpy as np
import math
from matplotlib import rc
import os
from search_pipeline import data_condition_remove_outliers
import argparse
day = "Oct 15"
sgrb_count = np.array([279, 893, 2225])

threshold_array = np.logspace(-1, 2, 50)

#rc('font',**{'family':'serif', 'serif':['Computer Modern']})
#rc('text', usetex=True)
fontsize = 23
titlesize = 1.2*fontsize

# pipeline tuning
valid_pix_array = np.load("npys/valid_pix_array.npy")
pixel_random = np.array(list(set(valid_pix_array)))
sorted_valid_pix_array = np.sort(valid_pix_array)
sorted_pixel_random = np.sort(pixel_random)

template_bank = np.load("npys/tot_array_interp_100.npy")
injected_lc = np.load("npys/injected_temp_gaussian.npy")

def apply_lnLambda_mask(threshold_value, shuffled_data, non_shuffled_data):
    assert shuffled_data.shape == non_shuffled_data.shape
    non_shuffled_data = non_shuffled_data.copy()
    non_shuffled_data[shuffled_data > threshold_value] = np.nan
    return non_shuffled_data 

def find_max_lnLambda(non_shuffled_data):
    return np.nanmax(non_shuffled_data), np.nanargmax(non_shuffled_data)

def find_detectable_sources(threshold_value, non_shuffled_data):
    count = (non_shuffled_data > threshold_value).sum()
    time_space = 4. * math.pi/3. * (1.5)**3 * count/100000. * 305./365.25
    return time_space

def find_min_upper_limit(upper_array):
    return threshold_array[np.nanargmin(upper_array)]

def properties_of_detectable_sources(threshold_value, non_shuffled_data, injected_time, injected_amp, injected_com_dis, recovered_time, recovered_amp, recovered_temp_num, injected_data):
    ind = (non_shuffled_data > threshold_value)

    det_template = template_bank[np.array(recovered_temp_num[ind],dtype=int),:]
    det_injected_lc = injected_lc[ind,:]
    det_recovered_time = np.array(recovered_time[ind], dtype=int)
    det_injected_data = injected_data[:, ind]
    estimated_amp = np.array([])
    for i in range(det_template.shape[0]):
        part_det_recovered_time = det_recovered_time[i]
        part_det_template = det_template[i,:].copy()
        part_det_injected_lc = det_injected_lc[i,:].copy()

        part_det_injected_data = det_injected_data[:,i]
        part_det_template = np.append(np.zeros(part_det_recovered_time), part_det_template)[:305]
        part_det_injected_lc = np.append(np.zeros(part_det_recovered_time), part_det_injected_lc)[:305]
        assert part_det_template.shape == part_det_injected_lc.shape == (305, ), f"{part_det_template.shape}, {part_det_injected_lc.shape}"

        part_det_template[np.isnan(part_det_injected_data)] = 0.
        part_det_injected_lc[np.isnan(part_det_injected_data)] = 0.
        
        intrinsic_amp = np.sqrt(np.dot(part_det_injected_lc, part_det_injected_lc)/np.dot(part_det_template, part_det_template))
        estimated_amp = np.append(estimated_amp, intrinsic_amp)

    return injected_time[ind] - recovered_time[ind], injected_amp[ind], recovered_amp[ind], injected_com_dis[ind], estimated_amp*injected_amp[ind]

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-flag", "--data-flag", type=str, default="n")
    args = parser.parse_args()

    flag = args.data_flag

    fig = plt.figure(figsize=(12,8))
    axis = fig.add_subplot(1,1,1)

    #rms_threshold_name_array = np.array(["lowest_threshold/", "lower_threshold/", "middle_threshold/", "/home/minori/copy_from_cedar/", "highest_threshold/"])
    #kernel_width_dir_name_array = np.array(["window_width_challenge/38_days", "window_width_challenge/48_days/", "/home/minori/copy_from_cedar/middle_threshold/", "window_width_challenge/75_days/", "window_width_challenge/94_days/"])

    rms_threshold_value_array = np.array([0.632, 0.8, 1.265, 2.0, 2.53])
    #kernel_width_value_array = np.array([38, 48, 60, 75, 94])

    color_array = np.array(["orangered", "royalblue", "forestgreen", "black", "purple"])
    #for j, rms_threshold_name in enumerate(rms_threshold_name_array):
    for j, rms_threshold_value in enumerate(rms_threshold_value_array[2:3]):
    #for j, kernel_width_name in enumerate(kernel_width_dir_name_array):
        color = color_array[j]
        #rms_threshold_value = rms_threshold_value_array[j]
        rms_tag = f"threshold_{rms_threshold_value}"
        #kernel_width_value = kernel_width_value_array[j]

        upper_array_test = np.zeros(len(threshold_array))
        upper_array_gaussian_test = np.zeros(len(threshold_array))
        upper_array_gaussian_no_mask = np.zeros(len(threshold_array))

        for i, threshold in enumerate(threshold_array):
            #======================================================================

            #if j == 2:
            #    orig_test_dir_path = "not_perfect_temp_bank/all_map/true_uniform/"
            #else:
            #    orig_test_dir_path = "not_perfect_temp_bank/"
            #test_dir_path = os.path.join(rms_threshold_name, orig_test_dir_path)
            orig_test_dir_path = "not_perfect_temp_bank/all_map/true_uniform/"
            test_dir_path = f"window_width_challenge/85_days/threshold_{rms_threshold_value}/not_perfect_temp_bank"

            injection_path_test = os.path.join(test_dir_path, f"lambda_all.npy")
            injection_Lambda_array_test = np.load(injection_path_test)

            no_injection_path_test = os.path.join(test_dir_path, f"temp/storage0/lambda_all.npy")
            no_injection_Lambda_array_test = np.load(no_injection_path_test)

            shuffle_no_injection_path_test = os.path.join(test_dir_path, f"temp/storage0/lambda_after_shuffling_all.npy")
            shuffle_no_injection_Lambda_array_test = np.load(shuffle_no_injection_path_test)

            shuffle_injection_path_test = os.path.join(test_dir_path, f"lambda_after_shuffling_all.npy")
            shuffle_injection_Lambda_array_test = np.load(shuffle_injection_path_test)

            injected_amp_path_test = os.path.join(orig_test_dir_path, f"injected_amp.npy")
            injected_amp_array_test = np.load(injected_amp_path_test)

            injected_com_dis_path_test = os.path.join(orig_test_dir_path, f"injected_com_dis.npy")
            injected_com_dis_array_test = np.load(injected_com_dis_path_test)

            injected_time_path_test = os.path.join(orig_test_dir_path, f"injected_merger_time.npy")
            injected_time_array_test = np.load(injected_time_path_test)

            recovered_amp_path_test = os.path.join(test_dir_path, f"recovered_amp_all.npy")
            recovered_amp_array_test = np.load(recovered_amp_path_test)

            recovered_time_path_test = os.path.join(test_dir_path, f"recovered_merger_time_all.npy")
            recovered_time_array_test = np.load(recovered_time_path_test)

            recovered_temp_num_path_test = os.path.join(test_dir_path, f"recovered_template_all.npy")
            recovered_temp_num_array_test = np.load(recovered_temp_num_path_test)

            injected_data_path_test = os.path.join(orig_test_dir_path, f"injected_data.npy")
            injected_data_test = np.load(injected_data_path_test)

            #======================================================================

            #if j == 2:
            #    orig_gaussian_dir_path = "gaussian_noise/all_at_once/data_sigma/not_perfect_temp_bank/true_uniform/"
            #else:
            #    orig_gaussian_dir_path = "gaussian_noise/"
            #gaussian_dir_path = os.path.join(rms_threshold_name, orig_gaussian_dir_path)
            #gaussian_dir_path = os.path.join(kernel_width_name, orig_gaussian_dir_path)
            #gaussian_dir_path = os.path.join(orig_gaussian_dir_path, rms_tag)
            orig_gaussian_dir_path = "gaussian_noise/all_at_once/data_sigma/not_perfect_temp_bank/true_uniform/"
            gaussian_dir_path = f"window_width_challenge/85_days/threshold_{rms_threshold_value}/gaussian"

            injection_path_gaussian_test = os.path.join(gaussian_dir_path, f"lambda_all.npy")
            injection_Lambda_array_gaussian_test = np.load(injection_path_gaussian_test)

            no_injection_path_gaussian_test = os.path.join(gaussian_dir_path, f"temp/storage0/lambda_all.npy")
            no_injection_Lambda_array_gaussian_test = np.load(no_injection_path_gaussian_test)

            shuffle_no_injection_path_gaussian_test = os.path.join(gaussian_dir_path, f"temp/storage0/lambda_after_shuffling_all.npy")
            shuffle_no_injection_Lambda_array_gaussian_test = np.load(shuffle_no_injection_path_gaussian_test)

            shuffle_injection_path_gaussian_test = os.path.join(gaussian_dir_path, f"lambda_after_shuffling_all.npy")
            shuffle_injection_Lambda_array_gaussian_test = np.load(shuffle_injection_path_gaussian_test)

            injected_amp_path_gaussian_test = os.path.join(orig_gaussian_dir_path, f"injected_amp.npy")
            injected_amp_array_gaussian_test = np.load(injected_amp_path_gaussian_test)

            injected_com_dis_path_gaussian_test = os.path.join(orig_gaussian_dir_path, f"injected_com_dis.npy")
            injected_com_dis_array_gaussian_test = np.load(injected_com_dis_path_gaussian_test)

            injected_time_path_gaussian_test = os.path.join(orig_gaussian_dir_path, f"injected_merger_time.npy")
            injected_time_array_gaussian_test = np.load(injected_time_path_gaussian_test)

            recovered_amp_path_gaussian_test = os.path.join(gaussian_dir_path, f"recovered_amp_all.npy")
            recovered_amp_array_gaussian_test = np.load(recovered_amp_path_gaussian_test)

            recovered_time_path_gaussian_test = os.path.join(gaussian_dir_path, f"recovered_merger_time_all.npy")
            recovered_time_array_gaussian_test = np.load(recovered_time_path_gaussian_test)

            recovered_temp_num_path_gaussian = os.path.join(gaussian_dir_path, f"recovered_template_all.npy")
            recovered_temp_num_array_gaussian_test = np.load(recovered_temp_num_path_gaussian)

            injected_data_path_gaussian_test = os.path.join(orig_gaussian_dir_path, f"injected_data.npy")
            injected_data_gaussian_test = np.load(injected_data_path_gaussian_test)

            #test_data_path = os.path.join(gaussian_dir_path, "no_inj_gaussian_noise.npy")
            #test_data = np.load(test_data_path)

            #======================================================================

            no_injection = apply_lnLambda_mask(threshold, shuffle_no_injection_Lambda_array_test, no_injection_Lambda_array_test)
            max_lnL_test, _ = find_max_lnLambda(no_injection)
            injection = apply_lnLambda_mask(threshold, shuffle_injection_Lambda_array_test, injection_Lambda_array_test)
            time_space_test = find_detectable_sources(max_lnL_test, injection) 

            no_injection = apply_lnLambda_mask(threshold, shuffle_no_injection_Lambda_array_gaussian_test, no_injection_Lambda_array_gaussian_test)
            max_lnL_gaussian_test, _ = find_max_lnLambda(no_injection)
            injection = apply_lnLambda_mask(threshold, shuffle_injection_Lambda_array_gaussian_test, injection_Lambda_array_gaussian_test)
            time_space_gaussian_test = find_detectable_sources(max_lnL_gaussian_test, injection) 

            #======================================================================
            no_mask_dir_path = os.path.join("no_mask/")

            injection_path_no_mask = os.path.join(no_mask_dir_path, f"lambda_all.npy")
            injection_Lambda_array_no_mask = np.load(injection_path_no_mask)

            no_injection_path_no_mask = os.path.join(no_mask_dir_path, f"temp/storage0/lambda_all.npy")
            no_injection_Lambda_array_no_mask = np.load(no_injection_path_no_mask)

            shuffle_no_injection_path_no_mask = os.path.join(no_mask_dir_path, f"temp/storage0/lambda_after_shuffling_all.npy")
            shuffle_no_injection_Lambda_array_no_mask = np.load(shuffle_no_injection_path_no_mask)

            shuffle_injection_path_no_mask = os.path.join(no_mask_dir_path, f"lambda_after_shuffling_all.npy")
            shuffle_injection_Lambda_array_no_mask = np.load(shuffle_injection_path_no_mask)

            injected_amp_path_no_mask = os.path.join(no_mask_dir_path, f"injected_amp.npy")
            injected_amp_array_no_mask = np.load(injected_amp_path_no_mask)

            injected_com_dis_path_no_mask = os.path.join(no_mask_dir_path, f"injected_com_dis.npy")
            injected_com_dis_array_no_mask = np.load(injected_com_dis_path_no_mask)

            injected_time_path_no_mask = os.path.join(no_mask_dir_path, f"injected_merger_time.npy")
            injected_time_array_no_mask = np.load(injected_time_path_no_mask)

            recovered_amp_path_no_mask = os.path.join(no_mask_dir_path, f"recovered_amp_all.npy")
            recovered_amp_array_no_mask = np.load(recovered_amp_path_no_mask)

            recovered_time_path_no_mask = os.path.join(no_mask_dir_path, f"recovered_merger_time_all.npy")
            recovered_time_array_no_mask = np.load(recovered_time_path_no_mask)

            recovered_temp_num_path_no_mask = os.path.join(no_mask_dir_path, f"recovered_template_all.npy")
            recovered_temp_num_array_no_mask = np.load(recovered_temp_num_path_no_mask)

            injected_data_path_no_mask = os.path.join(no_mask_dir_path, f"injected_data.npy")
            injected_data_no_mask = np.load(injected_data_path_no_mask)

            no_injection = apply_lnLambda_mask(threshold, shuffle_no_injection_Lambda_array_no_mask, no_injection_Lambda_array_no_mask)
            max_lnL_no_mask, _ = find_max_lnLambda(no_injection)
            injection = apply_lnLambda_mask(threshold, shuffle_injection_Lambda_array_no_mask, injection_Lambda_array_no_mask)
            time_space_no_mask = find_detectable_sources(max_lnL_no_mask, injection) 
            print(i, threshold, max_lnL_test, max_lnL_gaussian_test, max_lnL_no_mask, time_space_test, time_space_gaussian_test, time_space_no_mask)

            try:
                upper_test = 2.303/time_space_test
            except ZeroDivisionError:
                upper_test = np.nan
            upper_array_test[i] = upper_test
            
            try:
                upper_gaussian_test = 2.303/time_space_gaussian_test
            except ZeroDivisionError:
                upper_gaussian_test = np.nan
            upper_array_gaussian_test[i] = upper_gaussian_test

            try:
                upper_no_mask = 2.303/time_space_no_mask
            except ZeroDivisionError:
                upper_no_mask = np.nan
            upper_array_gaussian_no_mask[i] = upper_no_mask


        #plt.legend()
        #fig.savefig("test.png")

        print("closed, gaussian")
        print(np.nanargmin(upper_array_test), np.nanargmin(upper_array_gaussian_test))
        print(threshold_array[np.nanargmin(upper_array_test)], threshold_array[np.nanargmin(upper_array_gaussian_test)])
        print(np.nanmin(upper_array_test), np.nanmin(upper_array_gaussian_test))

        plt.scatter(threshold_array, upper_array_test, s=100, marker="*", c=color, label=rf"threshold = ${rms_threshold_value}$")
        plt.scatter(threshold_array, upper_array_gaussian_test, s=100, marker=".", c=color, label=r"Gaussian noise (6 mJy, masked)")
        plt.scatter(threshold_array, upper_array_gaussian_no_mask, s=100, marker=">", c=color, label=r"Gaussian noise (6 mJy, no mask)")
        #plt.scatter(threshold_array, upper_array_test, s=100, marker="*", c=color, label=rf"kenrel_width = ${kernel_width_value}$")
        #plt.scatter(threshold_array, upper_array_gaussian_test, s=100, marker=".", c=color)

    if flag == "y":
        threshold = np.inf
        limited_dir_path = os.path.join("new_limited_map/")

        injection_path_limited = os.path.join(limited_dir_path, f"lambda_all.npy")
        injection_Lambda_array_limited = np.load(injection_path_limited)

        no_injection_path_limited = os.path.join(limited_dir_path, f"temp/storage0/lambda_all.npy")
        no_injection_Lambda_array_limited = np.load(no_injection_path_limited)

        shuffle_no_injection_path_limited = os.path.join(limited_dir_path, f"temp/storage0/lambda_after_shuffling_all.npy")
        shuffle_no_injection_Lambda_array_limited = np.load(shuffle_no_injection_path_limited)

        shuffle_injection_path_limited = os.path.join(limited_dir_path, f"lambda_after_shuffling_all.npy")
        shuffle_injection_Lambda_array_limited = np.load(shuffle_injection_path_limited)

        no_injection = apply_lnLambda_mask(threshold, shuffle_no_injection_Lambda_array_limited, no_injection_Lambda_array_limited)
        max_lnL_limited, max_ind = find_max_lnLambda(no_injection)

        injection = apply_lnLambda_mask(threshold, shuffle_injection_Lambda_array_limited, injection_Lambda_array_limited)
        time_space_limited = find_detectable_sources(max_lnL_limited, injection) 
        try:
            upper_limited = 2.303/time_space_limited
        except ZeroDivisionError:
            upper_limited = np.nan

        print(max_lnL_limited, time_space_limited, upper_limited)
        plt.scatter(max_lnL_limited, upper_limited, s=100, marker="*", c="yellow", edgecolor="black", label="intensity-cut")

    axis.axhspan(10, 1700, color = "lightblue", alpha=0.2, label=r"BNS merger rate")

    plt.yscale("log")
    plt.xscale("log")
    plt.legend(loc="lower left", fontsize=fontsize*0.7, ncol=2, markerscale=2)

    plt.ylabel(r"$\langle R_\mathrm{det} \rangle_\mathrm{upper}$ [Gpc$^{-3}$ year$^{-1}$]", fontsize=fontsize)
    plt.xlabel(r"$\ln \Lambda_\mathrm{th}$", fontsize=fontsize)
    axis.tick_params(axis='x',labelsize=fontsize)
    axis.tick_params(axis='y',labelsize=fontsize)
    plt.xlim(0.1, 110)
    plt.ylim(0.1, 3*10**4)

    fig.set_facecolor("white")
    plt.grid()
    #plt.title(f"plotted on {day}, 2023", fontsize=titlesize, y=1.05)

    plt.savefig("for_poster/upper_limit_all_check.pdf", facecolor="white", bbox_inches="tight", pad_inches=0.3)

"""
fig = plt.figure(figsize=(12,8))
axis  = fig.add_subplot(1,1,1)
part_num = max_ind//1024
data_num = max_ind - part_num*1024 
#data_path_limited = os.path.join(limited_dir_path, f"temp/storage2/shuffled_data_n_{part_num}.npy")
data_path_limited = os.path.join(limited_dir_path, f"no_inj_new_limited_map.npy")
data_limited = np.load(data_path_limited)
max_data = data_limited[:, max_ind]
print(part_num, data_num, max_data)

plt.scatter(np.arange(305), max_data, s=150)

axis.tick_params(axis='x',labelsize=fontsize)
axis.tick_params(axis='y',labelsize=fontsize)
plt.legend(fontsize=fontsize, loc="upper left")
plt.grid()
fig.set_facecolor("white")
plt.savefig("for_poster/max_data.pdf", facecolor="white", bbox_inches="tight", pad_inches=0.3)
"""

# plot time discrepancy, i.e. the difference between t_injected and t_recovered.
threshold_min_test = find_min_upper_limit(upper_array_test)
threshold_min_gaussian_test = find_min_upper_limit(upper_array_gaussian_test)
threshold_min_no_mask = find_min_upper_limit(upper_array_gaussian_no_mask)

no_injection = apply_lnLambda_mask(threshold_min_test, shuffle_no_injection_Lambda_array_test, no_injection_Lambda_array_test)
max_lnL_test,_ = find_max_lnLambda(no_injection)
injection = apply_lnLambda_mask(threshold_min_test, shuffle_injection_Lambda_array_test, injection_Lambda_array_test)
det_time_dis_test, det_injected_amp_test, det_recovered_amp_test, det_injected_com_dis_test, det_estimated_amp_test = properties_of_detectable_sources(max_lnL_test, injection, injected_time_array_test, injected_amp_array_test, injected_com_dis_array_test, recovered_time_array_test, recovered_amp_array_test, recovered_temp_num_array_test, injected_data_test)

no_injection = apply_lnLambda_mask(threshold_min_gaussian_test, shuffle_no_injection_Lambda_array_gaussian_test, no_injection_Lambda_array_gaussian_test)
max_lnL_gaussian_test,_ = find_max_lnLambda(no_injection)
injection = apply_lnLambda_mask(threshold_min_gaussian_test, shuffle_injection_Lambda_array_gaussian_test, injection_Lambda_array_gaussian_test)
det_time_dis_gaussian_test, det_injected_amp_gaussian_test, det_recovered_amp_gaussian_test, det_injected_com_dis_gaussian_test, det_estimated_amp_gaussian_test = properties_of_detectable_sources(max_lnL_gaussian_test, injection, injected_time_array_gaussian_test, injected_amp_array_gaussian_test, injected_com_dis_array_gaussian_test, recovered_time_array_gaussian_test, recovered_amp_array_gaussian_test, recovered_temp_num_array_gaussian_test, injected_data_gaussian_test)

no_injection = apply_lnLambda_mask(threshold_min_no_mask, shuffle_no_injection_Lambda_array_no_mask, no_injection_Lambda_array_no_mask)
max_lnL_no_mask,_ = find_max_lnLambda(no_injection)
injection = apply_lnLambda_mask(threshold_min_no_mask, shuffle_injection_Lambda_array_no_mask, injection_Lambda_array_no_mask)
det_time_dis_no_mask, det_injected_amp_no_mask, det_recovered_amp_no_mask, det_injected_com_dis_no_mask, det_estimated_amp_no_mask = properties_of_detectable_sources(max_lnL_no_mask, injection, injected_time_array_no_mask, injected_amp_array_no_mask, injected_com_dis_array_no_mask, recovered_time_array_no_mask, recovered_amp_array_no_mask, recovered_temp_num_array_no_mask, injected_data_no_mask)

fig = plt.figure(figsize=(12,8))
axis = fig.add_subplot(1,1,1)
plt.hist(det_time_dis_test, color="black", lw=3, label="closed box", histtype="step" ,density=True, bins=np.arange(-210, 211, 10))
plt.hist(det_time_dis_gaussian_test, color="orangered", lw=3, label="Gaussian noise (6 mJy, masked)", histtype="step" ,density=True, bins=np.arange(-210, 211, 10), linestyle="dashed")

plt.xlabel(r"$t_\mathrm{rec} - t_\mathrm{inj}$ [days]",fontsize=fontsize)
plt.ylabel(r"$P$", fontsize=fontsize)
axis.tick_params(axis='x',labelsize=fontsize)
axis.tick_params(axis='y',labelsize=fontsize)
plt.legend(fontsize=fontsize, loc="upper left")
axis.set_xticks(np.arange(-200, 201, 100))
axis.set_xticks(np.arange(-200, 201, 10), minor=True)
plt.grid()
#plt.yscale("log")

fig.set_facecolor("white")

#plt.title(f"plotted on {day}, 2023", fontsize=titlesize, y=1.05)
plt.savefig("for_poster/time_discrepancy_hist.pdf", facecolor="white", bbox_inches="tight", pad_inches=0.3)

#========================================================

hist_count_test, bin_test = np.histogram(np.abs(det_time_dis_test), bins=np.logspace(0,2.3,10))
hist_count_test = np.array(hist_count_test, dtype="float")
bin_width_test = bin_test[1:] - bin_test[:-1]
bin_pos_test = (bin_test[1:] + bin_test[:-1])/2.
#hist_count_test /= bin_width_test
cum_hist_count_test = np.cumsum(hist_count_test)
cum_hist_count_test /= cum_hist_count_test[-1]
error_high_test = sgrb_count[-1]*cum_hist_count_test
error_low_test = sgrb_count[0]*cum_hist_count_test
error_bars_test = np.vstack((sgrb_count[1]*cum_hist_count_test-error_low_test, error_high_test-sgrb_count[1]*cum_hist_count_test))
#print(hist_count_test, bin_width_test, error_bars_test)

hist_count_gaussian_test, bin_gaussian_test = np.histogram(np.abs(det_time_dis_gaussian_test), bins=np.logspace(0,2.3,10))
hist_count_gaussian_test = np.array(hist_count_gaussian_test, dtype="float")
bin_width_gaussian_test = bin_gaussian_test[1:] - bin_gaussian_test[:-1]
bin_pos_gaussian_test = (bin_gaussian_test[1:] + bin_gaussian_test[:-1])/2.
#hist_count_gaussian_test /= bin_width_gaussian_test
cum_hist_count_gaussian_test = np.cumsum(hist_count_gaussian_test)
cum_hist_count_gaussian_test /= cum_hist_count_gaussian_test[-1]
error_high_gaussian_test = sgrb_count[-1]*cum_hist_count_gaussian_test
error_low_gaussian_test = sgrb_count[0]*cum_hist_count_gaussian_test
error_bars_gaussian_test = np.vstack((sgrb_count[1]*cum_hist_count_gaussian_test-error_low_gaussian_test, error_high_gaussian_test-sgrb_count[1]*cum_hist_count_gaussian_test))
#print(hist_count_gaussian_test, bin_width_gaussian_test, error_bars_gaussian_test)

fig = plt.figure(figsize=(12,8))
axis = fig.add_subplot(1,1,1)
#plt.errorbar(bin_pos_test, sgrb_count[1]*cum_hist_count_test, yerr=error_bars_test, marker=".", drawstyle="steps-mid", capsize=10, markeredgewidth=3, color="black", lw=3, label="closed-box")
#plt.errorbar(bin_pos_gaussian_test, sgrb_count[1]*cum_hist_count_gaussian_test, yerr=error_bars_gaussian_test, marker=".", drawstyle="steps-mid", capsize=10, markeredgewidth=3, color="orangered", lw=3, label="Gaussian noise (6 mJy, masked)")
plt.errorbar(bin_pos_test, cum_hist_count_test, marker=".", drawstyle="steps-mid", capsize=10, markeredgewidth=3, color="black", lw=3, label="closed-box")
plt.errorbar(bin_pos_gaussian_test, cum_hist_count_gaussian_test, marker=".", drawstyle="steps-mid", capsize=10, markeredgewidth=3, color="orangered", lw=3, label="Gaussian noise (6 mJy, masked)")

plt.xlabel(r"$|t_\mathrm{rec} - t_\mathrm{inj}|$ [days]",fontsize=fontsize)
plt.ylabel(r"$P(\Delta t )$", fontsize=fontsize)
axis.tick_params(axis='x',labelsize=fontsize)
axis.tick_params(axis='y',labelsize=fontsize)
plt.legend(fontsize=fontsize, loc="lower right")
axis.set_xticks(np.arange(0, 201, 50))
axis.set_xticks(np.arange(0, 201, 10), minor=True)
axis.set_yticks(np.arange(0, 1.1, 0.5))
axis.set_yticks(np.arange(0, 1.1, 0.1), minor=True)
plt.grid()
plt.yscale("log")
plt.xscale("log")

fig.set_facecolor("white")

#plt.title(f"plotted on {day}, 2023", fontsize=titlesize, y=1.05)

plt.savefig("for_poster/cum_hist.pdf", facecolor="white", bbox_inches="tight", pad_inches=0.3)

#========================================================

fig = plt.figure(figsize=(12,8))
axis = fig.add_subplot(1,1,1)
plt.scatter(det_injected_com_dis_test, np.abs(det_time_dis_test), c="black", s=15, label="closed box", marker="*",rasterized=True)
plt.scatter(det_injected_com_dis_gaussian_test, np.abs(det_time_dis_gaussian_test), c="orangered", s=15, label="Gaussian noise (6 mJy, masked)", marker="*",rasterized=True)

plt.ylabel(r"$|t_\mathrm{rec} - t_\mathrm{inj}|$ [days]",fontsize=fontsize)
plt.xlabel(r"$D$ [Mpc]", fontsize=fontsize)
axis.tick_params(axis='x',labelsize=fontsize)
axis.tick_params(axis='y',labelsize=fontsize)
plt.legend(fontsize=fontsize, loc="upper left",markerscale=5)
#axis.set_yticks(np.arange(0, 301, 150))
#axis.set_yticks(np.arange(0, 301, 10), minor=True)
plt.grid()
plt.yscale("log")
plt.xscale("log")
#plt.ylim(0.1, 300)
plt.xlim(10, 2*10**3)

fig.set_facecolor("white")

#plt.title(f"plotted on {day}, 2023", fontsize=titlesize, y=1.05)
plt.savefig("for_poster/time_discrepancy_vs_com_dis.pdf", facecolor="white", bbox_inches="tight", pad_inches=0.3)
#plt.savefig("for_poster/time_discrepancy_vs_com_dis_small_sigma.pdf", facecolor="white", bbox_inches="tight", pad_inches=0.3)

#========================================================

fig = plt.figure(figsize=(12,8))
axis = fig.add_subplot(1,1,1)
plt.scatter(det_estimated_amp_test, det_recovered_amp_test, c="black", s=15, label="closed box", marker="*",rasterized=True)
plt.scatter(det_estimated_amp_gaussian_test, det_recovered_amp_gaussian_test, c="orangered", s=15, label="Gaussian noise (6 mJy, masked)", marker="*",rasterized=True)
#plt.scatter(det_estimated_amp_no_mask, det_recovered_amp_no_mask, c="royalblue", s=5, label="Gaussian (6 mJy, no mask)", marker="*",rasterized=True)
plt.plot([10**(-3), 10**3], [10**(-3), 10**3], color="black", lw=3, linestyle="dashed", alpha=0.2)

plt.xlabel(r"$\langle A_\mathrm{rec} \rangle$",fontsize=fontsize)
plt.ylabel(r"$A_\mathrm{rec}$", fontsize=fontsize)
axis.tick_params(axis='x',labelsize=fontsize)
axis.tick_params(axis='y',labelsize=fontsize)
plt.legend(fontsize=fontsize, loc="lower right",markerscale=5)
#axis.set_yticks(np.arange(0, 301, 150))
#axis.set_yticks(np.arange(0, 301, 10), minor=True)
plt.grid()
plt.yscale("log")
plt.xscale("log")
#plt.ylim(10**(-3), 1)

fig.set_facecolor("white")

#plt.title(f"plotted on {day}, 2023", fontsize=titlesize, y=1.05)
plt.savefig("for_poster/inj_rec_amp.pdf", facecolor="white", bbox_inches="tight", pad_inches=0.3)
