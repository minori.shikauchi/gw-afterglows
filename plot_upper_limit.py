import matplotlib.pyplot as plt
import numpy as np
import math
from matplotlib import rc
day = "Sep 19"
threshold_array = np.logspace(-1, 2, 50)

rc('font',**{'family':'serif', 'serif':['Computer Modern']})
rc('text', usetex=True)
fontsize = 23
titlesize = 1.2*fontsize

# pipeline tuning
threshold_array = np.logspace(-1, 2, 50)
upper_array_test = np.zeros(len(threshold_array))
upper_array_gaussian_test = np.zeros(len(threshold_array))

for i in range(len(threshold_array)):
    threshold = threshold_array[i]

    injection_path_test = f"not_perfect_temp_bank/all_map/true_uniform/injection_{i}.npy"
    part_Lambda_array_test = np.load(injection_path_test)

    injection_path_gaussian_test = f"gaussian_noise/data_sigma/not_perfect_temp_bank/true_uniform/injection_{i}.npy"
    no_injection_path_gaussian_test = f"gaussian_noise/data_sigma/not_perfect_temp_bank/true_uniform/no_injection_{i}.npy"
    injection_Lambda_array_gaussian_test = np.load(injection_path_gaussian_test)
    no_injection_Lambda_array_gaussian_test = np.load(no_injection_path_gaussian_test)
    #injection_Lambda_array_gaussian_test *= 10**7
    #print(i, part_Lambda_array_test[:5], no_injection_Lambda_array_gaussian_test[:5], injection_Lambda_array_gaussian_test[:5])
    #no_injection_Lambda_array_gaussian_test *= 500

    ind_test = (part_Lambda_array_test > threshold)
    time_space_test = 4 * math.pi/3 * (1.5)**3 * part_Lambda_array_test[ind_test].shape[0]/100000 * 305/365.25

    #ind_gaussian_test = ((injection_Lambda_array_gaussian_test > threshold) & (no_injection_Lambda_array_gaussian_test < threshold))
    ind_gaussian_test = ((injection_Lambda_array_gaussian_test > threshold))
    time_space_gaussian_test = 4 * math.pi/3 * (1.5)**3 * injection_Lambda_array_gaussian_test[ind_gaussian_test].shape[0]/100000 * 305/365.25

    print(i, threshold, part_Lambda_array_test[ind_test].shape[0], injection_Lambda_array_gaussian_test[ind_gaussian_test].shape[0])

    try:
        upper_test = 2.303/time_space_test
    except ZeroDivisionError:
        upper_test = np.nan
        
    try:
        upper_gaussian_test = 2.303/time_space_gaussian_test
    except ZeroDivisionError:
        upper_gaussian_test = np.nan
        
    upper_array_test[i] = upper_test
    upper_array_gaussian_test[i] = upper_gaussian_test

fig = plt.figure(figsize=(12,8))
axis = fig.add_subplot(1,1,1)

plt.scatter(threshold_array, upper_array_test, s=100, marker=".", c="black", label=r"closed-box")
plt.scatter(threshold_array, upper_array_gaussian_test, s=100, marker=".", c="orangered", label=r"Gaussian noise (6 mJy)")

max_lnL_new_limited = 12.818
upper_new_limited = 975.42

max_lnL_new_limited_01_mJy = 12.085
upper_new_limited_01_mJy = 6502.80
#plt.scatter([max_lnL_new_limited], [upper_new_limited], s=100, marker="*", c="yellow", edgecolor="black", label=r"intensity-cut")

axis.axhspan(10, 1700, color = "lightblue", alpha=0.2, label=r"BNS merger rate")

plt.yscale("log")
plt.xscale("log")
plt.legend(loc="lower left", fontsize=fontsize*0.9, ncol=2, markerscale=2)

plt.ylabel(r"$<R_\mathrm{det}>_\mathrm{upper}$ [Gpc$^{-3}$ year$^{-1}$]", fontsize=fontsize)
plt.xlabel(r"$\ln \Lambda_\mathrm{th}$", fontsize=fontsize)
axis.tick_params(axis='x',labelsize=fontsize)
axis.tick_params(axis='y',labelsize=fontsize)
plt.xlim(0.1, 110)
plt.ylim(0.1, 3*10**4)

fig.set_facecolor("white")
#plt.title(f"plotted on {day}, 2023", fontsize=titlesize, y=1.05)

plt.savefig("for_poster/upper_limit_for_poster.pdf", facecolor="white", bbox_inches="tight", pad_inches=0.3)
#plt.savefig("for_poster/upper_limit_for_poster_small_sigma.pdf", facecolor="white", bbox_inches="tight", pad_inches=0.3)

