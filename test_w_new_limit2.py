﻿import numpy
import math
from astropy import units
from astropy.cosmology import Planck15, z_at_value
from scipy.stats import chi2, chi
import time
from multiprocessing import Pool, Process
import multiprocessing
import sys

consective_rev06 = [1878, 1879, 1889, 1894, 1904, 1906, 1907, 1908, 1909, 1912, 1913, 1914, 1919, 1920, 1921, 1924, 1926, 1927, 1928, 1929, 1931, 1933, 1934, 1935, 1936, 1939,1958, 1959, 1965, 1969, 1970, 1971, 1972, 1975, 1976, 1977, 1978, 1979, 1980, 1983, 1984, 1985, 1988, 1989, 1990, 2012, 2013, 2029, 2030, 2031, 2032, 2039, 2040, 2044, 2046, 2059, 2060, 2072, 2073, 2075, 2077, 2079, 2080, 2081, 2084, 2085, 2086, 2087, 2093, 2096, 2097, 2098, 2099, 2105, 2106, 2107, 2108, 2109, 2111, 2112, 2118, 2121, 2122, 2126, 2127, 2133, 2134, 2135, 2143, 2163, 2166, 2167, 2174, 2175, 2180, 2181, 2182]
day_duration = consective_rev06[-1] - consective_rev06[0] + 1

sigma = 6 * 10**(-3)
t_duration_list = numpy.logspace(0, 3.5, 10)
sample_number = 3400

# updated version so that the modules below can treat a template bank, i.e. multiple templates, at once
# the template bank must have a shape of (number of templates, time)
def weight_ave_mt(x,sigma):
    sigma_sq = sigma**2
    convolution = numpy.nansum(x/sigma_sq, axis=-1)
    denominator = numpy.nansum(1./sigma_sq, axis=-1)
    #print(x, convolution.shape, denominator.shape, numpy.ones_like(convolution).shape)
    try:
        return numpy.divide(convolution, denominator, out=numpy.ones_like(convolution)*numpy.nan, where=(denominator != 0))
    except TypeError: 
        if denominator == 0.:
            return numpy.nan
        else:
            return 1.*convolution/denominator

def convolve_temp_mt(data, sigma):
    """return a weighted inner product of (data, template) and that of (template, template) with a template bank"""
    template_bank = numpy.load("tot_array_interp_100.npy")
    
    convolve_temp = numpy.zeros(template_bank.shape)
    convolve_temp_data = numpy.zeros(template_bank.shape)
    ave_temp = numpy.zeros(template_bank.shape)
    ave_data = numpy.zeros(template_bank.shape)
    
    for n in range(template_bank.shape[1]):
        zero_pad = numpy.zeros((template_bank.shape[0],n))
        part_template = template_bank[:,:template_bank.shape[1]-n]
        padded_template = numpy.hstack((zero_pad, template_bank))[:,:template_bank.shape[1]]
        part_data = data
        part_sigma = sigma

        assert padded_template.shape[1] == part_data.shape[0], f"data not matched, {padded_template.shape}, {part_data.shape}"            
        assert padded_template.shape[1] == part_sigma.shape[0], f"sigma not matched, {padded_template.shape}, {part_sigma.shape}"
        assert (numpy.isnan(part_data) == numpy.isnan(part_sigma)).all(), "all the nan values should be in the same places!"
        sub_temp_ave = weight_ave_mt(padded_template, part_sigma)
        sub_data_ave = weight_ave_mt(part_data, part_sigma)

        padded_template -= sub_temp_ave[:,numpy.newaxis]
        convolve_temp[:,n] = numpy.nansum((padded_template/part_sigma)**2, axis=1)
        convolve_temp_data[:,n] = numpy.nansum(part_data*padded_template/part_sigma**2, axis=1)
        
        ave_temp[:,n] = sub_temp_ave
        ave_data[:,n] = sub_data_ave

    max_lambda = numpy.divide(0.5 * convolve_temp_data**2, convolve_temp, out=numpy.ones_like(convolve_temp)*numpy.nan, where=(convolve_temp != 0))
    amp_array =  numpy.divide(convolve_temp_data, convolve_temp, out=numpy.ones_like(convolve_temp)*numpy.nan, where=(convolve_temp != 0))
    const_array = ave_data - amp_array*ave_temp
    
    return convolve_temp, convolve_temp_data, max_lambda, amp_array, const_array, ave_temp

# calculate P(likelihood ratio|noise), finding max likelihood ratio
def calc_likelihood_ratio(start_pix, end_pix, result_list):
    #limited_map = numpy.load("/project/rpp-chime/minorip/limited_map.npy")
    limited_map = numpy.load("new_limited_map.npy")

    for p in range(start_pix, end_pix):
        pixel_random_number = p
        data = limited_map[:,pixel_random_number].copy()
    
        ind_array = numpy.arange(limited_map.shape[0])
        scrambled_ind_array = numpy.random.permutation(ind_array)

        #scrambled_data = numpy.random.permutation(data)
        scrambled_data = data[scrambled_ind_array]
        sigma_array = numpy.ones(day_duration)*sigma
        ind = (numpy.isnan(scrambled_data))
        sigma_array[ind] = numpy.nan

        convolve_template, convolve_temp_data, Lambda_n, amp, const_array, ave_temp = convolve_temp_mt(scrambled_data, sigma_array)
        """
        if numpy.nanmax(Lambda_n) > 1000:
            #print(p, numpy.isnan(convolve_template).all(), numpy.isnan(convolve_temp_data).all(), Lambda_n)
            print(p, numpy.nanmax(Lambda_n), Lambda_n, scrambled_data)
            numpy.save(f"weird_ind_data{p}", scrambled_ind_array)
            exit()
        """

        pos_amp_ind = (amp > 0)
        try:
            max_ind = numpy.nanargmax(Lambda_n[pos_amp_ind])
            max_lambda = Lambda_n[pos_amp_ind][max_ind]
        except ValueError:
            max_lambda = numpy.nan

        if (p+1) % 10000 == 0:
            print(f"... {p+1} processed ...")
            print(max_lambda)

        result_list_lock.acquire()
        result_list.append([p, max_lambda])
        result_list_lock.release()

if __name__ == "__main__":
    print("... starting ...")
    argvs = sys.argv
    file_number = int(argvs[1])
    new_limited_map = numpy.load("new_limited_map.npy")

    start_time = time.time()
    manager = multiprocessing.Manager()
    result_list = manager.list()

    processes = []
    process_num = 50
    for q in range(process_num):
        #process = Process(target=calc_likelihood_ratio, kwargs={'k': q, 'limited_map': limited_map, 'tot_array_interp': tot_array_interp, 'result_list': result_list})
        start_pix = sample_number*process_num*file_number + sample_number*q
        #end_pix = start_pix + sample_number
        end_pix = min(start_pix + sample_number, new_limited_map.shape[1])

        process = Process(target=calc_likelihood_ratio, kwargs={'start_pix': start_pix, 'end_pix': end_pix, 'result_list': result_list})
        process.start()
        processes.append(process)
        print(f"... {q+1}/{process_num} process(es) have been submitted: {start_pix} -  {end_pix}...")
        if end_pix == new_limited_map.shape[1]:
            break

    for p in processes:
        p.join()

    result_list = numpy.array(result_list)
    print(result_list.shape)
    numpy.save(f"no_injection/no_data_sigma/new_limited_map/test_likelihood_{file_number}", result_list)
        
    print(f"... {sample_number*process_num} calculate likelihood ratio ... : {time.time() - start_time:.2f} sec")

'''
# prepare a light curve template bank
array = np.load(f"/project/rpp-chime/minorip/lc_template/for_chime/update_jetangle_c/lc_41.npy")
tot_array = array[:,0,:]
print(tot_array.shape)

"""
for i in range(42, 46):
    array = np.load(f"/project/rpp-chime/minorip/lc_template/for_chime/update_jetangle_c/lc_{i}.npy")
    print(i, array[:,0,:].shape)
    tot_array = np.vstack((tot_array, array[:,0,:]))

tot_array.shape
"""

orig_template = tot_array[0,:]*10**(-3)
tot_array_interp = interp_lightcurve(0, orig_template)

#for j in range(1, tot_array.shape[0]):
for j in range(1, 100):
    orig_template = tot_array[j,:]*10**(-3)
    interp_template = interp_lightcurve(0, orig_template)
    tot_array_interp = np.vstack((tot_array_interp, interp_template))
    
tot_array_interp.shape

w1 = np.where(ind)[0]

type(w1), w1.shape

    if (k + 1) % 10000 == 0:
        print(k+1, tm.time() - start_time)
        

np.save("/project/rpp-chime/minorip/likelihood_array_1.npy", Lambda_array)


number = 100
template_number = 10

lc_random = np.random.randint(0, template_number, size=number)
pixel_random = np.array(random.choices(w1, np.ones(w1.shape[0]), k=number))
merger_random = np.random.randint(0, day_duration, size=number)


com_dis_path = "/project/rpp-chime/minorip/lc_template/add/com_dist_array_41_full.npy"
tot_com_dis = np.load(com_dis_path)
for i in range(42, 80):
    com_dis_path = f"/project/rpp-chime/minorip/lc_template/add/com_dist_array_{i}_full.npy"
    com_dis = np.load(com_dis_path)
    print(i, com_dis.shape)
    tot_com_dis = np.hstack((tot_com_dis, com_dis))

d_ref = 40 * u.Mpc


# load a stacked ringmap after filtering
filtered_stacked_ringmap = containers.RingMap.from_file("/project/rpp-chime/minorip/process_rev06/filtered_stacked_ringmap_intercyl.h5")

ra_values = filtered_stacked_ringmap.ra
el_values = filtered_stacked_ringmap.el

fname = "/project/rpp-chime/chime/chime_processed/beam_models/psrc_sky_fit/rev_02/beam_telescope_coords.h5"
gb = containers.GridBeam.from_file(fname)

num = 512
prim_beam_array = np.zeros((num, 4, 1217))
for p in range(num):
    #print(f"... regridding for freq = {gb.freq[p]} MHz. ...")
    beam_map_2d = np.abs(gb.beam[p,:,0]) # freq, pol, 0
    #beam_map_2d = np.abs(tot_beam[p,:,:])
    
    abs_prim_beam = beam_map_2d
    average_pb = abs_prim_beam[:,:,682]
    #print(np.max(average_pb.flatten()), np.min(average_pb.flatten()), np.median(average_pb.flatten()))
    prim_beam_array[p,:] = average_pb

total_pb_array = np.zeros((num, 4, 1024))

for i in range(1024):
    el = el_values[i]
    close_el_ind = np.argmin(np.abs(gb.theta - el))

    if el > gb.theta[close_el_ind]:
        assert (gb.theta[close_el_ind] < el) & (el < gb.theta[close_el_ind+1]), f"not correct, {gb.theta[close_el_ind]}, {el}, {gb.theta[close_el_ind+1]}"
        start_el_ind = close_el_ind
        end_el_ind = close_el_ind + 1
    elif el < gb.theta[close_el_ind]:
        assert (gb.theta[close_el_ind-1] < el) & (el < gb.theta[close_el_ind]), f"not correct, {gb.theta[close_el_ind-1]}, {el}, {gb.theta[close_el_ind]}"
        start_el_ind = close_el_ind - 1
        end_el_ind = close_el_ind
    else:
        assert gb.theta[close_el_ind] == el, f"not correct, {gb.theta[close_el_ind]}, {el}"
        start_el_ind = close_el_ind
        end_el_ind = close_el_ind

    if start_el_ind != end_el_ind:
        ave_pb = prim_beam_array[:,:,start_el_ind] + (prim_beam_array[:,:,end_el_ind] - prim_beam_array[:,:,start_el_ind])/(gb.theta[end_el_ind] - gb.theta[start_el_ind]) * (el - gb.theta[start_el_ind])
    else:
        ave_pb = prim_beam_array[:,:,start_el_ind]
        
    former_ave_pb = (prim_beam_array[:,:,start_el_ind] + prim_beam_array[:,:,end_el_ind])/2
            
    total_pb_array[:,:,i] = ave_pb

freq_array = filtered_stacked_ringmap.freq
average_pb_array = np.zeros(1024*len(freq_array)*4).reshape(len(freq_array), 4, 1024)

for i in range(len(freq_array)):
    freq = freq_array[i]
    ind = np.argmin(np.abs(gb.freq - freq))
    average_pb_array[i,:,:] = total_pb_array[ind,:,:]

ra_extent = (100, 150)
el_extent = (-0.2, 0.2)
start_ra, end_ra = np.searchsorted(ra_values, ra_extent)
start_el, end_el = np.searchsorted(el_values, el_extent)

# destriping 
weight_stacked = filtered_stacked_ringmap.weight
map_stacked = filtered_stacked_ringmap.map[0,:]

nan_ind = np.where(weight_stacked == 0)
map_stacked[nan_ind] = np.nan
med_ra = np.nanmedian(map_stacked, axis=-2)

des_filtered_stacked_ringmap = map_stacked – med_ra[:,:,np.newaxis,:]


days_data = consective_rev06

start_file = 0
file_num = len(days_data)

end_file = start_file + file_num
map_ind = 0

all_map = np.zeros(file_num*4096*1024).reshape(file_num, 4096, 1024)
all_map_before_med_mov = np.zeros(file_num*4096*1024).reshape(file_num, 4096, 1024)

tot = 0
for i in range(start_file, end_file):
    day = days_data[i]
    print(f"... {i+1}/{len(days_data)}: loading lsd_{day} ...")
    path = "/project/rpp-chime/minorip/process_rev06/lsd_{0}/filtered_sub_ringmap_lsd_{0}.h5".format(day)
    if not os.path.exists(path): 
        print(day)
    else:
        filtered_ringmap = containers.RingMap.from_file("/project/rpp-chime/minorip/process_rev06/lsd_{0}/filtered_sub_ringmap_lsd_{0}.h5".format(day))

        ave_pb_xx = average_pb_array[map_ind,0,:]
        f_map = (filtered_ringmap.map[0,0,map_ind,:] + des_filtered_stacked_ringmap[0,map_ind,:]) * ave_pb_xx[np.newaxis,:]
        #f_map /= 2
        weight = filtered_ringmap.weight[0,map_ind,:]
    
        #medsmooth = sn.median_filter(f_map, size=(5, 3))
        #msmin = medsmooth.min(axis=0)

        #new_f_map = f_map - msmin[np.newaxis, :]
    
        ind = np.where(weight == 0)
        f_map[ind] = np.nan
    
        #new_f_map[ind] = np.nan
    
        ##med_f_ra = np.nanmedian(f_map[start_ra:end_ra, :], axis=-2)
        med_f_ra = np.nanmedian(f_map, axis=-2)

        des_f_map = f_map - med_f_ra[np.newaxis,:]
        ##des_f_map = new_f_map - med_f_ra[np.newaxis,:]
        print(med_f_ra.shape, med_f_ra[np.newaxis,:].shape, np.sum(np.isnan(f_map))/(4096*1024))
    
        mov_med = moving_weighted_median(des_f_map, weight, (227, 1))
    
        ##all_map[tot,:] = new_f_map[:] #* average_pb_array[np.newaxis, map_ind]
        all_map[tot,:] = des_f_map - mov_med #* average_pb_array[np.newaxis, map_ind]
        #all_map[tot,:] = f_map #* average_pb_array[np.newaxis, map_ind]
        all_map_before_med_mov[tot,:] = des_f_map #* average_pb_array[np.newaxis, map_ind]
        tot += 1
    
print(np.sum(np.isnan(all_map))/(4096*1024*file_num))


# subtract median from the data (reducing the effect of time-independent sources in the sky)
median_map = np.nanmedian(all_map, axis=0)
all_map -= median_map[np.newaxis,:]

# Add the missing days back in to create the zero-padded maps
#first_date = rev06_good_days[0]
#end_date = rev06_good_days[93]
first_date = consective_rev06[0]
end_date = consective_rev06[-1]
day_range = end_date - first_date + 1

nan_pad_maps = np.zeros((day_range, all_map.shape[1], all_map.shape[2]))
nan_pad_maps_before_med_mov = np.zeros((day_range, all_map.shape[1], all_map.shape[2]))
#nan_pad_var_maps = np.zeros((day_range, all_map.shape[1], all_map.shape[2]))
#nan_pad_var_maps_before_med_mov = np.zeros((day_range, all_map.shape[1], all_map.shape[2]))

# average before padding with zeroes
intensity_average = np.nanmean(all_map, axis=0)
intensity_average_before_med_mov = np.nanmean(all_map, axis=0)

idx = 0
for day in range(first_date, end_date + 1):
    if day in days_data:
        # only subtract mean from nonzero days
        nan_pad_maps[day - first_date, :] = all_map[idx, :]
        nan_pad_maps_before_med_mov[day - first_date, :] = all_map_before_med_mov[idx, :] 

        #nan_pad_var_maps[day - first_date, :] = all_map[idx, :] - intensity_average
        #nan_pad_var_maps_before_med_mov[day - first_date, :] = all_map_before_med_mov[idx, :] - intensity_average_before_med_mov 
        idx += 1
    else:
        nan_pad_maps[day - first_date, :] = np.nan
        nan_pad_maps_before_med_mov[day - first_date, :] = np.nan 

        print(f'csd {day} does not exist!')

part_nan_pad_maps = nan_pad_maps[:,start_ra:end_ra, start_el:end_el]
part_nan_pad_maps_before_med_mov = nan_pad_maps_before_med_mov[:,start_ra:end_ra, start_el:end_el]

ind = (np.sum(np.isnan(part_nan_pad_maps) != True) > 50)
part_nan_pad_maps[ind].shape

# select pixels
#median_map = np.nanmedian(nan_pad_maps, axis=0)
#ind = (np.abs(median_map) < 10**(-3))
limited_map = nan_pad_maps

ind2 = (np.nansum(np.isnan(limited_map) != True, axis=0) > 30) & (np.nansum(np.isnan(limited_map) != True, axis=0) < 40)
limited_map = limited_map[:,ind2]
'''
