import numpy
import math
from astropy import units 
from astropy.cosmology import Planck15, z_at_value
import time 
from multiprocessing import Pool, Process
import multiprocessing
import sys
import random
from astropy.cosmology.core import CosmologyError

d_ref = 40 * units.Mpc

consective_rev06 = [1878, 1879, 1889, 1894, 1904, 1906, 1907, 1908, 1909, 1912, 1913, 1914, 1919, 1920, 1921, 1924, 1926, 1927, 1928, 1929, 1931, 1933, 1934, 1935, 1936, 1939,1958, 1959, 1965, 1969, 1970, 1971, 1972, 1975, 1976, 1977, 1978, 1979, 1980, 1983, 1984, 1985, 1988, 1989, 1990, 2012, 2013, 2029, 2030, 2031, 2032, 2039, 2040, 2044, 2046, 2059, 2060, 2072, 2073, 2075, 2077, 2079, 2080, 2081, 2084, 2085, 2086, 2087, 2093, 2096, 2097, 2098, 2099, 2105, 2106, 2107, 2108, 2109, 2111, 2112, 2118, 2121, 2122, 2126, 2127, 2133, 2134, 2135, 2143, 2163, 2166, 2167, 2174, 2175, 2180, 2181, 2182]
day_duration = consective_rev06[-1] - consective_rev06[0] + 1

sigma_orig = 1
t_duration_list = numpy.logspace(0, 3.5, 10)
threshold_array = numpy.logspace(-1, 2, 50)

tot_likelihood = numpy.load("tot_likelihood_all_map.npy")
tot_pix_number = numpy.load("tot_pix_number_all_map.npy")
valid_pix_array = numpy.load("valid_pix_array.npy")

lc_random = numpy.load("lc_random.npy")
merger_random = numpy.load("merger_random.npy")

#tot_array_interp_100 = numpy.load("tot_array_interp_100.npy")
inj_template_array = numpy.load("inj_temp_array.npy")

tot_com_dis = numpy.load("tot_com_dis.npy")
#tot_com_dis = numpy.load("com_dis_log_uniform.npy")

# updated version so that the modules below can treat a template bank, i.e. multiple templates, at once
# the template bank must have a shape of (number of templates, time)
def weight_ave_mt(x,sigma_array):
    sigma_sq = sigma_array**2
    convolution = numpy.nansum(x/sigma_sq, axis=-1)
    denominator = numpy.nansum(1./sigma_sq, axis=-1)
    #print(x, convolution.shape, denominator.shape, numpy.ones_like(convolution).shape)
    try:
        return numpy.divide(convolution, denominator, out=numpy.ones_like(convolution)*numpy.nan, where=(denominator != 0))
    except TypeError: 
        if denominator == 0.:
            return numpy.nan
        else:
            return 1.*convolution/denominator

def convolve_temp_mt(data_array, sigma_array):
    """return a weighted inner product of (data, template) and that of (template, template) with a template bank"""
    template_bank = numpy.load("tot_array_interp_100.npy")
    #template_bank = numpy.zeros(305).reshape(1, 305)
    #template_bank[0,50:305-50] = 1.*10**(-3)
    
    convolve_temp = numpy.zeros(template_bank.shape)
    convolve_temp_data = numpy.zeros(template_bank.shape)
    ave_temp = numpy.zeros(template_bank.shape)
    ave_data = numpy.zeros(template_bank.shape)
    
    for n in range(template_bank.shape[1]):
        zero_pad = numpy.zeros((template_bank.shape[0],n))
        part_template = template_bank[:,:template_bank.shape[1]-n]
        padded_template = numpy.hstack((zero_pad, template_bank))[:,:template_bank.shape[1]]
        part_data = data_array
        part_sigma = sigma_array

        assert padded_template.shape[1] == part_data.shape[0], f"data not matched, {padded_template.shape}, {part_data.shape}"            
        assert padded_template.shape[1] == part_sigma.shape[0], f"sigma not matched, {padded_template.shape}, {part_sigma.shape}"
        assert (numpy.isnan(part_data) == numpy.isnan(part_sigma)).all(), "all the nan values should be in the same places!"
        sub_temp_ave = weight_ave_mt(padded_template, part_sigma)
        sub_data_ave = weight_ave_mt(part_data, part_sigma)

        padded_template -= sub_temp_ave[:,numpy.newaxis]
        convolve_temp[:,n] = numpy.nansum((padded_template/part_sigma)**2, axis=1)
        convolve_temp_data[:,n] = numpy.nansum(part_data*padded_template/part_sigma**2, axis=1)
        
        ave_temp[:,n] = sub_temp_ave
        ave_data[:,n] = sub_data_ave

    max_lambda = numpy.divide(0.5 * convolve_temp_data**2, convolve_temp, out=numpy.ones_like(convolve_temp)*numpy.nan, where=(convolve_temp != 0))
    amp_array =  numpy.divide(convolve_temp_data, convolve_temp, out=numpy.ones_like(convolve_temp)*numpy.nan, where=(convolve_temp != 0))
    const_array = ave_data - amp_array*ave_temp
    
    return convolve_temp, convolve_temp_data, max_lambda, amp_array, const_array, ave_temp

# calculate P(likelihood ratio|noise), finding max likelihood ratio
def injection_test_w_gaussian_noise(start_pix, end_pix, result_list, pixel_random, orig_pix, result_list_lock):
    limited_map = numpy.load("all_map.npy")
    block_num = 3
    threshold = 3
    loop_num = limited_map.shape[0]//block_num + 1

    for k in range(start_pix, end_pix):
        pixel_random_number = int(pixel_random[k])
        ind_pixel = numpy.where(orig_pix == pixel_random_number)[0]

        for x in ind_pixel:
            #lc_random_number = lc_random[x]
            merger_random_number = merger_random[x]
        
            real_data = limited_map[:,pixel_random_number].copy()
            shuffle_ind = numpy.arange(305)
            numpy.random.shuffle(shuffle_ind)
            scrambled_data = real_data[shuffle_ind]
            for i in range(loop_num):
                start = block_num*i
                end = min(block_num*(i+1), len(scrambled_data))
                part_scrambled_data = scrambled_data[start:end]
    
                rms = numpy.sqrt(numpy.nanmean(scrambled_data**2))
                deviation_ind = (part_scrambled_data > threshold * rms)
                part_scrambled_data[deviation_ind] = numpy.nan
    
                scrambled_data[start:end] = part_scrambled_data

            inj_template = inj_template_array[x]
            #inj_template = tot_array_interp_100[lc_random_number]
    
            com_dis = tot_com_dis[x] * units.Mpc
            try:
                z = z_at_value(Planck15.comoving_distance, com_dis)
            except CosmologyError:
                z = 0
                
            lum_dis = Planck15.luminosity_distance(z)
            beta = (1 + z)*(d_ref/lum_dis)**2
        
            scrambled_data[merger_random_number:] += (beta.value*inj_template)[:305-merger_random_number]
            scrambled_data -= numpy.nanmedian(scrambled_data)
            data_sigma = numpy.nanstd(scrambled_data)

            data_sigma_array = numpy.ones(day_duration)*data_sigma

            data = numpy.random.normal(0, sigma_orig, 305)
            #data = numpy.random.normal(0, data_sigma, 305)
            mask_ind = numpy.isnan(scrambled_data)
            data[mask_ind] = numpy.nan
            data_sigma_array[mask_ind] = numpy.nan

            convolve_template_ni, convolve_temp_data_ni, Lambda_n_ni, amp_ni, const_array_ni, ave_temp_ni = convolve_temp_mt(data, data_sigma_array)
            pos_amp_ind_ni = ((amp_ni > 0) & (amp_ni <= 1.78))
            try:
                max_ind_ni = numpy.nanargmax(Lambda_n_ni[pos_amp_ind_ni])
                max_lambda_ni = Lambda_n_ni[pos_amp_ind_ni][max_ind_ni]
            except ValueError:
                max_lambda_ni = numpy.nan
    
            data[merger_random_number:] += (beta.value*inj_template)[:305-merger_random_number]
            data -= numpy.nanmedian(data)
    
            convolve_template, convolve_temp_data, Lambda_n, amp, const_array, ave_temp = convolve_temp_mt(data, data_sigma_array)
            pos_amp_ind = ((amp > 0) & (amp <= 1.78))
            try:
                max_ind = numpy.nanargmax(Lambda_n[pos_amp_ind])
                max_lambda = Lambda_n[pos_amp_ind][max_ind]
                estimate_amp = amp[pos_amp_ind][max_ind]
                estimate_const = const_array[pos_amp_ind][max_ind]
                #max_ind_2d = numpy.unravel_index(max_ind, Lambda_n.shape)
                #recovery_merger_time = max_ind_2d[1]
                max_ind_2d = numpy.where((Lambda_n == max_lambda) & (amp > 0))
                recovery_merger_time = max_ind_2d[1][0]
            except ValueError:
                max_lambda = numpy.nan
                estimate_amp = numpy.nan
                estimate_const = numpy.nan
                recovery_merger_time = numpy.nan
            #print(Lambda_n[pos_amp_ind][max_ind], estimate_amp, estimate_const)

            #if (k + 1) % sample_number == 0:
            #    print(k+1, time.time() - start_time)

            result_list_lock.acquire()
            result_list.append([pixel_random[k], max_lambda, recovery_merger_time, merger_random_number, estimate_amp, estimate_const, max_lambda_ni, beta.value])
            result_list_lock.release()

if __name__ == "__main__":
    print("... starting ...")
    argvs = sys.argv
    threshold_number = int(argvs[1])
    threshold = threshold_array[threshold_number]

    start_time = time.time()
    manager = multiprocessing.Manager()
    result_list = manager.list()
    result_list_lock = manager.Lock()

    processes = []

    ind = (tot_likelihood < threshold)
    pixel_random = numpy.array(list(set(tot_pix_number[ind]) & set(valid_pix_array)))
    sorted_valid_pix_array = numpy.sort(valid_pix_array)
    sorted_pixel_random = numpy.sort(pixel_random)
    total_inj_num = 0
    for i in sorted_pixel_random:
        ind = (valid_pix_array == i)
        total_inj_num += valid_pix_array[ind].shape[0]

    print(f"... threshold = {threshold}, pix number = {sorted_pixel_random.shape[0]}, # of injected sources = {total_inj_num} ...")
    #print(sorted_pixel_random)

    if len(sorted_pixel_random) <= 25:
        process_num = len(sorted_pixel_random)
        sample_number = 1
    else:
        process_num = 25
        sample_number = len(sorted_pixel_random)//process_num + 1

    for q in range(process_num):
        start_pix = sample_number*q
        end_pix = min(start_pix + sample_number, len(sorted_pixel_random))

        process = Process(target=injection_test_w_gaussian_noise, kwargs={'start_pix': start_pix, 'end_pix': end_pix, 'result_list': result_list, 'pixel_random': sorted_pixel_random, "orig_pix": sorted_valid_pix_array, 'result_list_lock':result_list_lock})
        process.start()
        processes.append(process)
        print(f"... {q+1}/{process_num} process(es) have been submitted: {start_pix} -  {end_pix}...")
        if end_pix == len(sorted_pixel_random):
            break

    for p in processes:
        p.join()

    """
    processes = []

    process_num = 20
    sample_number = len(sorted_valid_pix_array)//process_num + 1

    for q in range(process_num):
        start_pix = sample_number*q
        end_pix = min(start_pix + sample_number, len(sorted_valid_pix_array))

        process = Process(target=injection_test_w_gaussian_noise, kwargs={'start_pix': start_pix, 'end_pix': end_pix, 'result_list': result_list, 'valid_pix': sorted_valid_pix_array, "result_list_lock":result_list_lock})
        process.start()
        processes.append(process)
        print(f"... {q+1}/{process_num} process(es) have been submitted: {start_pix} -  {end_pix}...")
        if end_pix == len(sorted_valid_pix_array):
            break

    for p in processes:
        p.join()
    """

    result_list = numpy.array(result_list)
    ind = numpy.argsort(result_list[:,0])
    exp_likelihood = result_list[ind, 1]
    exp_pix_number = result_list[ind, 0]
    exp_rec_merger_time = result_list[ind, 2]
    exp_inj_merger_time = result_list[ind, 3]
    exp_amp = result_list[ind, 4]
    exp_const = result_list[ind, 5]
    exp_likelihood_ni = result_list[ind, 6]
    exp_inj_amp = result_list[ind, 7]
    print(result_list.shape, exp_pix_number[0:10], exp_likelihood[0:10], exp_rec_merger_time[0:10], exp_inj_merger_time[0:10])
    numpy.save(f"gaussian_noise/large_data_sigma/not_perfect_temp_bank/true_uniform/det_pix_{threshold_number}", exp_pix_number)
    numpy.save(f"gaussian_noise/large_data_sigma/not_perfect_temp_bank/true_uniform/injection_{threshold_number}", exp_likelihood)
    numpy.save(f"gaussian_noise/large_data_sigma/not_perfect_temp_bank/true_uniform/recovered_merger_time_{threshold_number}", exp_rec_merger_time)
    numpy.save(f"gaussian_noise/large_data_sigma/not_perfect_temp_bank/true_uniform/injected_merger_time_{threshold_number}", exp_inj_merger_time)
    numpy.save(f"gaussian_noise/large_data_sigma/not_perfect_temp_bank/true_uniform/recovered_amp_{threshold_number}", exp_amp)
    numpy.save(f"gaussian_noise/large_data_sigma/not_perfect_temp_bank/true_uniform/recovered_const_{threshold_number}", exp_const)
    numpy.save(f"gaussian_noise/large_data_sigma/not_perfect_temp_bank/true_uniform/no_injection_{threshold_number}", exp_likelihood_ni)
    numpy.save(f"gaussian_noise/large_data_sigma/not_perfect_temp_bank/true_uniform/injected_amp_{threshold_number}", exp_inj_amp)
        
    print(f"... injection test ... : {time.time() - start_time:.2f} sec")

