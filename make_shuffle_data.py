﻿import numpy as np
import math
from astropy.constants import c
import astropy.units as u
from draco.core import containers # needed for WienerFilter module
from ch_pipeline.core import containers as ch_containers
from astropy.cosmology import Planck15, z_at_value
from scipy.stats import chi2, chi
import time as tm
from multiprocessing import Pool, Process
import multiprocessing
import sys

sample_number = 1200

def shuffle_data(start_pix, end_pix, result_list):
    #limited_map = np.load("/project/rpp-chime/minorip/limited_map.npy")

    for p in range(start_pix, end_pix):
        pixel_random_number = p
        data = limited_map[:,pixel_random_number].copy()
        scrambled_data = np.random.permutation(data)

        new_part = np.append(np.array([p]), scrambled_data)
        if (p+1) % 10000 == 0:
            print(f"{p+1} processed ...")

        #result_list.append([p, scrambled_data])
        result_list.append(new_part)

if __name__ == "__main__":
    print("... starting ...")
    argvs = sys.argv
    file_number = int(argvs[1])
    repeat_number = int(argvs[2])

    limited_map = np.load(f"shuffle{repeat_number - 1}/limited_map_shuffle{repeat_number - 1}.npy")
    #limited_map = np.load("/project/rpp-chime/minorip/limited_map.npy")

    start_time = tm.time()
    manager = multiprocessing.Manager()
    result_list = manager.list()

    processes = []
    for q in range(50):
        start_pix = sample_number*50*file_number + sample_number*q
        end_pix = start_pix + sample_number

        process = Process(target=shuffle_data, kwargs={'start_pix': start_pix, 'end_pix': end_pix, 'result_list': result_list})
        process.start()
        processes.append(process)
        #print(f"... {q+1}/50 process(es) have been submitted: {start_pix} -  {end_pix}...")

    for p in processes:
        p.join()

    result_list = np.array(result_list)

    sort_arg = np.argsort(result_list[:,0])
    sort_result = result_list[sort_arg, 1:]
    sort_pix_num = result_list[sort_arg, 0]
    #print(result_list.shape, sort_result.shape, sort_pix_num[0:10])

    np.save(f"shuffle{repeat_number}/limited_map_shuffle{repeat_number}_{file_number}", sort_result)
        
    print(f"... {sample_number*50} shuffle data ... : {tm.time() - start_time:.2f} sec")

