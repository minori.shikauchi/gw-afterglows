import numpy
import math
from astropy import units 
from astropy.cosmology import Planck15, z_at_value
import time 
from multiprocessing import Pool, Process
import multiprocessing
import sys
import random
from astropy.cosmology.core import CosmologyError

d_ref = 40 * units.Mpc

consective_rev06 = [1878, 1879, 1889, 1894, 1904, 1906, 1907, 1908, 1909, 1912, 1913, 1914, 1919, 1920, 1921, 1924, 1926, 1927, 1928, 1929, 1931, 1933, 1934, 1935, 1936, 1939,1958, 1959, 1965, 1969, 1970, 1971, 1972, 1975, 1976, 1977, 1978, 1979, 1980, 1983, 1984, 1985, 1988, 1989, 1990, 2012, 2013, 2029, 2030, 2031, 2032, 2039, 2040, 2044, 2046, 2059, 2060, 2072, 2073, 2075, 2077, 2079, 2080, 2081, 2084, 2085, 2086, 2087, 2093, 2096, 2097, 2098, 2099, 2105, 2106, 2107, 2108, 2109, 2111, 2112, 2118, 2121, 2122, 2126, 2127, 2133, 2134, 2135, 2143, 2163, 2166, 2167, 2174, 2175, 2180, 2181, 2182]
day_duration = consective_rev06[-1] - consective_rev06[0] + 1

sigma = 6 * 10**(-3)
t_duration_list = numpy.logspace(0, 3.5, 10)
threshold_array = numpy.logspace(-1, 2, 50)

tot_array_interp_100 = numpy.load("/project/rpp-chime/minorip/tot_array_interp_100.npy")
tot_likelihood = numpy.load("tot_likelihood.npy")
tot_pix_number = numpy.load("tot_pix_number.npy")
valid_pix_array = numpy.load("valid_pix_array.npy")

lc_random = numpy.load("lc_random.npy")

merger_random = numpy.load("merger_random.npy")
#inj_template_array = numpy.load("inj_temp_array.npy")

tot_com_dis = numpy.load("tot_com_dis.npy")

# updated version so that the modules below can treat a template bank, i.e. multiple templates, at once
# the template bank must have a shape of (number of templates, time)
def weight_ave_mt(x,sigma):
    sigma_sq = sigma**2
    convolution = numpy.nansum(x/sigma_sq, axis=-1)
    denominator = numpy.nansum(1./sigma_sq, axis=-1)
    #print(x, convolution.shape, denominator.shape, numpy.ones_like(convolution).shape)
    try:
        return numpy.divide(convolution, denominator, out=numpy.ones_like(convolution)*numpy.nan, where=(denominator != 0))
    except TypeError: 
        if denominator == 0.:
            return numpy.nan
        else:
            return 1.*convolution/denominator

def convolve_temp_mt(data, sigma):
    """return a weighted inner product of (data, template) and that of (template, template) with a template bank"""
    template_bank = numpy.load("/project/rpp-chime/minorip/tot_array_interp_100.npy")
    #print(f"size of the template bank: {template_bank.shape}")
    
    convolve_temp = numpy.zeros(template_bank.shape)
    convolve_temp_data = numpy.zeros(template_bank.shape)
    ave_temp = numpy.zeros(template_bank.shape)
    ave_data = numpy.zeros(template_bank.shape)
    
    #print(f"size of weighted average of data: {sub_data_ave.shape}")
    nan_ind = numpy.array([])
    for n in range(template_bank.shape[1]):
        part_template = template_bank[:,:template_bank.shape[1]-n]
        part_data = data[n:]
        part_sigma = sigma[n:]

        assert part_template.shape[1] == part_data.shape[0], f"data not matched, {part_template.shape}, {part_data.shape}"            
        assert part_template.shape[1] == part_sigma.shape[0], f"sigma not matched, {part_template.shape}, {part_sigma.shape}"
        assert (numpy.isnan(part_data) == numpy.isnan(part_sigma)).all(), "all the nan values should be in the same places!"
        sub_temp_ave = weight_ave_mt(part_template, part_sigma)
        sub_data_ave = weight_ave_mt(part_data, part_sigma)

        part_template -= sub_temp_ave[:,numpy.newaxis]
        #print(weight_ave_mt(padded_template, stack_sigma))
        convolve_temp[:,n] = numpy.nansum((part_template/part_sigma)**2, axis=1)
        convolve_temp_data[:,n] = numpy.nansum(part_data*part_template/part_sigma**2, axis=1)
        
        ave_temp[:,n] = sub_temp_ave
        ave_data[:,n] = sub_data_ave

    max_lambda = numpy.divide(0.5 * convolve_temp_data**2, convolve_temp, out=numpy.ones_like(convolve_temp)*numpy.nan, where=(convolve_temp != 0))
    amp_array =  numpy.divide(convolve_temp_data, convolve_temp, out=numpy.ones_like(convolve_temp)*numpy.nan, where=(convolve_temp != 0))
    const_array = ave_data - amp_array*ave_temp
    
    return convolve_temp, convolve_temp_data, max_lambda, amp_array, const_array, ave_temp

# calculate P(likelihood ratio|noise), finding max likelihood ratio
def injection_test(start_pix, end_pix, result_list, pixel_random, orig_pix):
    #limited_map = numpy.load("/project/rpp-chime/minorip/limited_map.npy")
    limited_map = numpy.load("/project/rpp-chime/minorip/all_map.npy")

    for k in range(start_pix, end_pix):
        pixel_random_number = int(pixel_random[k])
        ind_pixel = numpy.where(orig_pix == pixel_random_number)[0]
        #print(ind_pixel, orig_pix[ind_pixel])

        for x in ind_pixel:
            #lc_random_number = lc_random[ind_pixel]
            lc_random_number = lc_random[x]
            #merger_random_number = merger_random[ind_pixel]
            merger_random_number = merger_random[x]
        
            data = limited_map[:,pixel_random_number].copy()
            numpy.random.shuffle(data)
            shuffled_data = numpy.random.permutation(data)
            #inj_template = inj_template_array[ind_pixel]
            inj_template = tot_array_interp_100[lc_random_number]
    
            #com_dis = tot_com_dis[ind_pixel] * units.Mpc
            com_dis = tot_com_dis[x] * units.Mpc
            #if len(ind_pixel) > 1:
            #    print(f"... injecting signals into pixel no. {pixel_random_number}, for {x} in {ind_pixel}, {com_dis} ...")
            #print(pixel_random_number, ind_pixel, merger_random_number, com_dis)
            try:
                z = z_at_value(Planck15.comoving_distance, com_dis)
            except CosmologyError:
                z = 0
                
            lum_dis = Planck15.luminosity_distance(z)
            beta = (1 + z)*(d_ref/lum_dis)**2
        
            shuffled_data[merger_random_number:] += (beta.value*inj_template)[:305-merger_random_number]
    
            sigma_array = numpy.ones(day_duration)*sigma
            #ind = (numpy.isnan(data))
            ind = (numpy.isnan(shuffled_data))
            sigma_array[ind] = numpy.nan

            convolve_template, convolve_temp_data, Lambda_n, amp, const_array, ave_temp = convolve_temp_mt(shuffled_data, sigma_array)

            max_ind = numpy.where(Lambda_n == numpy.nanmax(Lambda_n))
            recovery_ind = numpy.unravel_index(numpy.nanargmax(Lambda_n), Lambda_n.shape)
            recovery_merger_time = recovery_ind[1]

            estimate_amp = amp[max_ind][0] # need to be fixed in the future?
            estimate_const = const_array[max_ind][0] # same
            #print(estimate_amp.shape, estimate_const.shape)
            if len(estimate_amp) == 1:
                rec_amp = estimate_amp[0]
                rec_const = estimate_const[0]
            else:
                print(k, x, numpy.nanmax(Lambda_n), max_ind, estimate_amp, estimate_const, Lambda_n[max_ind])
                #print(pixel_random_number, ind_pixel, merger_random_number, lc_random_number, com_dis)
                numpy.save(f"mystery_data_{k}", shuffled_data)
                exit()

            result_list.append([pixel_random[k], numpy.nanmax(Lambda_n), recovery_merger_time, merger_random_number, estimate_amp, estimate_const, beta.value])

if __name__ == "__main__":
    print("... starting ...")
    argvs = sys.argv
    threshold_number = int(argvs[1])
    threshold = threshold_array[threshold_number]

    start_time = time.time()
    manager = multiprocessing.Manager()
    result_list = manager.list()

    processes = []

    ind = (tot_likelihood < threshold)
    #part_pix_number = tot_pix_number[ind]
    #pixel_random = numpy.array(random.choices(part_pix_number, numpy.ones(part_pix_number.shape[0]), k=sample_number*50))
    pixel_random = numpy.array(list(set(tot_pix_number[ind]) & set(valid_pix_array)))
    sorted_valid_pix_array = numpy.sort(valid_pix_array)
    sorted_pixel_random = numpy.sort(pixel_random)
    print(f"... threshold = {threshold}, pix number = {sorted_pixel_random.shape[0]} ...")
    #print(sorted_pixel_random)

    if len(sorted_pixel_random) <= 10:
        process_num = len(sorted_pixel_random)
        sample_number = 1
    else:
        process_num = 10
        sample_number = len(sorted_pixel_random)//process_num + 1

    #process_num = 1
    for q in range(process_num):
        start_pix = sample_number*q
        end_pix = min(start_pix + sample_number, len(sorted_pixel_random))
        #start_pix = 5584
        #end_pix = start_pix + 1

        process = Process(target=injection_test, kwargs={'start_pix': start_pix, 'end_pix': end_pix, 'result_list': result_list, 'pixel_random': sorted_pixel_random, "orig_pix": sorted_valid_pix_array})
        process.start()
        processes.append(process)
        print(f"... {q+1}/{process_num} process(es) have been submitted: {start_pix} -  {end_pix}...")
        if end_pix == len(sorted_pixel_random):
            break

    for p in processes:
        p.join()

    result_list = numpy.array(result_list)
    print(result_list.shape)
    ind = numpy.argsort(result_list[:,0])
    exp_likelihood = result_list[ind, 1]
    exp_pix_number = result_list[ind, 0]
    exp_rec_merger_time = result_list[ind, 2]
    exp_inj_merger_time = result_list[ind, 3]
    exp_amp = result_list[ind, 4]
    exp_const = result_list[ind, 5]
    inj_amp = result_list[ind, 6]
    print(exp_pix_number[0:10], exp_rec_merger_time[0:10], exp_inj_merger_time[0:10])
    #numpy.save(f"not_perfect_temp_bank/injection_{threshold_number}_not_perfect_temp_bank", tot_likelihood)
    numpy.save(f"perfect_temp_bank/true_uniform/det_pix_{threshold_number}", exp_pix_number)
    numpy.save(f"perfect_temp_bank/true_uniform/injection_{threshold_number}", exp_likelihood)
    numpy.save(f"perfect_temp_bank/true_uniform/recovered_merger_time_{threshold_number}", exp_rec_merger_time)
    numpy.save(f"perfect_temp_bank/true_uniform/injected_merger_time_{threshold_number}", exp_inj_merger_time)
    numpy.save(f"perfect_temp_bank/true_uniform/recovered_amp_{threshold_number}", exp_amp)
    numpy.save(f"perfect_temp_bank/true_uniform/recovered_const_{threshold_number}", exp_const)
    numpy.save(f"perfect_temp_bank/true_uniform/injected_amp_{threshold_number}", inj_amp)
        
    print(f"... {threshold_number + 1} injection test ... : {time.time() - start_time:.2f} sec")

