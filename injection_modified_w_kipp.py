import numpy 
import math
from astropy import units
from astropy.cosmology import Planck15, z_at_value
from multiprocessing import Pool, Process
import multiprocessing
import sys
import random

from astropy.cosmology.core import CosmologyError

# updated version so that the modules below can treat a template bank, i.e. multiple templates, at once
# the template bank must have a shape of (number of templates, time)
def weight_ave_mt(x,sigma):
    assert (numpy.isnan(x) == numpy.isnan(sigma)).all(), "all the nan values should be in the same places!"
    sigma_sq = sigma**2
    return numpy.nansum(x/sigma_sq, axis=1)/numpy.nansum(1./sigma_sq, axis=1)

#def convolve_temp_mt(data, temp, sigma):
def convolve_temp_mt(data, sigma):
    """return a weighted inner product of (data, template) and that of (template, template) with a template bank"""
    template_bank = numpy.load("/project/rpp-chime/minorip/tot_array_interp_100.npy")
    #print(f"size of the template bank: {template_bank.shape}")
    stack_data = numpy.tile(data, (template_bank.shape[0], 1)) # duplicate data for the number of templates
    stack_sigma = numpy.tile(sigma, (template_bank.shape[0], 1)) # duplicate sigma for the number of templates
        
    nan_array = numpy.isnan(stack_data)
    assert len(stack_data) == len(template_bank), f"lengths should be the same, {len(stack_data)}, {len(template_bank)}"
    
    convolve_temp = numpy.zeros(template_bank.shape)
    convolve_temp_data = numpy.zeros(template_bank.shape)
    ave_temp = numpy.zeros(template_bank.shape)
    
    sub_data_ave = weight_ave_mt(stack_data, stack_sigma)
    #print(f"size of weighted average of data: {sub_data_ave.shape}")
    for n in range(template_bank.shape[1]):
        zero_pad = numpy.zeros((template_bank.shape[0], n))
        padded_template = numpy.hstack((zero_pad, template_bank))[:,:template_bank.shape[1]]
        #print(n, numpy.sum(padded_template == 0)/100)

        assert padded_template.shape == template_bank.shape, f"not matched, {padded_template.shape}, {template_bank.shape}"            
        padded_template[nan_array] = numpy.nan
        #print(n, numpy.sum(numpy.isnan(padded_template))/100)
        sub_temp_ave = weight_ave_mt(padded_template, stack_sigma)

        padded_template -= sub_temp_ave[:,numpy.newaxis]
        #print(weight_ave_mt(padded_template, stack_sigma))
        convolve_temp[:,n] = numpy.nansum((padded_template/stack_sigma)**2, axis=1)
        convolve_temp_data[:,n] = numpy.nansum(stack_data*padded_template/stack_sigma**2, axis=1)
        
        ave_temp[:,n] = sub_temp_ave

    max_lambda = numpy.divide(0.5 * convolve_temp_data**2., convolve_temp, out=numpy.ones_like(convolve_temp)*numpy.nan, where=(convolve_temp != 0))
    amp_array =  numpy.divide(convolve_temp_data, convolve_temp, out=numpy.ones_like(convolve_temp)*numpy.nan, where=(convolve_temp != 0))
    const_array = sub_data_ave[:,numpy.newaxis] - amp_array*ave_temp
    
    return convolve_temp, convolve_temp_data, max_lambda, amp_array, const_array, ave_temp
