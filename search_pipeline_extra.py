import numpy
import math
from astropy import units 
from astropy.cosmology import Planck15, z_at_value
import time 
from multiprocessing import Pool, Process
import multiprocessing
import sys
import random
from astropy.cosmology.core import CosmologyError
import os

d_ref = 40 * units.Mpc

consective_rev06 = [1878, 1879, 1889, 1894, 1904, 1906, 1907, 1908, 1909, 1912, 1913, 1914, 1919, 1920, 1921, 1924, 1926, 1927, 1928, 1929, 1931, 1933, 1934, 1935, 1936, 1939,1958, 1959, 1965, 1969, 1970, 1971, 1972, 1975, 1976, 1977, 1978, 1979, 1980, 1983, 1984, 1985, 1988, 1989, 1990, 2012, 2013, 2029, 2030, 2031, 2032, 2039, 2040, 2044, 2046, 2059, 2060, 2072, 2073, 2075, 2077, 2079, 2080, 2081, 2084, 2085, 2086, 2087, 2093, 2096, 2097, 2098, 2099, 2105, 2106, 2107, 2108, 2109, 2111, 2112, 2118, 2121, 2122, 2126, 2127, 2133, 2134, 2135, 2143, 2163, 2166, 2167, 2174, 2175, 2180, 2181, 2182]
day_duration = consective_rev06[-1] - consective_rev06[0] + 1

t_duration_list = numpy.logspace(0, 3.5, 10)
threshold_array = numpy.logspace(-1, 2, 50)

valid_pix_array = numpy.load("valid_pix_array.npy")
merger_random = numpy.load("merger_random.npy")
inj_template_array = numpy.load("inj_temp_array.npy")
tot_com_dis = numpy.load("tot_com_dis.npy")
data_std = numpy.load("data_based_sigma.npy")

# updated version so that the modules below can treat a template bank, i.e. multiple templates, at once
# the template bank must have a shape of (number of templates, time)
def weight_ave_mt(x,sigma_array):
    sigma_sq = sigma_array**2
    convolution = numpy.nansum(x/sigma_sq, axis=-1)
    denominator = numpy.nansum(1./sigma_sq, axis=-1)
    #print(x, convolution.shape, denominator.shape, numpy.ones_like(convolution).shape)
    try:
        return numpy.divide(convolution, denominator, out=numpy.ones_like(convolution)*numpy.nan, where=(denominator != 0))
    except TypeError: 
        if denominator == 0.:
            return numpy.nan
        else:
            return 1.*convolution/denominator

def convolve_temp_mt(data_array, sigma_array):
    """return a weighted inner product of (data, template) and that of (template, template) with a template bank"""
    template_bank = numpy.load("tot_array_interp_100.npy")
    
    convolve_temp = numpy.zeros(template_bank.shape)
    convolve_temp_data = numpy.zeros(template_bank.shape)
    ave_temp = numpy.zeros(template_bank.shape)
    ave_data = numpy.zeros(template_bank.shape)
    
    for n in range(template_bank.shape[1]):
        zero_pad = numpy.zeros((template_bank.shape[0],n))
        part_template = template_bank[:,:template_bank.shape[1]-n]
        padded_template = numpy.hstack((zero_pad, template_bank))[:,:template_bank.shape[1]]
        part_data = data_array
        part_sigma = sigma_array

        assert padded_template.shape[1] == part_data.shape[0], f"data not matched, {padded_template.shape}, {part_data.shape}"            
        assert padded_template.shape[1] == part_sigma.shape[0], f"sigma not matched, {padded_template.shape}, {part_sigma.shape}"
        assert (numpy.isnan(part_data) == numpy.isnan(part_sigma)).all(), "all the nan values should be in the same places!"
        sub_temp_ave = weight_ave_mt(padded_template, part_sigma)
        sub_data_ave = weight_ave_mt(part_data, part_sigma)

        padded_template -= sub_temp_ave[:,numpy.newaxis]
        convolve_temp[:,n] = numpy.nansum((padded_template/part_sigma)**2, axis=1)
        convolve_temp_data[:,n] = numpy.nansum(part_data*padded_template/part_sigma**2, axis=1)
        
        ave_temp[:,n] = sub_temp_ave
        ave_data[:,n] = sub_data_ave

    max_lambda = numpy.divide(0.5 * convolve_temp_data**2, convolve_temp, out=numpy.ones_like(convolve_temp)*numpy.nan, where=(convolve_temp != 0))
    amp_array =  numpy.divide(convolve_temp_data, convolve_temp, out=numpy.ones_like(convolve_temp)*numpy.nan, where=(convolve_temp != 0))
    const_array = ave_data - amp_array*ave_temp
    
    return convolve_temp, convolve_temp_data, max_lambda, amp_array, const_array, ave_temp

# calculate P(likelihood ratio|noise), finding max likelihood ratio
def calc_likelihood(start_pix, end_pix, data, result_list, result_list_lock, inj_or_no_inj, data_std):
    start_func = time.time()
    block_num = 3
    threshold_rms = 3

    for k in range(start_pix, end_pix):
        real_data = data[:,k]
        data_sigma = data_std[k]

        for start in range(0, len(real_data), block_num):
            end = min(start+block_num, len(real_data))
            part_real_data = real_data[start:end]
    
            rms = numpy.sqrt(numpy.nanmean(part_real_data**2))
            deviation_ind = (part_real_data > threshold_rms * rms)
            part_real_data[deviation_ind] = numpy.nan
    
            real_data[start:end] = part_real_data

        scrambled_data = numpy.random.permutation(real_data)
    
        sigma_array = numpy.ones(day_duration)*data_sigma
        ind = (numpy.isnan(real_data))
        sigma_array[ind] = numpy.nan

        convolve_template, convolve_temp_data, Lambda_n, amp, const_array, ave_temp = convolve_temp_mt(real_data, sigma_array)
    
        pos_amp_ind = ((amp > 0) & (amp <= 1.78*10**3))
        try:
            max_ind = numpy.nanargmax(Lambda_n[pos_amp_ind])
            max_lambda = Lambda_n[pos_amp_ind][max_ind]
            if inj_or_no_inj == "y":
                estimate_amp = amp[pos_amp_ind][max_ind]
                estimate_const = const_array[pos_amp_ind][max_ind]
                max_ind_2d = numpy.where((Lambda_n == max_lambda) & (amp > 0))
                recovery_merger_time = max_ind_2d[1][0]
            else:
                pass
        except ValueError:
            max_lambda = numpy.nan
            if inj_or_no_inj == "y":
                estimate_amp = numpy.nan
                estimate_const = numpy.nan
                recovery_merger_time = numpy.nan
            else:
                pass

        sigma_array_sc = numpy.ones(day_duration)*data_sigma
        ind = (numpy.isnan(scrambled_data))
        sigma_array_sc[ind] = numpy.nan

        convolve_template_sc, convolve_temp_data_sc, Lambda_n_sc, amp_sc, const_array_sc, ave_temp_sc = convolve_temp_mt(scrambled_data, sigma_array_sc)

        sc_pos_amp_ind = ((amp_sc > 0) & (amp_sc <= 1.78*10**3))
        try:
            sc_max_ind = numpy.nanargmax(Lambda_n_sc[sc_pos_amp_ind])
            max_lambda_sc = Lambda_n_sc[sc_pos_amp_ind][sc_max_ind]
        except ValueError:
            max_lambda_sc = numpy.nan

        result_list_lock.acquire()
        if inj_or_no_inj == "y":
            result_list.append([k, max_lambda, recovery_merger_time, estimate_amp, estimate_const, max_lambda_sc])
        else:
            result_list.append([k, max_lambda, max_lambda_sc])
        result_list_lock.release()

        if (k+1)%1000 == 0.:
            print(k+1, time.time() - start_func)

if __name__ == "__main__":
    print("... starting ...")
    argvs = sys.argv
    data_option = argvs[1]
    inj_or_no_inj = argvs[2]
    part_num = int(argvs[3])

    if data_option == "closed-box":
        dir_path = "not_perfect_temp_bank/all_map/true_uniform/"
        if inj_or_no_inj == "y":
            tag = f"w_inj_{part_num}"
            data_path = os.path.join(dir_path, "injected_data.npy")
            data_std = data_std[valid_pix_array]
        elif inj_or_no_inj == "n":
            data_path = os.path.join(dir_path, "no_inj_all_map.npy")
            tag = f"no_inj_{part_num}"
        else:
            print("set 'y' or 'n'.")
            exit()
    elif data_option == "gaussian":
        dir_path = "gaussian_noise/all_at_once/small_data_sigma/not_perfect_temp_bank/true_uniform/"
        if inj_or_no_inj == "y":
            tag = f"w_inj_{part_num}"
            data_path = os.path.join(dir_path, "injected_data.npy")
            data_std = data_std[valid_pix_array]
        elif inj_or_no_inj == "n":
            data_path = os.path.join(dir_path, "no_inj_gaussian_noise.npy")
            tag = f"no_inj_{part_num}"
        else:
            print("set 'y' or 'n'.")
            exit()
    else:
        print("set an appropriate tag!")
        exit()

    start_time = time.time()
    manager = multiprocessing.Manager()
    result_list = manager.list()
    result_list_lock = manager.Lock()

    processes = []
    data = numpy.load(data_path)
    
    print(f"... # of data = {data.shape[1]} ...")

    process_num = 20
    sample_number = 50

    for q in range(process_num):
        start_pix = process_num*sample_number*part_num + sample_number*q
        end_pix = min(start_pix + sample_number, data.shape[1])

        process = Process(target=calc_likelihood, kwargs={'start_pix': start_pix, 'end_pix': end_pix, 'data': data, 'result_list': result_list, 'result_list_lock':result_list_lock, "inj_or_no_inj": inj_or_no_inj, "data_std":data_std})
        process.start()
        processes.append(process)
        print(f"... {q+1}/{process_num} process(es) have been submitted: {start_pix} -  {end_pix}...")
        if end_pix == data.shape[1]:
            break

    for p in processes:
        p.join()

    result_list = numpy.array(result_list)
    print(result_list.shape)
    ind = numpy.argsort(result_list[:,0])
    exp_pix_number = result_list[ind, 0]
    exp_likelihood = result_list[ind, 1]

    if inj_or_no_inj == "y":
        exp_rec_merger_time = result_list[ind, 2]
        exp_amp = result_list[ind, 3]
        exp_const = result_list[ind, 4]
        exp_likelihood_sc = result_list[ind, 5]
        print(exp_pix_number[0:10], exp_likelihood[0:10], exp_rec_merger_time[0:10])

        numpy.save(os.path.join(dir_path, f"lambda_{tag}"), exp_likelihood)
        numpy.save(os.path.join(dir_path, f"recovered_merger_time_{tag}"), exp_rec_merger_time)
        numpy.save(os.path.join(dir_path, f"recovered_amp_{tag}"), exp_amp)
        numpy.save(os.path.join(dir_path, f"recovered_const_{tag}"), exp_const)
        numpy.save(os.path.join(dir_path, f"lambda_after_shuffling_{tag}"), exp_likelihood_sc)
    elif inj_or_no_inj == "n":
        exp_likelihood_sc = result_list[ind, 2]
        print(exp_pix_number[0:10], exp_likelihood[0:10])

        numpy.save(os.path.join(dir_path, f"temp/storage0/lambda_{tag}"), exp_likelihood)
        numpy.save(os.path.join(dir_path, f"temp/storage0/lambda_after_shuffling_{tag}"), exp_likelihood_sc)
        
    print(f"... calculation done ... : {time.time() - start_time:.2f} sec")

