import subprocess
import numpy as np
import time
import sys

cmd = ""
#end_submit_number = 28 # for the all map
#end_submit_number = 18 # for the new limited map
#end_submit_number = 3 # for the new limited map (0.1 mJy)

shuffle_num = 0

if __name__ == "__main__":
    argvs = sys.argv
    start_submit_number = int(argvs[1])
    end_submit_number = int(argvs[2])
    option1 = argvs[3]
    option2 = argvs[4]

    for i in range(start_submit_number, end_submit_number):
        if i == end_submit_number - 1:
            cmd += f"python3 search_pipeline_extra.py {option1} {option2} {i} "
        else:
            cmd += f"python3 search_pipeline_extra.py {option1} {option2} {i} && "

    print(cmd)
    subprocess.run(cmd, shell=True)


"""
tot_array = np.load(f"no_injection/new_limited_map_01_mJy/test_likelihood_0.npy")
for i in range(1,end_submit_number):
    part_array = np.load(f"no_injection/new_limited_map_01_mJy/test_likelihood_{i}.npy")
    tot_array = np.vstack((tot_array, part_array))

print(tot_array.shape)
tot_array = np.sort(tot_array, axis=0)
tot_likelihood = tot_array[:,1]
tot_pix_number = tot_array[:,0]
print(tot_likelihood, np.array(tot_pix_number,dtype="int"))
np.save(f"tot_likelihood_new_limited_map_01_mJy", tot_likelihood)
np.save(f"tot_pix_number_new_limited_map_01_mJy", np.array(tot_pix_number, dtype="int"))
"""
